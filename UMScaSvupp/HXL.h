#include "stdafx.h"
#include "UMScaSvuppDB.h"
#include "Resource.h"

typedef enum _replacedlg {REPLACE_ALL, NOT_REPLACE_ALL, DO_DIALOG, RENAME} enumREPLACEDLG;

int GetNextFileNumber(CString /*sFilename*/);
int Check_File(CString sName, int *n_Replace_All_Files);
int HXL_Make_File_Plant(TRAKT_DATA_PLANT t,CString sName);
int HXL_Make_File_Mark(TRAKT_DATA_MARK t,CString sName);
int HXL_Make_File_Roj(TRAKT_DATA_ROJ t,CString sName);

CString ReplaceIllegal(CString *pcsIn);
