// MDITraktFormView.cpp : implementation file
//

#include "stdafx.h"
#include "MDITraktFormView.h"

#include "ResLangFileReader.h"

// CMDITraktFormView dialog

IMPLEMENT_DYNAMIC(CMDITraktFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CMDITraktFormView, CDialog)
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()


CMDITraktFormView::CMDITraktFormView()
	: CXTResizeFormView(CMDITraktFormView::IDD)
{
}

CMDITraktFormView::~CMDITraktFormView()
{
}

BOOL CMDIGallBasTraktFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CMDITraktFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	
	DDX_Control(pDX, IDC_STATIC2, m_wndLbl1);
	DDX_Control(pDX, IDC_STATIC4, m_wndLbl2);

	DDX_Control(pDX, IDC_COMBO1, m_wndCBox1);
	DDX_Control(pDX, IDC_COMBO1, m_wndCBox2);
}

#ifdef _DEBUG
void CMDIGallBasTraktFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CMDIGallBasTraktFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

// CMDITraktFormView message handlers

void CMDIGallBasTraktFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	m_wndCBox1.SetEnabledColor(BLACK,WHITE );
	m_wndCBox1.SetDisabledColor(BLACK,INFOBK );

	m_wndCBox2.SetEnabledColor(BLACK,WHITE );
	m_wndCBox2.SetDisabledColor(BLACK,INFOBK );

	setAllReadOnly(TRUE,TRUE);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	getForvaltsFromDB();
	addForvaltsToCBox();

	getDistrictFromDB();
	addDistrictToCBox();


	// Setup language filename; 051214 p�d
	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	
	setLanguage();

	m_nDBIndex = 0;
	populateData(m_nDBIndex);

	m_bIsDirty = FALSE;
}

BOOL CMDITraktFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CMDITraktFormView::OnSetFocus(CWnd*)
{
	if (!m_vecTraktData.empty())
	{
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTraktData.size()-1));
	}
	else
	{
		setNavigationButtons(FALSE,FALSE);
	}
}

void CMDITraktFormView::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			// Trakt information
			m_wndLbl1.SetWindowText(xml->str(IDS_STRING7501));
			m_wndLbl2.SetWindowText(xml->str(IDS_STRING7503));
			
		}
	}
}

void CMDITraktFormView::populateData(UINT idx)
{
	DISTRICT_DATA data;
	if (!m_vecTraktData.empty() && 
		  idx >= 0 && 
			idx < m_vecTraktData.size())
	{
		m_recActiveTrakt = m_vecTraktData[idx];

		m_wndCBox1.SetWindowText(getForvaltSelected(m_recActiveTrakt.m_nForvaltID));
		m_wndCBox2.SetWindowText(getDistrictSelected(m_recActiveTrakt.m_nDistrictID));		
	}
	else
	{
		// This method set editboxes read or read only, depending on 
		// if there's Compartments and Plots; 061010 p�d
		setAllReadOnly( TRUE, TRUE );
		clearAll();
	}

}



// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 061010 p�d
LRESULT CMDITraktFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch(wParam)
	{
		// Messages from HMSShell
		case ID_NEW_ITEM :
		{
			m_enumAction = MANUALLY;
			addTraktManually();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_OPEN_ITEM :
		{
			m_enumAction = FROM_FILE;
			addTrakt();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_SAVE_ITEM :
		{
			saveTrakt();
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_DELETE_ITEM :
		{
			deleteTrakt();
			resetTrakt(RESET_TO_LAST_SET_NB);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell
		case ID_UPDATE_ITEM :
		{
			resetTrakt(RESET_TO_LAST_SET_NB);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			if (!isDataChanged())
			{
				m_nDBIndex = 0;
				setNavigationButtons(FALSE,TRUE);
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_PREV :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			if (!isDataChanged())
			{
				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;
				if (m_nDBIndex == 0)
				{
					setNavigationButtons(FALSE,TRUE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			if (!isDataChanged())
			{

				m_nDBIndex++;
				if (m_nDBIndex > ((UINT)m_vecTraktData.size() - 1))
					m_nDBIndex = (UINT)m_vecTraktData.size() - 1;
					
				if (m_nDBIndex == (UINT)m_vecTraktData.size() - 1)
				{
					setNavigationButtons(TRUE,FALSE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_END :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			if (!isDataChanged())
			{
				m_nDBIndex = (UINT)m_vecTraktData.size()-1;
				setNavigationButtons(TRUE,FALSE);	
				populateData(m_nDBIndex);
			}
			break;
		}	// case ID_NEW_ITEM :

	};
	return 0L;
}

void CMDITraktFormView::getTraktsFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getTrakts(m_vecTraktData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDITraktFormView::getForvaltsFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getForvalts(m_vecForvaltData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDITraktFormView::getDistrictFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getDistricts(m_vecDistrictData);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}


void CMDITraktFormView::addTraktManually(void)
{
	setAllReadOnly( FALSE,FALSE );	// Open up fields for editing; 061011 p�d

	clearAll();
	m_wndListMachineBtn.EnableWindow( TRUE );
	m_bIsDirty = TRUE;
}

void CMDITraktFormView::addTrakt(void)
{
	if (createFromFile(m_dbConnectionData,m_sLangFN,&m_structTraktIdentifer))
		resetTrakt(RESET_TO_JUST_ENTERED_SET_NB);
}

void CMDITraktFormView::deleteTrakt(void)
{
	DISTRICT_DATA dataDistrict;
	TRAKT_DATA dataTrakt;
	CString sMsg;
	CString sText1;
	CString sText2;
	CString sText3;
	CString sText4;
	CString sRegion;
	CString sDistrict;
	CString sMachine;
	CString sTrakt;
	CString sCaption;
	CString sOKBtn;
	CString sCancelBtn;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sText1 = xml->str(IDS_STRING5001);
			sText2 = xml->str(IDS_STRING5011);
			sText3 = xml->str(IDS_STRING5021);
			sText4 = xml->str(IDS_STRING5031);
			sRegion = xml->str(IDS_STRING504);
			sDistrict = xml->str(IDS_STRING505);
			sMachine = xml->str(IDS_STRING506);
			sTrakt = xml->str(IDS_STRING507);

			sCaption = xml->str(IDS_STRING109);
			sOKBtn = xml->str(IDS_STRING110);
			sCancelBtn = xml->str(IDS_STRING111);
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))

	// Check that there's any data in m_vecTraktData vector; 061010 p�d
	if (m_vecTraktData.size() > 0)
	{
		if (m_bConnected)
		{
			// Get Region information from Database server; 070122 p�d
			CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
			if (pDB != NULL)
			{
					// Get Regions in database; 061002 p�d	
					dataTrakt = m_vecTraktData[m_nDBIndex];
					// Setup a message for user upon deleting machine; 061010 p�d

					getRegionAndDistrictData(dataTrakt.m_nRegionID,dataTrakt.m_nDistrictID,dataDistrict);
					sMsg.Format("<font size=\"+4\"><center><b>%s</b></center></font><br><hr><br>%s : <b>%d</b>   (%s)<br>%s : <b>%d</b>  (%s)<br>%s : <b>%d</b><br>%s : <b>%s</b><br><hr><br>%s<br>%s<br><br><font size=\"+4\" color=\"#ff0000\"><center>%s</center></font>",
											sText1,
											sRegion,
											dataDistrict.m_nRegionID,
											dataDistrict.m_sRegionName,
											sDistrict,
											dataDistrict.m_nDistrictID,
											dataDistrict.m_sDistrictName,
											sMachine,
											dataTrakt.m_nMachineID,
											sTrakt,
											dataTrakt.m_sTraktNum,
											sText2,
											sText3,
											sText4);

					if (messageDialog(sCaption,sOKBtn,sCancelBtn,sMsg))
					{
						// Delete Machine and ALL underlying 
						//	data (Trakt(s),Compartment(s) and Plot(s)); 061010 p�d
						pDB->delTrakt(dataTrakt);
					}

				delete pDB;
			}	// if (pDB != NULL)
		} // 	if (m_bConnected)
	}	// if (m_vecMachineData.size() > 0)
	m_bIsDirty = FALSE;
	resetTrakt(RESET_TO_LAST_SET_NB);
}

// PROTECTED
void CMDITraktFormView::setDefaultTypeAndOrigin(void)
{
	CString sTmp;
	for (int i = 0;i < sizeof(INVENTORY_TYPE_ARRAY)/sizeof(INVENTORY_TYPE_ARRAY[0]);i++)
	{
		m_wndCBox1.AddString(INVENTORY_TYPE_ARRAY[i].sInvTypeName);
	}

	for (int i = 0;i < sizeof(ORIGIN_ARRAY)/sizeof(ORIGIN_ARRAY[0]);i++)
	{
		m_wndCBox2.AddString(ORIGIN_ARRAY[i].sOrigin);
	}
}


// Get manually entered data; 061011 p�d
void CMDITraktFormView::getEnteredData(void)
{
	// Identification information
	m_recNewTrakt.m_nRegionID		= getRegionID();
	m_recNewTrakt.m_nDistrictID	= getDistrictID();
	m_recNewTrakt.m_nMachineID		= m_wndEdit5.getInt();
	m_recNewTrakt.m_nOrigin			= getOrigin_setByUser();
	m_recNewTrakt.m_sOriginName	= getOriginName_setByUser();
	m_recNewTrakt.m_sType			= getType_setByUser();
	m_recNewTrakt.m_sTypeName		= getTypeName_setByUser();
	m_recNewTrakt.m_fTraktAreal	= m_wndEdit33.getFloat();
	// Trakt information
	CString sDate;
	m_wndDatePicker.GetWindowText(sDate);
	m_recNewTrakt.m_sTraktDate		= sDate;
	m_recNewTrakt.m_sTraktNum		= m_wndEdit31.getText();
	m_recNewTrakt.m_sTraktName		= m_wndEdit32.getText();
	// Notes
	m_recNewTrakt.m_sNotes			= m_wndEdit34.getText();
	// Trakt data
	m_recNewTrakt.m_recData = CVariables(m_wndEdit7.getFloat(),
																				0.0,	// Road dist 2 not used
																				m_wndEdit8.getFloat(),
																				0.0,	// Road width 2 not used
																				m_wndEdit9.getFloat(),
																				m_wndEdit10.getFloat(),
																				m_wndEdit11.getFloat(),
																				m_wndEdit12.getFloat(),
																				m_wndEdit13.getFloat(),
																				m_wndEdit23.getFloat(),
																				m_wndEdit24.getFloat(),
																				0.0,
																				m_wndEdit14.getFloat(),
																				m_wndEdit15.getFloat(),
																				m_wndEdit16.getFloat(),
																				m_wndEdit17.getFloat(),
																				m_wndEdit18.getFloat(),
																				m_wndEdit19.getFloat(),
																				m_wndEdit20.getFloat(),
																				m_wndEdit21.getFloat(),
																				m_wndEdit22.getFloat(),
																				m_wndEdit26.getFloat(),
																				m_wndEdit27.getFloat(),
																				m_wndEdit28.getFloat(),
																				m_wndEdit29.getFloat(),
																				m_wndEdit30.getFloat());

	// Check RegionID and DistrictID. If they eq. -1 then
	// Set to m_recActiveTrakt values; 061011 p�d
	if (m_recNewMachine.m_nRegionID == -1 && m_recNewMachine.m_nDistrictID == -1)
	{
		m_recNewMachine.m_nRegionID = m_recActiveTrakt.m_nRegionID;
		m_recNewMachine.m_nDistrictID = m_recActiveTrakt.m_nDistrictID;
	}

	// Setup who data's entered, Maually or by file; 061201 p�d
	if (m_enumAction == MANUALLY)
		m_recNewTrakt.n_nEnterType = 1;
}

// Method used on createing a new Trakt manually.
// Clears ALL fileds for entering data. OBS! Region/District is entered
// using a Dialog; 061011 p�d
void CMDITraktFormView::clearAll(void)
{
	m_wndEdit5.SetWindowText("");
	m_wndEdit6.SetWindowText("");
	m_wndEdit7.SetWindowText("");
	m_wndEdit8.SetWindowText("");
	m_wndEdit9.SetWindowText("");
	m_wndEdit10.SetWindowText("");
	m_wndEdit11.SetWindowText("");
	m_wndEdit12.SetWindowText("");
	m_wndEdit13.SetWindowText("");
	m_wndEdit14.SetWindowText("");
	m_wndEdit15.SetWindowText("");
	m_wndEdit16.SetWindowText("");
	m_wndEdit17.SetWindowText("");
	m_wndEdit18.SetWindowText("");
	m_wndEdit19.SetWindowText("");
	m_wndEdit20.SetWindowText("");
	m_wndEdit21.SetWindowText("");
	m_wndEdit22.SetWindowText("");
	m_wndEdit23.SetWindowText("");
	m_wndEdit24.SetWindowText("");
	m_wndEdit26.SetWindowText("");
	m_wndEdit27.SetWindowText("");
	m_wndEdit28.SetWindowText("");
	m_wndEdit29.SetWindowText("");
	m_wndEdit30.SetWindowText("");
	m_wndEdit31.SetWindowText("");
	m_wndEdit32.SetWindowText("");
	m_wndEdit33.SetWindowText("");
	m_wndEdit34.SetWindowText("");
	m_wndCBox1.SetCurSel(-1);
	m_wndCBox2.SetCurSel(-1);
	m_wndCBox3.SetCurSel(-1);
	m_wndCBox4.SetCurSel(-1);
	m_wndDatePicker.SetWindowText("");

	// Set m_recNewTrakt key-values to -1
	m_recNewTrakt.m_nRegionID = -1;
	m_recNewTrakt.m_nDistrictID = -1;
	m_recNewTrakt.m_nMachineID = -1;
	m_recNewTrakt.m_sTraktNum = "";
	m_recNewTrakt.m_nOrigin = -1;
	m_recNewTrakt.m_sOriginName = "";
	m_recNewTrakt.m_sType = "";
	m_recNewTrakt.m_sTypeName = "";
}

// Set all Editboxes to read or read only, depending on
// if data comes from database and holds Compartmant(s) and Plot(s); 061010 p�d
void CMDITraktFormView::setAllReadOnly(BOOL ro1,BOOL ro2)
{
	m_wndListMachineBtn.EnableWindow( FALSE );

	m_wndEdit7.SetReadOnly( ro1 );
	m_wndEdit8.SetReadOnly( ro1 );
	m_wndEdit10.SetReadOnly( ro1 );
	m_wndEdit11.SetReadOnly( ro1 );
	m_wndEdit13.SetReadOnly( ro1 );
	m_wndEdit14.SetReadOnly( ro1 );
	m_wndEdit15.SetReadOnly( ro1 );
	m_wndEdit16.SetReadOnly( ro1 );
	m_wndEdit17.SetReadOnly( ro1 );
	m_wndEdit19.SetReadOnly( ro1 );
	m_wndEdit20.SetReadOnly( ro1 );
	m_wndEdit21.SetReadOnly( ro1 );
	m_wndEdit22.SetReadOnly( ro1 );
	m_wndEdit23.SetReadOnly( ro1 );
	m_wndEdit24.SetReadOnly( ro1 );
	m_wndEdit26.SetReadOnly( ro1 );
	m_wndEdit27.SetReadOnly( ro1 );
	m_wndEdit28.SetReadOnly( ro1 );
	m_wndEdit29.SetReadOnly( ro1 );
	m_wndEdit30.SetReadOnly( ro1 );
//	Commented out 2007-04-19 on request from Holmen
//	this field'll always be editable; 070419 p�d
//	m_wndEdit32.SetReadOnly( ro1 );

	m_wndEdit31.SetReadOnly( ro2 );
	m_wndCBox1.SetReadOnly( ro2 );
	m_wndCBox2.SetReadOnly( ro2 );
	m_wndCBox3.SetReadOnly( ro2 );
	m_wndCBox4.SetReadOnly( ro2 );

}

// Create a string containing Region and District in the same form as displayed in
// dialog for "Bekr�fta inl�sning"; 061011 p�d
CString CMDITraktFormView::getRegionAndDistrictData(int region,int dist,DISTRICT_DATA &rec)
{
	CString sRetStr;
	DISTRICT_DATA data;
	if (m_vecDistrictData.empty())
		return "";

	for (UINT i = 0;i < m_vecDistrictData.size();i++)
	{
		data = m_vecDistrictData[i];
		if (data.m_nRegionID == region && data.m_nDistrictID == dist)
		{
			sRetStr.Format("(%d) %s  -  (%d) %s",data.m_nRegionID,
																					data.m_sRegionName,
																					data.m_nDistrictID,
																					data.m_sDistrictName);
			rec = data;
			break;
		}
	}	// for (UINT i = 0;i < m_vecDistrictData.size();i++)
	return sRetStr;
}


// Add regions in registry, into ComboBox3; 061204 p�d
void CMDITraktFormView::addForvaltsToCBox(void)
{
	CString sText;
	if (m_vecForvaltData.size() > 0)
	{
		m_wndCBox1.ResetContent();
		for (UINT i = 0;i < m_vecForvaltData.size();i++)
		{
			FORVALT_DATA data = m_vecForvaltData[i];
			sText.Format("%d - %s",data.m_nForvaltID,data.m_sForvaltName);
			m_wndCBox1.AddString(sText);
			m_wndCBox1.SetItemData(i, data.m_nForvaltID);
		}	// for (UINT i = 0;i < m_vecRegionData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
}

// Try to find region, based on index; 061204 p�d
CString CMDITraktFormView::getForvaltSelected(int forvalt_num)
{
	CString sText;
	if (m_vecForvaltData.size() > 0)
	{
		for (UINT i = 0;i < m_vecForvaltData.size();i++)
		{
			FORVALT_DATA data = m_vecForvaltData[i];
			if (data.m_nForvaltID == forvalt_num)
			{
				sText.Format("%d - %s",data.m_nForvaltID,data.m_sForvaltName);
				return sText;
			}
		}
	}	// if (m_vecRegionData.size() > 0)
	return "";
}

// Try to find index of forvaltnumber in m_vecForvaltData;
int CMDITraktFormView::getForvaltID(void)
{
	// If there's data and the index's on a specific item, from that
	// index. This's used on Update of trakt.
	// Index of Currently selected Forvaltn is used on New item; 061205 p�d
	if (m_vecTraktData.size() > 0 && m_nDBIndex >= 0 && m_nDBIndex < m_vecTraktData.size() && m_enumAction == NOTHING)
	{
		return m_vecTraktData[m_nDBIndex].m_nTrakt_forvalt_num;
	}
	else
	{
		int nIdx = m_wndCBox1.GetCurSel();
		if (nIdx != CB_ERR)
		{
			return m_wndCBox1.GetItemData(nIdx);
		}
	}
	return -1;
}

// Add Districts into Combobox4, based on selected region; 061204 p�d
void CMDITraktFormView::addDistrictsToCBox(void)
{
	CString sText;
	if (m_vecDistrictData.size() > 0)
	{
		m_wndCBox2.ResetContent();
		for (UINT i = 0;i < m_vecDistrictData.size();i++)
		{
			DISTRICT_DATA data = m_vecDistrictData[i];
			sText.Format("%d - %s",data.m_nDistrictID,data.m_sDistrictName);
			m_wndCBox2.AddString(sText);
			m_wndCBox2.SetItemData(i, data.m_nDistrictID);
		}	// for (UINT i = 0;i < m_vecDistrictData.size();i++)
	}	// if (m_vecDistrictData.size() > 0)
}

// Try to find district, based on index; 061204 p�d
CString CMDITraktFormView::getDistrictSelected(int district_num)
{
	CString sText;
	if (m_vecDistrictData.size() > 0)
	{
		for (UINT i = 0;i < m_vecDistrictData.size();i++)
		{
			DISTRICT_DATA data = m_vecDistrictData[i];
			if (data.m_nDistrictID == district_num)
			{
				sText.Format("%d - %s",data.m_nDistrictID,data.m_sDistrictName);
				return sText;
			}	// if (data.m_nDistrictID == district_num)
		}	// for (UINT i = 0;i < m_vecDistrictData.size();i++)
	}	// if (m_vecRegionData.size() > 0)
	
		return "";
}

// Try to find index of regionnumber in m_vecDistrictData; 061204 p�d
int CMDITraktFormView::getDistrictID(void)
{
	// If there's data and the index's on a specific item, from that
	// index. This's used on Update of trakt.
	// Index of Currently selected District is used on New item; 061205 p�d
	if (m_vecTraktData.size() > 0 && m_nDBIndex >= 0 && m_nDBIndex < m_vecTraktData.size() && m_enumAction == NOTHING)
	{
		return m_vecTraktData[m_nDBIndex].m_nTrakt_distrikt_num;
	}
	else
	{
		int nIdx = m_wndCBox2.GetCurSel();
		if (nIdx != CB_ERR)
		{
			return m_wndCBox2.GetItemData(nIdx);
		}
	}
	return -1;
}


// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CMDITraktFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
}


// PUBLIC
void CMDITraktFormView::saveTrakt(void)
{
	if (isOKToSave() && m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CGallBasDB *pDB = new CGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			getEnteredData();
			// Setup index information; 061207 p�d
			m_structTraktIdentifer.nRegionID			= m_recNewTrakt.m_nRegionID;
			m_structTraktIdentifer.nDistrictID		= m_recNewTrakt.m_nDistrictID;
			m_structTraktIdentifer.nMachineID		= m_recNewTrakt.m_nMachineID;
			m_structTraktIdentifer.sTraktNum			= m_recNewTrakt.m_sTraktNum;
			m_structTraktIdentifer.sType				= m_recNewTrakt.m_sType;
			m_structTraktIdentifer.nOrigin			= m_recNewTrakt.m_nOrigin;

			if (!pDB->addTrakt( m_recNewTrakt ))
			{
				if ((::MessageBox(0,m_sUpdateTraktMsg,m_sErrCap,MB_YESNO | MB_ICONSTOP) == IDYES))
				{
					pDB->updTrakt( m_recNewTrakt );
				}
			}
			delete pDB;
		}	// if (pDB != NULL)

		m_bIsDirty = FALSE;
		resetIsDirty();
		resetTrakt(RESET_TO_JUST_ENTERED_SET_NB);
		m_enumAction = NOTHING;

	}	// if (isOKToSave())
}

// Check that information needed to be abel to save Trakt, is enterd
// by user; 061113 p�d
BOOL CMDITraktFormView::isOKToSave(void)
{
	if (strcmp(m_wndCBox3.getText().Trim(),"") == 0 ||
		  strcmp(m_wndCBox4.getText().Trim(),"") == 0 ||
		  m_wndEdit5.getInt() <= 0 ||
		  strcmp(m_wndEdit31.getText().Trim(),"") == 0 ||
			getType_setByUser().Trim() == "" ||
		  getOrigin_setByUser() == -1) 
	{
		// Also tell user that there's insufficent data; 061113 p�d
		::MessageBox(0,m_sNotOkToSave,m_sErrCap,MB_ICONSTOP | MB_OK);
		return FALSE;
	}

	return TRUE;
}

// Check if active data's chnged by user.
// I.e. manually enterd data; 061113 p�d
BOOL CMDITraktFormView::isDataChanged(void)
{
	if (getIsDirty())
	{
		if (::MessageBox(0,m_sSaveData,m_sErrCap,MB_ICONSTOP | MB_YESNO) == IDYES)
		{
			saveTrakt();
		}
		resetIsDirty();
		return TRUE;
	}
	return FALSE;
}

void CMDITraktFormView::doPopulate(UINT index)
{
	m_nDBIndex = index;
	populateData(m_nDBIndex);
	setNavigationButtons(m_nDBIndex > 0,
											 m_nDBIndex < (m_vecTraktData.size()-1));
}

// Reset and get data from database; 061010 p�d
void CMDITraktFormView::resetTrakt(enumRESET reset)
{
	// Get updated data from Database
	getTraktsFromDB();
	// Check that there's any data in m_vecMachineData vector; 061010 p�d
	if (m_vecTraktData.size() > 0)
	{

		if (reset == RESET_TO_FIRST_SET_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = 0;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(m_nDBIndex > 0,
				    								m_nDBIndex < (m_vecTraktData.size()-1));
		}
		if (reset == RESET_TO_FIRST_NO_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = 0;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(FALSE,FALSE);
		}
		if (reset == RESET_TO_LAST_SET_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = m_vecTraktData.size()-1;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(m_nDBIndex > 0,
				    								m_nDBIndex < (m_vecTraktData.size()-1));
		}
		if (reset == RESET_TO_LAST_NO_NB)
		{
			// Reset to last item in m_vecTraktData
			m_nDBIndex = m_vecTraktData.size()-1;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons(FALSE,FALSE);
		}
		if (reset == RESET_TO_JUST_ENTERED_SET_NB || reset == RESET_TO_JUST_ENTERED_NO_NB)
		{

			// Find itemindex for last entry, from file, and set on populateData; 061207 p�d
			for (UINT i = 0;i < m_vecTraktData.size();i++)
			{
				TRAKT_DATA data = m_vecTraktData[i];
				if (data.m_nRegionID == m_structTraktIdentifer.nRegionID &&
					  data.m_nDistrictID == m_structTraktIdentifer.nDistrictID &&
						data.m_nMachineID == m_structTraktIdentifer.nMachineID &&
						data.m_sTraktNum.Trim() == m_structTraktIdentifer.sTraktNum.Trim() &&
						data.m_sType.Trim() == m_structTraktIdentifer.sType.Trim() &&
						data.m_nOrigin == m_structTraktIdentifer.nOrigin)
				{
					// Reset to last item in m_vecTraktData
					m_nDBIndex = i;
					break;
				}	// if (data.m_nRegionID == m_structTraktIdentifer.nRegionID &&
			}	// for (UINT i = 0;i < m_vecTraktData.size();i++)
			// Populate data on User interface
			populateData(m_nDBIndex);
			if (reset == RESET_TO_JUST_ENTERED_SET_NB)
			{
				setNavigationButtons(m_nDBIndex > 0,
					   								m_nDBIndex < (m_vecTraktData.size()-1));
			}
			else if (reset == RESET_TO_JUST_ENTERED_NO_NB)
			{
				setNavigationButtons(FALSE,FALSE);
			}
		}	// if (reset == RESET_TO_JUST_ENTERED_SET_NB)
	}	// if (m_vecTraktData.size() > 0)
	else 
	{
		setNavigationButtons(FALSE,FALSE);
		populateData(-1);	// Clear data
	}
}




