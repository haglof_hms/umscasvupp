#pragma once

#include "Resource.h"
#include "XHTMLStatic.h"
#include "UMHmsMisc.h"
// CMyMessageDlg dialog

class CMyMessageDlg : public CDialog
{
	DECLARE_DYNAMIC(CMyMessageDlg)

	CString m_sCaption;
	CString m_sOKBtn;
	CString m_sCancelBtn;
	CString m_sMsgText;
public:
	CMyMessageDlg(CWnd* pParent = NULL);   // standard constructor
	CMyMessageDlg(LPCTSTR cap,LPCTSTR ok_btn,LPCTSTR cancel_btn,LPCTSTR msg,CWnd* pParent = NULL); 
	virtual ~CMyMessageDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

	CXHTMLStatic m_wndHTML;

	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

protected:
	//{{AFX_VIRTUAL(CComfirmationDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

class CMyFileMessageDlg : public CDialog
{
	DECLARE_DYNAMIC(CMyFileMessageDlg)

	CString m_sCaption;
	CString m_sYesBtn;
	CString m_sNoBtn;
	CString m_sYesToAllBtn;
	CString m_sNoToAllBtn;
	CString m_sRename;
	CString m_sMsgText;
public:
	CMyFileMessageDlg(CWnd* pParent = NULL);   // standard constructor
	CMyFileMessageDlg(CString cap, CString yes_btn, CString no_btn, CString yes_to_all, CString no_to_all, CString msg, CString rename, CWnd* pParent = NULL); 
	virtual ~CMyFileMessageDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG3 };

	CXHTMLStatic m_wndHTML;

	CButton m_wndYesBtn;
	CButton m_wndNoBtn;
	CButton m_wndYesToAllBtn;
	CButton m_wndNoToAllBtn;
	CButton m_wndRename;
protected:
	//{{AFX_VIRTUAL(CComfirmationDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonRename();
};
