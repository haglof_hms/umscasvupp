#include "stdafx.h"
#include "MDIScaSvuppSettingsFormView.h"
#include "ResLangFileReader.h"
#include "XBrowseForFolder.h"


IMPLEMENT_DYNCREATE(CScaSvuppSettingsFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CScaSvuppSettingsFormView, CXTResizeFormView)
	ON_WM_COPYDATA()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_WM_SHOWWINDOW()
	ON_MESSAGE(XTPWM_PROPERTYGRID_NOTIFY, OnValueChanged)
//	ON_WM_CREATE()
END_MESSAGE_MAP()

CScaSvuppSettingsFormView::CScaSvuppSettingsFormView()
	: CXTResizeFormView(CScaSvuppSettingsFormView::IDD)
{

}

CScaSvuppSettingsFormView::~CScaSvuppSettingsFormView()
{

}

void CScaSvuppSettingsFormView::OnClose()
{
}


BOOL CScaSvuppSettingsFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CScaSvuppSettingsFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}


void CScaSvuppSettingsFormView::OnSize(UINT nType,int cx,int cy)
{
	CView::OnSize(nType, cx, cy);

}


void CScaSvuppSettingsFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();
	RECT rect;
	::SetCursor(::LoadCursor(NULL,IDC_WAIT));

//	SetScaleToFitSize(CSize(90, 1));

	//Ladda s�kv�gar
	m_sFilepathPlant=regGetStr(REG_ROOT,
		PROGRAM_NAME,
		REG_SCASVUPP_XML_PLANT_FILEPATH_KEY_NAME,
		_T(""));

	m_sFilepathMark=regGetStr(REG_ROOT,
		PROGRAM_NAME,
		REG_SCASVUPP_XML_MARK_FILEPATH_KEY_NAME,
		_T(""));

	m_sFilepathRoj=regGetStr(REG_ROOT,
		PROGRAM_NAME,
		REG_SCASVUPP_XML_ROJ_FILEPATH_KEY_NAME,
		_T(""));
													
	m_sLangAbbrev = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sLangAbbrev,LANGUAGE_FN_EXT);
	setLanguage();
	GetClientRect(&rect);
	SetWindowText(m_sCaption);

		// get the size of the placeholder, this will be used when creating the grid.
	
	if ( m_wndPropertyGrid.Create( rect, this, IDC_PROPERTY_GRID ) )
	{
		m_wndPropertyGrid.SetTheme(xtpGridThemeOffice2003);
		m_wndPropertyGrid.SetVariableItemsHeight(TRUE);
		if (fileExists(m_sLangFN))
		{	
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				// create global settings category.
				CXTPPropertyGridItem* pPaths = m_wndPropertyGrid.AddCategory(xml->str(IDS_STRING8583));
				// add child items to category.
				m_pItemPathPlant = pPaths->AddChildItem(new CCustomItemFileBox(xml->str(IDS_STRING8584), m_sFilepathPlant));
				m_pItemPathPlant->SetDescription(xml->str(IDS_STRING8587));
				m_pItemPathMark = pPaths->AddChildItem(new CCustomItemFileBox(xml->str(IDS_STRING8585), m_sFilepathMark));
				m_pItemPathMark->SetDescription(xml->str(IDS_STRING8587));
				m_pItemPathRoj = pPaths->AddChildItem(new CCustomItemFileBox(xml->str(IDS_STRING8586), m_sFilepathRoj));
				m_pItemPathRoj->SetDescription(xml->str(IDS_STRING8587));
				pPaths->Expand();
			}
			delete xml;
		}
	}
		// set the resize
	SetResize(IDC_PROPERTY_GRID, SZ_TOP_LEFT, SZ_BOTTOM_RIGHT);

	// resize the window to match the frame
	GetParentFrame()->GetClientRect(&rect);
	OnSize(1, rect.right, rect.bottom);

	UpdateData(FALSE);
	::SetCursor(::LoadCursor(NULL,IDC_ARROW));
}


void CScaSvuppSettingsFormView::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_sCaption=xml->str(IDS_STRING8582);
		}
		delete xml;
	}
}

void CScaSvuppSettingsFormView::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTResizeFormView::OnShowWindow(bShow,nStatus);
}

LRESULT CScaSvuppSettingsFormView::OnValueChanged(WPARAM wParam, LPARAM lParam)
{
	int nGridAction = (int)wParam;
	CString sPath=_T("");
	CXTPPropertyGridItem* pItem = (CXTPPropertyGridItem*)lParam;
	ASSERT(pItem);

	switch (nGridAction)
	{
		case XTP_PGN_ITEMVALUE_CHANGED:
		{
			if(pItem == m_pItemPathPlant)
			{
				sPath = m_pItemPathPlant->GetValue();
				if(sPath.GetAt(sPath.GetLength()-1) != '\\')
					sPath += _T("\\");
				regSetStr(REG_ROOT,PROGRAM_NAME,REG_SCASVUPP_XML_PLANT_FILEPATH_KEY_NAME,sPath);
				
			}
			else if(pItem == m_pItemPathMark)
			{
				sPath = m_pItemPathMark->GetValue();
				if(sPath.GetAt(sPath.GetLength()-1) != '\\')
					sPath += _T("\\");
				regSetStr(REG_ROOT,PROGRAM_NAME,REG_SCASVUPP_XML_MARK_FILEPATH_KEY_NAME,sPath);
			}
			else if(pItem == m_pItemPathRoj)
			{
				sPath = m_pItemPathRoj->GetValue();
				if(sPath.GetAt(sPath.GetLength()-1) != '\\')
					sPath += _T("\\");
				regSetStr(REG_ROOT,PROGRAM_NAME,REG_SCASVUPP_XML_ROJ_FILEPATH_KEY_NAME,sPath);
			}
		}
		break;

		case XTP_PGN_SELECTION_CHANGED:
		{
		}
		break;
	}

	return FALSE;
}


///////////////////////////////////////////////////////////////////////////////
CCustomItemFileBox::CCustomItemFileBox(CString strCaption, CString strValue)
	: CXTPPropertyGridItem(strCaption, strValue)
{
	m_nFlags = xtpGridItemHasExpandButton|xtpGridItemHasEdit;
}

//void CCustomItemFileBox::OnInplaceButtonDown()
void CCustomItemFileBox::OnInplaceButtonDown(CXTPPropertyGridInplaceButton* /*pButton*/)
{
	TCHAR szFolder[MAX_PATH*2];
	szFolder[0] = _T('\0');
	BOOL bRet = XBrowseForFolder(GetParentItem()->GetGrid()->m_hWnd,
								 GetValue(),
								 szFolder,
								 sizeof(szFolder)/sizeof(TCHAR)-2);

	if(bRet == TRUE)
	{
		OnValueChanged( szFolder );
		m_pGrid->Invalidate( FALSE );
	}
};
