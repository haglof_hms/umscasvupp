// UMScaGallBas.cpp : Defines the initialization routines for the DLL.
//
#include "StdAfx.h"
#include "UMScaSvupp.h"
#include "Resource.h"

#include "MDIFrameDoc.h"
#include "UMScaSvuppFrames.h"
#include "UMScaSvuppTraktFormView.h"
#include "UMScaSvuppUrvalPlant.h"
#include "UMScaSvuppUrvalMark.h"
#include "UMScaSvuppUrvalRoj.h"
#include "MDIScaSvuppSettingsFrame.h"
#include "MDIScaSvuppSettingsFormView.h"

#include <afxdllx.h>

static AFX_EXTENSION_MODULE UMScaGallBasDLL = { NULL, NULL };

HINSTANCE g_hInstance = NULL;


extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("UMScaGallBas.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UMScaGallBasDLL, hInstance))
			return 0;
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("UMScaGallBas.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(UMScaGallBasDLL);
	}
	g_hInstance = hInstance;
	return 1;   // ok
}

void DLL_BUILD DoAlterTables(LPCTSTR dbname)
{
	check_if_tables_exists();
}

// Initialize the DLL, register the classes etc
void DLL_BUILD InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &idx,vecINFO_TABLE &info)
{
	
	new CDynLinkLibrary(UMScaGallBasDLL);
	CString sReportSettingsFN;
	CString sLangFN;
	CString sVersion;
	CString sCopyright;
	CString sCompany;
	CString sPath;
	// Setup the searchpath and name of the program.
	// This information is used e.g. in setting up the
	// Language filename in an OpenSuite() function;  051214 p�d
	sLangFN.Format(_T("%s%s"),getLanguageDir(),PROGRAM_NAME);

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW2,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIScaGBTraktFrame),
			RUNTIME_CLASS(CMDIScaGBTraktFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW2,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW3,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CUMScaSvuppUrvalPlantFrame),
			RUNTIME_CLASS(CUMScaSvuppUrvalPlant)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW3,suite,sLangFN,TRUE));


	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW4,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CUMScaSvuppUrvalMarkFrame),
			RUNTIME_CLASS(CUMScaSvuppUrvalMark)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW4,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW5,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CUMScaSvuppUrvalRojFrame),
			RUNTIME_CLASS(CUMScaSvuppUrvalRoj)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW5,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CScaSvuppSettingsFrame),
			RUNTIME_CLASS(CScaSvuppSettingsFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW,suite,sLangFN,TRUE));

	sVersion	= getVersionInfo(g_hInstance,VER_NUMBER);
	sCopyright	= getVersionInfo(g_hInstance,VER_COPYRIGHT);
	sCompany	= getVersionInfo(g_hInstance,VER_COMPANY);

	info.push_back(INFO_TABLE(-999,2 ,
									(TCHAR*)sLangFN.GetBuffer(),
									(TCHAR*)sVersion.GetBuffer(),
									(TCHAR*)sCopyright.GetBuffer(),
									(TCHAR*)sCompany.GetBuffer()));

	// Check if there's a connection set; 080702 p�d
	if (getIsDBConSet() == 1)
	{
		// Testa om tabeller ska skapas
		DoAlterTables(_T(""));
		//check_if_tables_exists();
	}
}

BOOL check_if_tables_exists()
{
	//TCHAR sSQL[127];
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
   //DB_CONNECTION_DATA	m_dbConnectionData;
	CString S;
	CString csT=_T("");
	int nServerConn=0;
	bool check=false;
	
	try
	{
		if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		{
		//Beh�ver inte kolla s� det finns anv�ndarnamn eller l�sen om det �r windows verifiering
		GetAuthentication(&nServerConn);
		if(nServerConn)
			check=(_tcscmp(sUserName,_T("")) != 0 &&	_tcscmp(sPSW,_T("")) != 0);
		else
			check=true;
			if (_tcscmp(sLocation,_T("")) != 0 &&
				_tcscmp(sDBName,_T("")) != 0 &&
				check)
			{
				CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
				if (pDB)
				{
					if (pDB->connectToDatabase(nServerConn,sUserName,sPSW,sLocation,sDBName))
					{
						if (!pDB->existsTableInDB(sDBName,TBL_PLANT_TRAKT))
						{
						}

					}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
					delete pDB;
				}	// if (pDB)
			}	// if (_tcscmp(sLocation,"") != 0 &&
			bReturn = TRUE;
		}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}
