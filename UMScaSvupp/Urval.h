#pragma once
#include "stdafx.h"
#include "UMScaSvuppDB.h"
#include "Resource.h"
#include "UMHmsMisc.h"

#define MAX_NUM_OF_REP_COLUMNS		14
#define NUM_OF_REP_COL_HEADER_PLANT 13
#define NUM_OF_REP_COL_HEADER_MARK	17
#define NUM_OF_REP_COL_HEADER_ROJ	11



//---------------------------------------------------------------------------
// CTraktReportDataRec
//---------------------------------------------------------------------------
class CTraktReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
	int m_nTrakt_Id;
	CString tst;
	CString m_sLangFN;
	CString dat;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

	class CDoubleItem : public CXTPReportRecordItemNumber
	{
		double m_nValue;
	public:
		CDoubleItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0.0;
		}

		CDoubleItem(double nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstof(szText);
				SetValue(m_nValue);
		}

		double getDoubleItem(void)	{ return m_nValue; }
	};

	//////////////////////////////////////////////////////////////////////////
	// Customized record item, used for displaying checkboxes.
	class CCheckItem : public CXTPReportRecordItem
	{
	public:
		// Constructs record item with the initial checkbox value.
		CCheckItem(BOOL bCheck)
		{
			HasCheckbox(TRUE);
			SetChecked(bCheck);
		}

		virtual BOOL getChecked(void)
		{
			return IsChecked()? TRUE: FALSE;
		}

		virtual void setChecked(BOOL bCheck)
		{
			SetChecked(bCheck);
		}
	};


public:
	// Det som visas i traktlistan
	CTraktReportDataRec(int nInventType)
	{
		int i=0;
		m_nIndex = -1;
		switch(nInventType)
		{
		case PLANT_TYPE:
			AddItem(new CCheckItem(FALSE));
			for(i=0;i<NUM_OF_REP_COL_HEADER_PLANT;i++)
				AddItem(new CTextItem(_T("")));
			break;
		case MARK_TYPE:
			AddItem(new CCheckItem(FALSE));
			for(i=0;i<NUM_OF_REP_COL_HEADER_MARK;i++)
				AddItem(new CTextItem(_T("")));
			break;
		}
	}

	CTraktReportDataRec(UINT index,TRAKT_DATA_PLANT t,BOOL check)
	{
		m_nIndex = index;
		//Sparar undan traktid f�r att kunna kolla om en trakt redan finns i listan 
		//och om den d� redan �r satt som checked innan listan uppdateras 
		//med nya trakter
		m_nTrakt_Id=t.m_nTrakt_Id;
		AddItem(new CCheckItem(check));

 		//4
		AddItem(new CTextItem(t.m_sTrakt_Forvaltning));
		//5
		AddItem(new CTextItem(t.m_sTrakt_Distrikt));
		//3
		if(t.m_nTrakt_Ursprung==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_Ursprung));
		//2
		AddItem(new CTextItem(t.m_sTrakt_AtgardsId));
		//6
		AddItem(new CTextItem(t.m_sTrakt_Traktnamn));
		//7
		if(t.m_fTrakt_Areal==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CDoubleItem(t.m_fTrakt_Areal));
		//9
		AddItem(new CTextItem(t.m_sTrakt_Entreprenor));
		//10
		AddItem(new CTextItem(t.m_sTrakt_Lag));
		//1
		AddItem(new CTextItem(t.m_sTrakt_Datum));
		//8
		AddItem(new CTextItem(t.m_sTrakt_Inventerare));

		//11
		if(t.m_nTrakt_MB_typ==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_MB_typ));
		//12
		if(t.m_nTrakt_Mal_PlHa==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_Mal_PlHa));

		// 13
		if(t.m_nTrakt_HogtSnytbaggetryck == -1 || t.m_nTrakt_HogtSnytbaggetryck == 0)
			AddItem(new CTextItem(_T("Nej")));
		else
			AddItem(new CTextItem(_T("Ja")));
	}

	CTraktReportDataRec(UINT index,TRAKT_DATA_MARK t,BOOL check)
	{
		m_nIndex = index;
		//Sparar undan traktid f�r att kunna kolla om en trakt redan finns i listan 
		//och om den d� redan �r satt som checked innan listan uppdateras 
		//med nya trakter
		m_nTrakt_Id=t.m_nTrakt_Id;
		AddItem(new CCheckItem(check));
		//1
		AddItem(new CTextItem(t.m_sTrakt_Forvaltning));
		//2
		AddItem(new CTextItem(t.m_sTrakt_Distrikt));
		//3
		if(t.m_nTrakt_Ursprung==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_Ursprung));
		//4
		AddItem(new CTextItem(t.m_sTrakt_AtgardsId));
		//5
		AddItem(new CTextItem(t.m_sTrakt_Traktnamn));
		//6
		if(t.m_fTrakt_Areal==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CDoubleItem(t.m_fTrakt_Areal));
		//7
		AddItem(new CTextItem(t.m_sTrakt_Entreprenor));
		//8
		AddItem(new CTextItem(t.m_sTrakt_Lag));
		//12
		AddItem(new CTextItem(t.m_sTrakt_Datum));
		//13
		AddItem(new CTextItem(t.m_sTrakt_Inventerare));
		//9
		if(t.m_nTrakt_Metod==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_Metod));
		//10
		AddItem(new CTextItem(t.m_sTrakt_Aggr));
		//11
		if(t.m_nTrakt_MalMbStHa==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_MalMbStHa));

		// 14
		if(t.m_nTrakt_HogtSnytbaggetryck == -1 || t.m_nTrakt_HogtSnytbaggetryck == 0)
			AddItem(new CTextItem(_T("Nej")));
		else
			AddItem(new CTextItem(_T("Ja")));

		// 15
		if(t.m_nTrakt_SumSkadadeFornminnen==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_SumSkadadeFornminnen));

		// 16
		if(t.m_nTrakt_SumSkadadeKulturminnen==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_SumSkadadeKulturminnen));

		// 17
		if(t.m_nTrakt_SumKorskador==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_SumKorskador));
	}

	CTraktReportDataRec(UINT index,TRAKT_DATA_ROJ t,BOOL check)
	{
		m_nIndex = index;
		//Sparar undan traktid f�r att kunna kolla om en trakt redan finns i listan 
		//och om den d� redan �r satt som checked innan listan uppdateras 
		//med nya trakter
		m_nTrakt_Id=t.m_nTrakt_Id;
		AddItem(new CCheckItem(check));

 		//1
		AddItem(new CTextItem(t.m_sTrakt_Forvaltning));
		//2
		AddItem(new CTextItem(t.m_sTrakt_Distrikt));
		//3
		if(t.m_nTrakt_Ursprung==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_Ursprung));
		//4
		AddItem(new CTextItem(t.m_sTrakt_AtgardsId));
		//5
		AddItem(new CTextItem(t.m_sTrakt_Traktnamn));
		//6
		if(t.m_fTrakt_Areal==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CDoubleItem(t.m_fTrakt_Areal));
		//7
		AddItem(new CTextItem(t.m_sTrakt_Entreprenor));
		//8
		AddItem(new CTextItem(t.m_sTrakt_Lag));
		//9
		AddItem(new CTextItem(t.m_sTrakt_Datum));
		//10
		AddItem(new CTextItem(t.m_sTrakt_Inventerare));
		//11
		AddItem(new CTextItem(t.m_sTrakt_SI));
	}

	int getTraktId(void)
	{
	return m_nTrakt_Id;
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	double getColumnDouble(int item)	
	{ 
		return ((CDoubleItem*)GetItem(item))->getDoubleItem();
	}

	BOOL getColumnCheck(int item)
	{
		return ((CCheckItem*)GetItem(item))->getChecked();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}

	
};



//---------------------------------------------------------------------------
// CTraktReportDataRec Plantering Urvalsf�lt, F�rv, Distr, Entr etc...
//---------------------------------------------------------------------------
class CTraktReportDataRec_Urval : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
	CString tst;
	CString m_sLangFN;
	CString dat;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
		public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

	class CDoubleItem : public CXTPReportRecordItemNumber
	{
		double m_nValue;
	public:
		CDoubleItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0.0;
		}

		CDoubleItem(double nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstof(szText);
				SetValue(m_nValue);
		}

		double getDoubleItem(void)	{ return m_nValue; }
	};

	//////////////////////////////////////////////////////////////////////////
	// Customized record item, used for displaying checkboxes.
	class CCheckItem : public CXTPReportRecordItem
	{
	public:
		// Constructs record item with the initial checkbox value.
		CCheckItem(BOOL bCheck)
		{
			HasCheckbox(TRUE);
			SetChecked(bCheck);
		}

		virtual BOOL getChecked(void)
		{
			return IsChecked()? TRUE: FALSE;
		}

		virtual void setChecked(BOOL bCheck)
		{
			SetChecked(bCheck);
		}
	};


public:
	// Det som visas i traktlistan
	CTraktReportDataRec_Urval(void)
	{
		m_nIndex = -1;
		AddItem(new CCheckItem(FALSE));
		AddItem(new CTextItem(_T("")));

	}

	//Urvalsdata Plantering
	CTraktReportDataRec_Urval(UINT index,TRAKT_DATA_PLANT t,int var,BOOL checked)
	{
		m_nIndex = index;
		AddItem(new CCheckItem(checked));
		switch(var)
		{
		case 0:	
			AddItem(new CTextItem(t.m_sTrakt_Forvaltning));
			break;
		case 1:	
			AddItem(new CTextItem(t.m_sTrakt_Distrikt));
			break;
		case 2:	
			tst.Format(_T("%d"),t.m_nTrakt_Ursprung);
			AddItem(new CTextItem(tst));
			break;
		case 3:	
			AddItem(new CTextItem(t.m_sTrakt_AtgardsId));
			break;
		case 4:	
			AddItem(new CTextItem(t.m_sTrakt_Entreprenor));
			break;
		case 5:	
			AddItem(new CTextItem(t.m_sTrakt_Lag));
			break;
		case 6:	
			AddItem(new CTextItem(t.m_sTrakt_Inventerare));
			break;
		case 7:	
			tst.Format(_T("%d"),t.m_nTrakt_MB_typ);
			AddItem(new CTextItem(tst));
			break;

		}
	}

	//Urvalsdata Markberedning
	CTraktReportDataRec_Urval(UINT index,TRAKT_DATA_MARK t,int var,BOOL checked)
	{
		m_nIndex = index;
		AddItem(new CCheckItem(checked));
		switch(var)
		{
		case 0:	
			AddItem(new CTextItem(t.m_sTrakt_Forvaltning));
			break;
		case 1:	
			AddItem(new CTextItem(t.m_sTrakt_Distrikt));
			break;
		case 2:	
			tst.Format(_T("%d"),t.m_nTrakt_Ursprung);
			AddItem(new CTextItem(tst));
			break;
		case 3:	
			AddItem(new CTextItem(t.m_sTrakt_AtgardsId));
			break;
		case 4:	
			AddItem(new CTextItem(t.m_sTrakt_Entreprenor));
			break;
		case 5:	
			AddItem(new CTextItem(t.m_sTrakt_Lag));
			break;
		case 6:	
			AddItem(new CTextItem(t.m_sTrakt_Inventerare));
			break;
		case 7:	
			tst.Format(_T("%d"),t.m_nTrakt_Metod);
			AddItem(new CTextItem(tst));
			break;
		case 8:	
			AddItem(new CTextItem(t.m_sTrakt_Aggr));
			break;

		}
	}

	//Urvalsdata R�jning
	CTraktReportDataRec_Urval(UINT index,TRAKT_DATA_ROJ t,int var,BOOL checked)
	{
		m_nIndex = index;
		AddItem(new CCheckItem(checked));
		switch(var)
		{
		case 0:	
			AddItem(new CTextItem(t.m_sTrakt_Forvaltning));
			break;
		case 1:	
			AddItem(new CTextItem(t.m_sTrakt_Distrikt));
			break;
		case 2:	
			tst.Format(_T("%d"),t.m_nTrakt_Ursprung);
			AddItem(new CTextItem(tst));
			break;
		case 3:	
			AddItem(new CTextItem(t.m_sTrakt_AtgardsId));
			break;
		case 4:	
			AddItem(new CTextItem(t.m_sTrakt_Entreprenor));
			break;
		case 5:	
			AddItem(new CTextItem(t.m_sTrakt_Lag));
			break;
		case 6:	
			AddItem(new CTextItem(t.m_sTrakt_Inventerare));
			break;
		}
	}


	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	double getColumnDouble(int item)	
	{ 
		return ((CDoubleItem*)GetItem(item))->getDoubleItem();
	}

	BOOL getColumnCheck(int item)
	{
		return ((CCheckItem*)GetItem(item))->getChecked();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};


//---------------------------------------------------------------------------
// CTraktViewData
//---------------------------------------------------------------------------
class CTraktViewData : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
	int idx,tmpIdx;
	CString tst;
protected:
	
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* , LPCTSTR szText)
		{
			if(_tcslen(szText)==0)
				m_nValue=MY_NULL;
			else
				m_nValue = _tstoi(szText);
			SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	
		{ 

				return m_sText; 
		
		}
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

	class CDoubleItem : public CXTPReportRecordItemNumber
	{
		double m_nValue;
	public:
		CDoubleItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0.0;
		}

		CDoubleItem(double nValue) : CXTPReportRecordItemNumber(nValue,_T("%.1f"))	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			if(_tcslen(szText)==0)
				m_nValue=MY_NULL;
			else
				m_nValue = _tstof(szText);
				SetValue(m_nValue);
		}

		double getDoubleItem(void)	{ return m_nValue; }
		void setDoubleItem(double nValue)	
		{ 
			m_nValue = nValue; 
			SetValue(m_nValue);
		}
	};

public:
	CTraktViewData()
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));		
		AddItem(new CTextItem(_T("")));		
		AddItem(new CTextItem(_T("")));		
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));		
		AddItem(new CTextItem(_T("")));		
		AddItem(new CTextItem(_T("")));		

	}

	CTraktViewData(TRAKT_DATA_PLANT t)
	{
		if(t.m_nTrakt_GP1==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_GP1));
		if(t.m_nTrakt_GP2==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_GP2));
		if(t.m_nTrakt_GP3==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_GP3));
		if(t.m_nTrakt_GP4==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_GP4));
		if(t.m_nTrakt_GP5==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_GP5));
		if(t.m_nTrakt_GP6==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_GP6));
		if(t.m_nTrakt_EjGP1==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_EjGP1));
		if(t.m_nTrakt_EjGP2==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_EjGP2));
		if(t.m_nTrakt_EjGP3==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_EjGP3));
		if(t.m_nTrakt_EjGP4==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_EjGP4));
		if(t.m_nTrakt_EjGP5==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_EjGP5));
		if(t.m_nTrakt_EjGP6==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_EjGP6));
		if(t.m_nTrakt_BattrePlantp==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_BattrePlantp));
		if(t.m_nTrakt_OutnPlantp==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_OutnPlantp));
		if(t.m_nTrakt_Plantvard==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_Plantvard));
		if(t.m_nTrakt_Stadat==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_Stadat));
		if(t.m_nTrakt_SkildaS==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_SkildaS));
		if(t.m_nTrakt_Redovisat==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(t.m_nTrakt_Redovisat));

	}

	CTraktViewData(PLOT_DATA_PLANT p)
	{
		// 1
		if(p.m_nPlot_YtNr==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(p.m_nPlot_YtNr));
		// 2
		if(p.m_fPlot_KoordinatX==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CDoubleItem(p.m_fPlot_KoordinatX));
		// 3
		if(p.m_fPlot_KoordinatY==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CDoubleItem(p.m_fPlot_KoordinatY));
		// 4
		if(p.m_nPlot_Med_mineral==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(p.m_nPlot_Med_mineral));
		// 5
		if(p.m_nPlot_Utan_mineral==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(p.m_nPlot_Utan_mineral));
		// 6
		if(p.m_nPlot_Hogt_mineral==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(p.m_nPlot_Hogt_mineral));
		// 7
		if(p.m_nPlot_Hogt_humus==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(p.m_nPlot_Hogt_humus));
		// 8
		if(p.m_nPlot_Behandlad==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(p.m_nPlot_Behandlad));
		// 9
		if(p.m_nPlot_Summa_GK==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(p.m_nPlot_Summa_GK));
		// 10
		if(p.m_nPlot_Ej_djup==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(p.m_nPlot_Ej_djup));
		// 11
		if(p.m_nPlot_Ej_punkt==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(p.m_nPlot_Ej_punkt));
		// 12
		if(p.m_nPlot_Ej_10cm==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(p.m_nPlot_Ej_10cm));
		// 13
		if(p.m_nPlot_Ej_avstand==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(p.m_nPlot_Ej_avstand));
		// 14
		if(p.m_nPlot_Hogt==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(p.m_nPlot_Hogt));
		// 15
		if(p.m_nPlot_Ovriga==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(p.m_nPlot_Ovriga));
		// 16
		if(p.m_nPlot_Summg_EGK==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(p.m_nPlot_Summg_EGK));
		// 17
		if(p.m_nPlot_Battre==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(p.m_nPlot_Battre));
		// 18
		if(p.m_nPlot_Outnyttj==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(p.m_nPlot_Outnyttj));
		// 19
		AddItem(new CTextItem(p.m_sPlot_Anteckningar));
		// 20
		if(p.m_nPlot_Hogt_torv==MY_NULL)
			AddItem(new CTextItem(_T("")));
		else
			AddItem(new CIntItem(p.m_nPlot_Hogt_torv));
	}


	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	void setColumnText(int item,CString text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

	double getColumnDouble(int item)	
	{ 
		return ((CDoubleItem*)GetItem(item))->getDoubleItem();
	}

	void setColumnDouble(int item,double data)	
	{ 
		((CDoubleItem*)GetItem(item))->setDoubleItem(data);
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};