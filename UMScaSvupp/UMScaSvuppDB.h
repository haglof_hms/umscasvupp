#if !defined(AFX_UMSCAGALLBASDB_H)
#define AFX_UMSCAGALLBASDB_H

#include "StdAfx.h"
#include "DBBaseClass_SQLApi.h"

#define MY_NULL											-1
//#include "AddDataToDataBase.h"



void setupForDBConnection(HWND hWndReciv,HWND hWndSend);

//////////////////////////////////////////////////////////////////////////////////
// CGallBasDBDB; Handle ALL transactions for GallBas

#define PAGE_NR_TRAKT				0
#define PAGE_NR_YTA					1

#define NUM_OF_PLANT_TRAKT_VAR	18
#define NUM_OF_PLANT_TRAKT_YTVAR	20


typedef struct _plot_data_plant
{
int m_nPlot_Trakt_Id;
int m_nPlot_YtNr;
int m_nPlot_Standort;
double m_fPlot_KoordinatX;
double m_fPlot_KoordinatY;
int m_nPlot_Med_mineral;
int m_nPlot_Utan_mineral;
int m_nPlot_Hogt_mineral;
int m_nPlot_Hogt_humus;
int m_nPlot_Behandlad;
int m_nPlot_Summa_GK;
int m_nPlot_Ej_djup;
int m_nPlot_Ej_punkt;
int m_nPlot_Ej_10cm;
int m_nPlot_Ej_avstand;
int m_nPlot_Hogt;
int m_nPlot_Ovriga;
int m_nPlot_Summg_EGK;
int m_nPlot_Battre;
int m_nPlot_Outnyttj;
CString m_sPlot_Anteckningar;
int m_nPlot_Hogt_torv;

	_plot_data_plant(void)
	{
		m_nPlot_Trakt_Id=MY_NULL;
		m_nPlot_YtNr=MY_NULL;
		m_nPlot_Standort = MY_NULL;
		m_fPlot_KoordinatX=MY_NULL;
		m_fPlot_KoordinatY=MY_NULL;
		m_nPlot_Med_mineral=MY_NULL;
		m_nPlot_Utan_mineral=MY_NULL;
		m_nPlot_Hogt_mineral=MY_NULL;
		m_nPlot_Hogt_humus=MY_NULL;
		m_nPlot_Behandlad=MY_NULL;
		m_nPlot_Summa_GK=MY_NULL;
		m_nPlot_Ej_djup=MY_NULL;
		m_nPlot_Ej_punkt=MY_NULL;
		m_nPlot_Ej_10cm=MY_NULL;
		m_nPlot_Ej_avstand=MY_NULL;
		m_nPlot_Hogt=MY_NULL;
		m_nPlot_Ovriga=MY_NULL;
		m_nPlot_Summg_EGK=MY_NULL;
		m_nPlot_Battre=MY_NULL;
		m_nPlot_Outnyttj=MY_NULL;
		m_sPlot_Anteckningar=_T("");
		m_nPlot_Hogt_torv=MY_NULL;
	}

	_plot_data_plant(
int nPlot_Trakt_Id,
int nPlot_YtNr,
int nPlot_Standort,
double fPlot_KoordinatX,
double fPlot_KoordinatY,
int nPlot_Med_mineral,
int nPlot_Utan_mineral,
int nPlot_Hogt_mineral,
int nPlot_Hogt_humus,
int nPlot_Behandlad,
int nPlot_Summa_GK,
int nPlot_Ej_djup,
int nPlot_Ej_punkt,
int nPlot_Ej_10cm,
int nPlot_Ej_avstand,
int nPlot_Hogt,
int nPlot_Ovriga,
int nPlot_Summg_EGK,
int nPlot_Battre,
int nPlot_Outnyttj,
CString sPlot_Anteckningar,
int nPlot_Hogt_torv
	)
	{
		m_nPlot_Trakt_Id=nPlot_Trakt_Id;
		m_nPlot_YtNr=nPlot_YtNr;
		m_nPlot_Standort = nPlot_Standort;
		m_fPlot_KoordinatX=fPlot_KoordinatX;
		m_fPlot_KoordinatY=fPlot_KoordinatY;
		m_nPlot_Med_mineral=nPlot_Med_mineral;
		m_nPlot_Utan_mineral=nPlot_Utan_mineral;
		m_nPlot_Hogt_mineral=nPlot_Hogt_mineral;
		m_nPlot_Hogt_humus=nPlot_Hogt_humus;
		m_nPlot_Behandlad=nPlot_Behandlad;
		m_nPlot_Summa_GK=nPlot_Summa_GK;
		m_nPlot_Ej_djup=nPlot_Ej_djup;
		m_nPlot_Ej_punkt=nPlot_Ej_punkt;
		m_nPlot_Ej_10cm=nPlot_Ej_10cm;
		m_nPlot_Ej_avstand=nPlot_Ej_avstand;
		m_nPlot_Hogt=nPlot_Hogt;
		m_nPlot_Ovriga=nPlot_Ovriga;
		m_nPlot_Summg_EGK=nPlot_Summg_EGK;
		m_nPlot_Battre=nPlot_Battre;
		m_nPlot_Outnyttj=nPlot_Outnyttj;
		m_sPlot_Anteckningar=sPlot_Anteckningar;
		m_nPlot_Hogt_torv=nPlot_Hogt_torv;
	}
} PLOT_DATA_PLANT;
typedef std::vector<PLOT_DATA_PLANT> vecPlotData_Plant;


typedef struct _plot_data_mark
{
int m_nPlot_Trakt_Id;
int m_nPlot_YtNr;
double m_fPlot_KoordinatX;
double m_fPlot_KoordinatY;
CString m_sPlot_Anteckningar;
int m_nPlot_Rader;
CString m_sPlot_Standort;
int m_nPlot_poeng1;
int m_nPlot_poeng2;
int m_nPlot_poengM;
int m_nPlot_poengHT;
int m_nPlot_poeng4;
int m_nPlot_poeng5;
int m_nPlot_poeng7x7;
int m_nPlot_poeng3;
int m_nPlot_PoengSkadadeLagor;
int m_nPlot_PoengTorv3;
int m_nPlot_PoengMineral3;
int m_nPlot_PoengHumus3;
int m_nPlot_PoengAntalLagor;
int m_nPlot_SkadadeFornminnen;
int m_nPlot_SkadadeKulturminnen;
int m_nPlot_Korskador;

	_plot_data_mark(void)
	{
		m_nPlot_Trakt_Id=MY_NULL;
		m_nPlot_YtNr=MY_NULL;
		m_fPlot_KoordinatX=MY_NULL;
		m_fPlot_KoordinatY=MY_NULL;
		m_sPlot_Anteckningar=_T("");
		m_nPlot_Rader=MY_NULL;
		m_sPlot_Standort=_T("");
		m_nPlot_poeng1=MY_NULL;
		m_nPlot_poeng2=MY_NULL;
		m_nPlot_poengM=MY_NULL;
		m_nPlot_poengHT=MY_NULL;
		m_nPlot_poeng4=MY_NULL;
		m_nPlot_poeng5=MY_NULL;
		m_nPlot_poeng7x7=MY_NULL;
		m_nPlot_poeng3=MY_NULL;
		m_nPlot_PoengSkadadeLagor=MY_NULL;
		m_nPlot_PoengTorv3=MY_NULL;
		m_nPlot_PoengMineral3=MY_NULL;
		m_nPlot_PoengHumus3=MY_NULL;
		m_nPlot_PoengAntalLagor=MY_NULL;
		m_nPlot_SkadadeFornminnen = MY_NULL;
		m_nPlot_SkadadeKulturminnen = MY_NULL;
		m_nPlot_Korskador = MY_NULL;
	}

	_plot_data_mark(
int nPlot_Trakt_Id,
int nPlot_YtNr,
double fPlot_KoordinatX,
double fPlot_KoordinatY,
CString sPlot_Anteckningar,
int nPlot_Rader,
CString sPlot_Standort,
int nPlot_poeng1,
int nPlot_poeng2,
int nPlot_poengM,
int nPlot_poengHT,
int nPlot_poeng4,
int nPlot_poeng5,
int nPlot_poeng7x7,
int nPlot_poeng3,
int nPlot_PoengSkadadeLagor,
int nPlot_PoengTorv3,
int nPlot_PoengMineral3,
int nPlot_PoengHumus3,
int nPlot_PoengAntalLagor,
int nPlot_SkadadeFornminnen,
int nPlot_SkadadeKulturminnen,
int nPlot_Korskador
	)
	{
m_nPlot_Trakt_Id=nPlot_Trakt_Id;
m_nPlot_YtNr=nPlot_YtNr;
m_fPlot_KoordinatX=fPlot_KoordinatX;
m_fPlot_KoordinatY=fPlot_KoordinatY;
m_sPlot_Anteckningar=sPlot_Anteckningar;
m_nPlot_Rader=nPlot_Rader;
m_sPlot_Standort=sPlot_Standort;
m_nPlot_poeng1=nPlot_poeng1;
m_nPlot_poeng2=nPlot_poeng2;
m_nPlot_poengM=nPlot_poengM;
m_nPlot_poengHT=nPlot_poengHT;
m_nPlot_poeng4=nPlot_poeng4;
m_nPlot_poeng5=nPlot_poeng5;
m_nPlot_poeng7x7=nPlot_poeng7x7;
m_nPlot_poeng3=nPlot_poeng3;
m_nPlot_PoengSkadadeLagor=nPlot_PoengSkadadeLagor;
m_nPlot_PoengTorv3=nPlot_PoengTorv3;
m_nPlot_PoengMineral3=nPlot_PoengMineral3;
m_nPlot_PoengHumus3=nPlot_PoengHumus3;
m_nPlot_PoengAntalLagor=nPlot_PoengAntalLagor;
m_nPlot_SkadadeFornminnen = nPlot_SkadadeFornminnen;
m_nPlot_SkadadeKulturminnen = nPlot_SkadadeKulturminnen;
m_nPlot_Korskador = nPlot_Korskador;
	}
} PLOT_DATA_MARK;
typedef std::vector<PLOT_DATA_MARK> vecPlotData_Mark;


typedef struct _plot_data_roj
{
	int m_nPlot_Trakt_Id;	
	int m_nPlot_YtNr;
	double m_fPlot_KoordinatX;
	double m_fPlot_KoordinatY;
	int m_nPlot_StPerHa;
	int m_nPlot_Rojd;
	int m_nPlot_Rojbehov;
	int m_nPlot_TrslVal;
	int m_nPlot_Stamval;
	int m_nPlot_Stamantal;
	int m_nPlot_AntalHsTall;
	int m_nPlot_MhojdTall;
	int m_nPlot_AntalHsGran;
	int m_nPlot_MhojdGran;
	int m_nPlot_AntalHsCont;
	int m_nPlot_MhojdCont;
	int m_nPlot_AntalHsBjork;
	int m_nPlot_MhojdBjork;
	int m_nPlot_AntalBi;
	int m_nPlot_MhojdBi;
	int m_nPlot_AntalRo;
	int m_nPlot_MhojdRo;
	int m_nPlot_AntalOr;
	int m_nPlot_MhojdOr;
	int m_nPlot_AntalHa;
	int m_nPlot_HansynsSt;
	int m_nPlot_Myr;
	int m_nPlot_Vattendrag;
	int m_nPlot_Sjo;
	int m_nPlot_Annat;
	int m_nPlot_Surdrag;
	int m_nPlot_OvrigaBio;
	int m_nPlot_Kultur;
	int m_nPlot_AlgskadorNya;
	int m_nPlot_AlgskadorGamla;
	int m_nPlot_Knackesjuka;
	int m_nPlot_Gremeniella;
	int m_nPlot_Ovriga;
	CString m_sPlot_Anteckningar;
	
	_plot_data_roj(void)
	{
	int m_nPlot_Trakt_Id=MY_NULL;
	int m_nPlot_YtNr=MY_NULL;
	double m_fPlot_KoordinatX=MY_NULL;
	double m_fPlot_KoordinatY=MY_NULL;
	int m_nPlot_StPerHa=MY_NULL;
	int m_nPlot_Rojd=MY_NULL;
	int m_nPlot_Rojbehov=MY_NULL;
	int m_nPlot_TrslVal=MY_NULL;
	int m_nPlot_Stamval=MY_NULL;
	int m_nPlot_Stamantal=MY_NULL;
	int m_nPlot_AntalHsTall=MY_NULL;
	int m_nPlot_MhojdTall=MY_NULL;
	int m_nPlot_AntalHsGran=MY_NULL;
	int m_nPlot_MhojdGran=MY_NULL;
	int m_nPlot_AntalHsCont=MY_NULL;
	int m_nPlot_MhojdCont=MY_NULL;
	int m_nPlot_AntalHsBjork=MY_NULL;
	int m_nPlot_MhojdBjork=MY_NULL;
	int m_nPlot_AntalBi=MY_NULL;
	int m_nPlot_MhojdBi=MY_NULL;
	int m_nPlot_AntalRo=MY_NULL;
	int m_nPlot_MhojdRo=MY_NULL;
	int m_nPlot_AntalOr=MY_NULL;
	int m_nPlot_MhojdOr=MY_NULL;
	int m_nPlot_AntalHa=MY_NULL;
	int m_nPlot_HansynsSt=MY_NULL;
	int m_nPlot_Myr=MY_NULL;
	int m_nPlot_Vattendrag=MY_NULL;
	int m_nPlot_Sjo=MY_NULL;
	int m_nPlot_Annat=MY_NULL;
	int m_nPlot_Surdrag=MY_NULL;
	int m_nPlot_OvrigaBio=MY_NULL;
	int m_nPlot_Kultur=MY_NULL;
	int m_nPlot_AlgskadorNya=MY_NULL;
	int m_nPlot_AlgskadorGamla=MY_NULL;
	int m_nPlot_Knackesjuka=MY_NULL;
	int m_nPlot_Gremeniella=MY_NULL;
	int m_nPlot_Ovriga=MY_NULL;
	CString m_sPlot_Anteckningar=_T("");
	}
}
 PLOT_DATA_ROJ;
typedef std::vector<PLOT_DATA_ROJ> vecPlotData_Roj;

typedef struct _trakt_index_plant
{
	int m_nTrakt_Id;

	_trakt_index_plant(int nTrakt_Id)
	{
		m_nTrakt_Id=nTrakt_Id;
	}
	_trakt_index_plant()
	{
		m_nTrakt_Id=MY_NULL;
	}

}TRAKT_INDEX_PLANT;
typedef std::vector<TRAKT_INDEX_PLANT> vecTraktIndex_Plant;

typedef struct _trakt_index_mark
{
	int m_nTrakt_Id;

	_trakt_index_mark(int nTrakt_Id)
	{
		m_nTrakt_Id=nTrakt_Id;
	}
	_trakt_index_mark()
	{
		m_nTrakt_Id=MY_NULL;
	}

}TRAKT_INDEX_MARK;
typedef std::vector<TRAKT_INDEX_MARK> vecTraktIndex_Mark;


typedef struct _trakt_index_roj
{
	int m_nTrakt_Id;

	_trakt_index_roj(int nTrakt_Id)
	{
		m_nTrakt_Id=nTrakt_Id;
	}
	_trakt_index_roj()
	{
		m_nTrakt_Id=MY_NULL;
	}

}TRAKT_INDEX_ROJ;
typedef std::vector<TRAKT_INDEX_ROJ> vecTraktIndex_Roj;

// TRAKT DATA STRUCTURES PLANTERING
typedef struct _trakt_data_plant
{
int		m_nTrakt_Id;
int		m_nTrakt_Slutford;
CString	m_sTrakt_Datum;
CString	m_sTrakt_Forvaltning;
CString	m_sTrakt_Distrikt;
int		m_nTrakt_Ursprung;	
double	m_fTrakt_KartaX;
double	m_fTrakt_KartaY;
CString	m_sTrakt_Traktnamn;
double	m_fTrakt_Areal;
//int		m_nTrakt_ObjNr;
CString	m_sTrakt_Inventerare;
CString	m_sTrakt_Entreprenor;
CString	m_sTrakt_Lag;
int		m_nTrakt_MB_typ;
int		m_nTrakt_Mal_PlHa;
int		m_nTrakt_Yta;
CString	m_sTrakt_Anteckningar;

int		m_nTrakt_GP1;
int		m_nTrakt_GP2;
int		m_nTrakt_GP3;
int		m_nTrakt_GP4;
int		m_nTrakt_GP5;
int		m_nTrakt_EjGP1;
int		m_nTrakt_EjGP2;
int		m_nTrakt_EjGP3;
int		m_nTrakt_EjGP4;
int		m_nTrakt_EjGP5;
int		m_nTrakt_EjGP6;
int		m_nTrakt_BattrePlantp;
int		m_nTrakt_OutnPlantp;
int		m_nTrakt_Plantvard;
int		m_nTrakt_Stadat;
int		m_nTrakt_SkildaS;
int		m_nTrakt_Redovisat;
int		m_nTrakt_InvTyp;
int		m_nTrakt_GP6;
CString	m_sTrakt_OwnerId;
	CString m_sTrakt_AtgardsId;
	int m_nTrakt_HogtSnytbaggetryck;

  vecPlotData_Plant	m_vPlotData;
    
  _trakt_data_plant(void){
	  m_nTrakt_Id=MY_NULL;
	  m_nTrakt_InvTyp=MY_NULL;
	  m_nTrakt_Slutford=MY_NULL;
	  m_sTrakt_Datum=_T("");
	  m_sTrakt_Forvaltning=_T("");
	  m_sTrakt_Distrikt=_T("");
	  m_nTrakt_Ursprung=MY_NULL;	
	  m_fTrakt_KartaX=MY_NULL;
	  m_fTrakt_KartaY=MY_NULL;
	  m_sTrakt_Traktnamn=_T("");
	  m_fTrakt_Areal=MY_NULL;
	  //m_nTrakt_ObjNr=MY_NULL;
	  m_sTrakt_Inventerare=_T("");
	  m_sTrakt_Entreprenor=_T("");
	  m_sTrakt_Lag=_T("");
	  m_nTrakt_MB_typ=MY_NULL;
	  m_nTrakt_Mal_PlHa=MY_NULL;
	  m_nTrakt_Yta=MY_NULL;;
	  m_sTrakt_Anteckningar=_T("");

	  m_nTrakt_GP1=MY_NULL;
	  m_nTrakt_GP2=MY_NULL;
	  m_nTrakt_GP3=MY_NULL;
	  m_nTrakt_GP4=MY_NULL;
	  m_nTrakt_GP5=MY_NULL;
	  m_nTrakt_GP6=MY_NULL;
	  m_nTrakt_EjGP1=MY_NULL;
	  m_nTrakt_EjGP2=MY_NULL;
	  m_nTrakt_EjGP3=MY_NULL;
	  m_nTrakt_EjGP4=MY_NULL;
	  m_nTrakt_EjGP5=MY_NULL;
	  m_nTrakt_EjGP6=MY_NULL;
	  m_nTrakt_BattrePlantp=MY_NULL;
	  m_nTrakt_OutnPlantp=MY_NULL;
	  m_nTrakt_Plantvard=MY_NULL;
	  m_nTrakt_Stadat=MY_NULL;
	  m_nTrakt_SkildaS=MY_NULL;
	  m_nTrakt_Redovisat=MY_NULL;
	  m_sTrakt_OwnerId=_T("");
	  m_sTrakt_AtgardsId = _T("");
	  m_nTrakt_HogtSnytbaggetryck = 0;
	  m_vPlotData.clear();
  }

  
  _trakt_data_plant(
	  int		nTrakt_Id,
	  int		nTrakt_Slutford,
	  CString	sTrakt_Datum,
	  CString	sTrakt_Forvaltning,
	  CString	sTrakt_Distrikt,
	  int		nTrakt_Ursprung,	
	  double	fTrakt_KartaX,
	  double	fTrakt_KartaY,
	  CString	sTrakt_Traktnamn,
	  double	fTrakt_Areal,
	  //int		nTrakt_ObjNr,
	  CString	sTrakt_Inventerare,
	  CString	sTrakt_Entreprenor,
	  CString	sTrakt_Lag,
	  int		nTrakt_MB_typ,
	  int		nTrakt_Mal_PlHa,
	  int		nTrakt_Yta,
	  CString	sTrakt_Anteckningar,

	  int		nTrakt_GP1,
	  int		nTrakt_GP2,
	  int		nTrakt_GP3,
	  int		nTrakt_GP4,
	  int		nTrakt_GP5,
	  int		nTrakt_EjGP1,
	  int		nTrakt_EjGP2,
	  int		nTrakt_EjGP3,
	  int		nTrakt_EjGP4,
	  int		nTrakt_EjGP5,
	  int		nTrakt_EjGP6,
	  int		nTrakt_BattrePlantp,
	  int		nTrakt_OutnPlantp,
	  int		nTrakt_Plantvard,
	  int		nTrakt_Stadat,
	  int		nTrakt_SkildaS,
	  int		nTrakt_Redovisat,
	  int		nTrakt_InvTyp,
	  int		nTrakt_GP6,
	  CString	sTrakt_OwnerId,
	  CString	sTrakt_AtgardsId,
	  int		nTrakt_HogtSnytbaggetryck
	  ){
		  m_nTrakt_Id=nTrakt_Id;
		  m_nTrakt_Slutford=nTrakt_Slutford;
		  m_sTrakt_Datum=sTrakt_Datum;
		  m_sTrakt_Forvaltning=sTrakt_Forvaltning;
		  m_sTrakt_Distrikt=sTrakt_Distrikt;
		  m_nTrakt_Ursprung=nTrakt_Ursprung;	
		  m_fTrakt_KartaX=fTrakt_KartaX;
		  m_fTrakt_KartaY=fTrakt_KartaY;
		  m_sTrakt_Traktnamn=sTrakt_Traktnamn;
		  m_fTrakt_Areal=fTrakt_Areal;
		 // m_nTrakt_ObjNr=nTrakt_ObjNr;
		  m_sTrakt_Inventerare=sTrakt_Inventerare;
		  m_sTrakt_Entreprenor=sTrakt_Entreprenor;
		  m_sTrakt_Lag=sTrakt_Lag;
		  m_nTrakt_MB_typ=nTrakt_MB_typ;
		  m_nTrakt_Mal_PlHa=nTrakt_Mal_PlHa;
		  m_nTrakt_Yta=nTrakt_Yta;
		  m_sTrakt_Anteckningar=sTrakt_Anteckningar;
		  m_nTrakt_GP1=nTrakt_GP1;
		  m_nTrakt_GP2=nTrakt_GP2;
		  m_nTrakt_GP3=nTrakt_GP3;
		  m_nTrakt_GP4=nTrakt_GP4;
		  m_nTrakt_GP5=nTrakt_GP5;
		  m_nTrakt_GP6=nTrakt_GP6;
		  m_nTrakt_EjGP1=nTrakt_EjGP1;
		  m_nTrakt_EjGP2=nTrakt_EjGP2;
		  m_nTrakt_EjGP3=nTrakt_EjGP3;
		  m_nTrakt_EjGP4=nTrakt_EjGP4;
		  m_nTrakt_EjGP5=nTrakt_EjGP5;
		  m_nTrakt_EjGP6=nTrakt_EjGP6;
		  m_nTrakt_BattrePlantp=nTrakt_BattrePlantp;
		  m_nTrakt_OutnPlantp=nTrakt_OutnPlantp;
		  m_nTrakt_Plantvard=nTrakt_Plantvard;
		  m_nTrakt_Stadat=nTrakt_Stadat;
		  m_nTrakt_SkildaS=nTrakt_SkildaS;
		  m_nTrakt_Redovisat=nTrakt_Redovisat;
		  m_nTrakt_InvTyp=nTrakt_InvTyp;
		  m_sTrakt_OwnerId=sTrakt_OwnerId;
		  m_sTrakt_AtgardsId = sTrakt_AtgardsId;
		m_nTrakt_HogtSnytbaggetryck = nTrakt_HogtSnytbaggetryck;
  }
} TRAKT_DATA_PLANT;

typedef std::vector<TRAKT_DATA_PLANT> vecTraktData_Plant;




// TRAKT DATA STRUCTURES MARKBEREDNING
typedef struct _trakt_data_mark
{
	int		m_nTrakt_Id;
	int		m_nTrakt_Slutford;
	CString	m_sTrakt_Datum;
	CString	m_sTrakt_Forvaltning;
	CString	m_sTrakt_Distrikt;
	int		m_nTrakt_Ursprung;	
	double	m_fTrakt_KartaX;
	double	m_fTrakt_KartaY;
	CString	m_sTrakt_Traktnamn;
	double	m_fTrakt_Areal;
	//int		m_nTrakt_ObjNr;
	CString	m_sTrakt_Inventerare;
	CString	m_sTrakt_Entreprenor;
	CString	m_sTrakt_Lag;
	int		m_nTrakt_Metod;
	CString	m_sTrakt_Aggr;
	int		m_nTrakt_MalMbStHa;
	CString	m_sTrakt_Anteckningar;

	int		m_nTrakt_Sum1;
	int		m_nTrakt_Sum2;
	int		m_nTrakt_SumM;
	int		m_nTrakt_SumHT;
	int		m_nTrakt_Sum4;
	int		m_nTrakt_Sum5;
	int		m_nTrakt_SumPoang1;
	int		m_nTrakt_SumPoang2;
	int		m_nTrakt_SumPoangM;
	int		m_nTrakt_SumPoangHT;
	int		m_nTrakt_SumPoang4;
	int		m_nTrakt_SumPoang5;
	int		m_nTrakt_MedelKvalitet;
	int		m_nTrakt_GKPerYta;
	int		m_nTrakt_GKPerHa;
	int		m_nTrakt_OnodigaSkador;
	int		m_nTrakt_SkadorFangstgrop;
	int		m_nTrakt_SkadorMark;
	int		m_nTrakt_OljaAvfall;
	int		m_nTrakt_Inlamnat10dagar;
	int		m_nTrakt_Sum3;
	int		m_nTrakt_SumPoang3;
	int		m_nTrakt_Invtyp;
	int		m_nTrakt_SumTorv3;
	int		m_nTrakt_SumMineral3;
	int		m_nTrakt_SumHumus3;
	int		m_nTrakt_SumPoangTorv3;
	int		m_nTrakt_SumPoangMineral3;
	int		m_nTrakt_SumPoangHumus3;
	int		m_nTrakt_PoangTorv3;
	int		m_nTrakt_Sum7x7;
	int		m_nTrakt_SumSkadadeLagor;
	int		m_nTrakt_SumAntalLagor;
	CString	m_sTrakt_OwnerId;
	CString m_sTrakt_AtgardsId;
	int		m_nTrakt_HogtSnytbaggetryck;
	int m_nTrakt_SumSkadadeFornminnen;
	int m_nTrakt_SumSkadadeKulturminnen;
	int m_nTrakt_SumKorskador;

  vecPlotData_Mark	m_vPlotData;
   
_trakt_data_mark(void){
	m_nTrakt_Id=MY_NULL;
	m_nTrakt_Slutford=MY_NULL;
	m_sTrakt_Datum=_T("");
	m_sTrakt_Forvaltning=_T("");
	m_sTrakt_Distrikt=_T("");
	m_nTrakt_Ursprung=MY_NULL;	
	m_fTrakt_KartaX=MY_NULL;
	m_fTrakt_KartaY=MY_NULL;
	m_sTrakt_Traktnamn=_T("");
	m_fTrakt_Areal=MY_NULL;
	//m_nTrakt_ObjNr=MY_NULL;
	m_sTrakt_Inventerare=_T("");
	m_sTrakt_Entreprenor=_T("");
	m_sTrakt_Lag=_T("");
	m_nTrakt_Metod=MY_NULL;
	m_sTrakt_Aggr=_T("");
	m_nTrakt_MalMbStHa=MY_NULL;
	m_sTrakt_Anteckningar=_T("");

	m_nTrakt_Sum1=MY_NULL;
	m_nTrakt_Sum2=MY_NULL;
	m_nTrakt_SumM=MY_NULL;
	m_nTrakt_SumHT=MY_NULL;
	m_nTrakt_Sum4=MY_NULL;
	m_nTrakt_Sum5=MY_NULL;
	m_nTrakt_SumPoang1=MY_NULL;
	m_nTrakt_SumPoang2=MY_NULL;
	m_nTrakt_SumPoangM=MY_NULL;
	m_nTrakt_SumPoangHT=MY_NULL;
	m_nTrakt_SumPoang4=MY_NULL;
	m_nTrakt_SumPoang5=MY_NULL;
	m_nTrakt_MedelKvalitet=MY_NULL;
	m_nTrakt_GKPerYta=MY_NULL;
	m_nTrakt_GKPerHa=MY_NULL;
	m_nTrakt_OnodigaSkador=MY_NULL;
	m_nTrakt_SkadorFangstgrop=MY_NULL;
	m_nTrakt_SkadorMark=MY_NULL;
	m_nTrakt_OljaAvfall=MY_NULL;
	m_nTrakt_Inlamnat10dagar=MY_NULL;
	m_nTrakt_Sum3=MY_NULL;
	m_nTrakt_SumPoang3=MY_NULL;
	m_nTrakt_Invtyp=MY_NULL;
	m_nTrakt_SumTorv3=MY_NULL;
	m_nTrakt_SumMineral3=MY_NULL;
	m_nTrakt_SumHumus3=MY_NULL;
	m_nTrakt_SumPoangTorv3=MY_NULL;
	m_nTrakt_SumPoangMineral3=MY_NULL;
	m_nTrakt_SumPoangHumus3=MY_NULL;
	m_nTrakt_PoangTorv3=MY_NULL;
	m_nTrakt_Sum7x7=MY_NULL;
	m_nTrakt_SumSkadadeLagor=MY_NULL;
	m_nTrakt_SumAntalLagor=MY_NULL;
	m_sTrakt_OwnerId=_T("");
	m_sTrakt_AtgardsId = _T("");
	m_nTrakt_HogtSnytbaggetryck = 0;
	m_nTrakt_SumSkadadeFornminnen = 0;
	m_nTrakt_SumSkadadeKulturminnen = 0;
	m_nTrakt_SumKorskador = 0;
}

  
  _trakt_data_mark(
	int nTrakt_Id,
	int nTrakt_Slutford,
	CString sTrakt_Datum,
	CString sTrakt_Forvaltning,
	CString sTrakt_Distrikt,
	int nTrakt_Ursprung,	
	double fTrakt_KartaX,
	double fTrakt_KartaY,
	CString sTrakt_Traktnamn,
	double fTrakt_Areal,
	//int nTrakt_ObjNr,
	CString sTrakt_Inventerare,
	CString sTrakt_Entreprenor,
	CString sTrakt_Lag,
	int nTrakt_Metod,
	CString sTrakt_Aggr,
	int nTrakt_MalMbStHa,
	CString sTrakt_Anteckningar,

	int nTrakt_Sum1,
	int nTrakt_Sum2,
	int nTrakt_SumM,
	int nTrakt_SumHT,
	int nTrakt_Sum4,
	int nTrakt_Sum5,
	int nTrakt_SumPoang1,
	int nTrakt_SumPoang2,
	int nTrakt_SumPoangM,
	int nTrakt_SumPoangHT,
	int nTrakt_SumPoang4,
	int nTrakt_SumPoang5,
	int nTrakt_MedelKvalitet,
	int nTrakt_GKPerYta,
	int nTrakt_GKPerHa,
	int nTrakt_OnodigaSkador,
	int nTrakt_SkadorFangstgrop,
	int nTrakt_SkadorMark,
	int nTrakt_OljaAvfall,
	int nTrakt_Inlamnat10dagar,
	int nTrakt_Sum3,
	int nTrakt_SumPoang3,
	int nTrakt_Invtyp,
	int nTrakt_SumTorv3,
	int nTrakt_SumMineral3,
	int nTrakt_SumHumus3,
	int nTrakt_SumPoangTorv3,
	int nTrakt_SumPoangMineral3,
	int nTrakt_SumPoangHumus3,
	int nTrakt_PoangTorv3,
	int nTrakt_Sum7x7,
	int nTrakt_SumSkadadeLagor,
	int nTrakt_SumAntalLagor,
	CString	sTrakt_OwnerId,
	CString sTrakt_AtgardsId,
	int nTrakt_HogtSnytbaggetryck,
	int nTrakt_SumSkadadeFornminnen,
	int nTrakt_SumSkadadeKulturminnen,
	int nTrakt_SumKorskador)
  {
	m_nTrakt_Id=nTrakt_Id;
	m_nTrakt_Slutford=nTrakt_Slutford;
	m_sTrakt_Datum=sTrakt_Datum;
	m_sTrakt_Forvaltning=sTrakt_Forvaltning;
	m_sTrakt_Distrikt=sTrakt_Distrikt;
	m_nTrakt_Ursprung=nTrakt_Ursprung;	
	m_fTrakt_KartaX=fTrakt_KartaX;
	m_fTrakt_KartaY=fTrakt_KartaY;
	m_sTrakt_Traktnamn=sTrakt_Traktnamn;
	m_fTrakt_Areal=fTrakt_Areal;
	//m_nTrakt_ObjNr=nTrakt_ObjNr;
	m_sTrakt_Inventerare=sTrakt_Inventerare;
	m_sTrakt_Entreprenor=sTrakt_Entreprenor;
	m_sTrakt_Lag=sTrakt_Lag;
	m_nTrakt_Metod=nTrakt_Metod;
	m_sTrakt_Aggr=sTrakt_Aggr;
	m_nTrakt_MalMbStHa=nTrakt_MalMbStHa;
	m_sTrakt_Anteckningar=sTrakt_Anteckningar;

	m_nTrakt_Sum1=nTrakt_Sum1;
	m_nTrakt_Sum2=nTrakt_Sum2;
	m_nTrakt_SumM=nTrakt_SumM;
	m_nTrakt_SumHT=nTrakt_SumHT;
	m_nTrakt_Sum4=nTrakt_Sum4;
	m_nTrakt_Sum5=nTrakt_Sum5;
	m_nTrakt_SumPoang1=nTrakt_SumPoang1;
	m_nTrakt_SumPoang2=nTrakt_SumPoang2;
	m_nTrakt_SumPoangM=nTrakt_SumPoangM;
	m_nTrakt_SumPoangHT=nTrakt_SumPoangHT;
	m_nTrakt_SumPoang4=nTrakt_SumPoang4;
	m_nTrakt_SumPoang5=nTrakt_SumPoang5;
	m_nTrakt_MedelKvalitet=nTrakt_MedelKvalitet;
	m_nTrakt_GKPerYta=nTrakt_GKPerYta;
	m_nTrakt_GKPerHa=nTrakt_GKPerHa;
	m_nTrakt_OnodigaSkador=nTrakt_OnodigaSkador;
	m_nTrakt_SkadorFangstgrop=nTrakt_SkadorFangstgrop;
	m_nTrakt_SkadorMark=nTrakt_SkadorMark;
	m_nTrakt_OljaAvfall=nTrakt_OljaAvfall;
	m_nTrakt_Inlamnat10dagar=nTrakt_Inlamnat10dagar;
	m_nTrakt_Sum3=nTrakt_Sum3;
	m_nTrakt_SumPoang3=nTrakt_SumPoang3;
	m_nTrakt_Invtyp=nTrakt_Invtyp;
	m_nTrakt_SumTorv3=nTrakt_SumTorv3;
	m_nTrakt_SumMineral3=nTrakt_SumMineral3;
	m_nTrakt_SumHumus3=nTrakt_SumHumus3;
	m_nTrakt_SumPoangTorv3=nTrakt_SumPoangTorv3;
	m_nTrakt_SumPoangMineral3=nTrakt_SumPoangMineral3;
	m_nTrakt_SumPoangHumus3=nTrakt_SumPoangHumus3;
	m_nTrakt_PoangTorv3=nTrakt_PoangTorv3;
	m_nTrakt_Sum7x7=nTrakt_Sum7x7;
	m_nTrakt_SumSkadadeLagor=nTrakt_SumSkadadeLagor;
	m_nTrakt_SumAntalLagor=nTrakt_SumAntalLagor;		
	m_sTrakt_OwnerId=sTrakt_OwnerId;
	m_sTrakt_AtgardsId = sTrakt_AtgardsId;
	m_nTrakt_HogtSnytbaggetryck = nTrakt_HogtSnytbaggetryck;
	m_nTrakt_SumSkadadeFornminnen = m_nTrakt_SumSkadadeFornminnen;
	m_nTrakt_SumSkadadeKulturminnen = m_nTrakt_SumSkadadeKulturminnen;
	m_nTrakt_SumKorskador = m_nTrakt_SumKorskador;
	}
} TRAKT_DATA_MARK;

typedef std::vector<TRAKT_DATA_MARK> vecTraktData_Mark;

// TRAKT DATA STRUCTURES MARKBEREDNING
typedef struct _trakt_data_roj
{
    
	int		m_nTrakt_Id;
    int		m_nTrakt_Slutford;
    CString	m_sTrakt_Datum;
    CString	m_sTrakt_Forvaltning;
    CString	m_sTrakt_Distrikt;
    int		m_nTrakt_Ursprung;
    double	m_fTrakt_KartaX;
    double	m_fTrakt_KartaY;
    CString	m_sTrakt_Traktnamn;
    double	m_fTrakt_Areal;
    //int		m_nTrakt_ObjNr;
    CString	m_sTrakt_Inventerare;
    CString	m_sTrakt_Entreprenor;
    CString	m_sTrakt_Lag;
    CString	m_sTrakt_SI;
	int		m_nTrakt_AntalYtor;
    CString	m_sTrakt_Anteckningar;
    double	m_fTrakt_AntalHsTall;
    int		m_nTrakt_MhojdTall;
    int		m_nTrakt_StHaTall;
    int		m_nTrakt_AndelTall;
    double	m_fTrakt_AntalHsGran;
    int		m_nTrakt_MhojdGran;
    int		m_nTrakt_StHaGran;
    int		m_nTrakt_AndelGran;
    double	m_fTrakt_AntalHsCont;
    int		m_nTrakt_MhojdCont;
    int		m_nTrakt_StHaCont;
    int		m_nTrakt_AndelCont;
    double	m_fTrakt_AntalHsBjork;
    int		m_nTrakt_MhojdBjork;
    int		m_nTrakt_StHaBjork;
    int		m_nTrakt_AndelBjork;
	double	m_fTrakt_AntalBi;
    double	m_fTrakt_AntalRoj;
    double	m_fTrakt_AntalOroj;
    double	m_fTrakt_AntalHansyn;
	int		m_nTrakt_HojdBi;
    int		m_nTrakt_HojdRoj;
    int		m_nTrakt_HojdOroj;
	int		m_nTrakt_StHaBi;
    int		m_nTrakt_StHaRoj;
    int		m_nTrakt_StHaOroj;
    int		m_nTrakt_StHaHansyn;
    int		m_nTrakt_RojdaYtorRojbehov;
    int		m_nTrakt_OrojdaYtorRojbehov;
    int		m_nTrakt_RojdaYtorEjRojbehov;
    int		m_nTrakt_OrojdaYtorEjRojbehov;
	int		m_nTrakt_RojdaYtor;
	int		m_nTrakt_Rojbehov;
    double	m_fTrakt_Tslval;
    double	m_fTrakt_Stamval;
    double	m_fTrakt_Stamantal;
    double	m_fTrakt_Hansynsstammar;
    double	m_fTrakt_Myr;
    double	m_fTrakt_Vattendrag;
    double	m_fTrakt_Sjo;
    double	m_fTrakt_Annat;
    double	m_fTrakt_Surdrag;
    double	m_fTrakt_OvrBio;
    double	m_fTrakt_Kultur;
    double	m_fTrakt_TotKvalNatur;
    int		m_nTrakt_AlgskNya;
    int		m_nTrakt_AlgskGamla;
    int		m_nTrakt_Knackesjuka;
    int		m_nTrakt_Gremeniella;
	int		m_nTrakt_Ovriga;
	int		m_nTrakt_Invtyp;
	CString	m_sTrakt_OwnerId;
	CString m_sTrakt_AtgardsId;

	vecPlotData_Roj	m_vPlotData;

	_trakt_data_roj(void){
	m_nTrakt_Id=MY_NULL;
    m_nTrakt_Slutford=MY_NULL;
    m_sTrakt_Datum=_T("");
    m_sTrakt_Forvaltning=_T("");
    m_sTrakt_Distrikt=_T("");
    m_nTrakt_Ursprung=MY_NULL;
    m_fTrakt_KartaX=MY_NULL;
    m_fTrakt_KartaY=MY_NULL;
    m_sTrakt_Traktnamn=_T("");
    m_fTrakt_Areal=MY_NULL;
    //m_nTrakt_ObjNr=MY_NULL;
    m_sTrakt_Inventerare=_T("");
    m_sTrakt_Entreprenor=_T("");
    m_sTrakt_Lag=_T("");
    m_sTrakt_SI=_T("");
	m_nTrakt_AntalYtor=MY_NULL;
    m_sTrakt_Anteckningar=_T("");
    m_fTrakt_AntalHsTall=MY_NULL;
    m_nTrakt_MhojdTall=MY_NULL;
    m_nTrakt_StHaTall=MY_NULL;
    m_nTrakt_AndelTall=MY_NULL;
    m_fTrakt_AntalHsGran=MY_NULL;
    m_nTrakt_MhojdGran=MY_NULL;
    m_nTrakt_StHaGran=MY_NULL;
    m_nTrakt_AndelGran=MY_NULL;
    m_fTrakt_AntalHsCont=MY_NULL;
    m_nTrakt_MhojdCont=MY_NULL;
    m_nTrakt_StHaCont=MY_NULL;
    m_nTrakt_AndelCont=MY_NULL;
    m_fTrakt_AntalHsBjork=MY_NULL;
    m_nTrakt_MhojdBjork=MY_NULL;
    m_nTrakt_StHaBjork=MY_NULL;
    m_nTrakt_AndelBjork=MY_NULL;
	m_fTrakt_AntalBi=MY_NULL;
    m_fTrakt_AntalRoj=MY_NULL;
    m_fTrakt_AntalOroj=MY_NULL;
    m_fTrakt_AntalHansyn=MY_NULL;
	m_nTrakt_HojdBi=MY_NULL;
    m_nTrakt_HojdRoj=MY_NULL;
    m_nTrakt_HojdOroj=MY_NULL;
	m_nTrakt_StHaBi=MY_NULL;
    m_nTrakt_StHaRoj=MY_NULL;
    m_nTrakt_StHaOroj=MY_NULL;
    m_nTrakt_StHaHansyn=MY_NULL;
    m_nTrakt_RojdaYtorRojbehov=MY_NULL;
    m_nTrakt_OrojdaYtorRojbehov=MY_NULL;
    m_nTrakt_RojdaYtorEjRojbehov=MY_NULL;
    m_nTrakt_OrojdaYtorEjRojbehov=MY_NULL;
	m_nTrakt_RojdaYtor=MY_NULL;
	m_nTrakt_Rojbehov=MY_NULL;
    m_fTrakt_Tslval=MY_NULL;
    m_fTrakt_Stamval=MY_NULL;
    m_fTrakt_Stamantal=MY_NULL;
    m_fTrakt_Hansynsstammar=MY_NULL;
    m_fTrakt_Myr=MY_NULL;
    m_fTrakt_Vattendrag=MY_NULL;
    m_fTrakt_Sjo=MY_NULL;
    m_fTrakt_Annat=MY_NULL;
    m_fTrakt_Surdrag=MY_NULL;
    m_fTrakt_OvrBio=MY_NULL;
    m_fTrakt_Kultur=MY_NULL;
    m_fTrakt_TotKvalNatur=MY_NULL;
    m_nTrakt_AlgskNya=MY_NULL;
    m_nTrakt_AlgskGamla=MY_NULL;
    m_nTrakt_Knackesjuka=MY_NULL;
    m_nTrakt_Gremeniella=MY_NULL;
	m_nTrakt_Ovriga=MY_NULL;
	m_nTrakt_Invtyp=MY_NULL;
	m_sTrakt_OwnerId=_T("");
	m_sTrakt_AtgardsId = _T("");
	}
  
} TRAKT_DATA_ROJ;
typedef std::vector<TRAKT_DATA_ROJ> vecTraktData_Roj;


class CScaGallBasDB : public CDBBaseClass_SQLApi //CDBHandlerBase
{
	
public:
	CScaGallBasDB();
	CScaGallBasDB(SAClient_t client,LPCTSTR db_name = _T(""),LPCTSTR user_name = _T(""),LPCTSTR psw = _T(""));
	CScaGallBasDB(DB_CONNECTION_DATA &db_connection);

	BOOL doTableExists(LPCTSTR table_name);
	BOOL createTable(LPCTSTR script_string);

	
	//----------------- Plantering --------------------------------
	//BOOL getTrakts(vecTraktData_Plant&);
	BOOL getPlots(TRAKT_DATA_PLANT &trkt);
	BOOL getTrakts(vecTraktData_Plant &vec,CString sSqlQues,int var);
	BOOL getTrakts(vecTraktData_Plant &vec,CString sSqlQues);

	void Load_Trakt_Variables(vecTraktData_Plant&);
	void Load_Trakt_Variables(TRAKT_DATA_PLANT &);
	
	BOOL getTrakt(int Lopnr,TRAKT_DATA_PLANT &);
	BOOL getTraktIndex(vecTraktIndex_Plant &vec);

	void GetKeyDBQuestion(CString *,TRAKT_DATA_PLANT rec);
	void GetKeyPlotDBQuestion(CString *str,PLOT_DATA_PLANT rec);


	//----------------- Markberedning --------------------------------
	//BOOL getTrakts(vecTraktData_Mark&);
	BOOL getPlots(TRAKT_DATA_MARK &trkt);
	BOOL getTrakts(vecTraktData_Mark &vec,CString sSqlQues,int var);
	BOOL getTrakts(vecTraktData_Mark &vec,CString sSqlQues);


	void Load_Trakt_Variables(vecTraktData_Mark&);
	void Load_Trakt_Variables(TRAKT_DATA_MARK &);
	
	BOOL getTrakt(int Lopnr,TRAKT_DATA_MARK &);
	BOOL getTraktIndex(vecTraktIndex_Mark &vec);

	void GetKeyDBQuestion(CString *,TRAKT_DATA_MARK rec);
	void GetKeyPlotDBQuestion(CString *str,PLOT_DATA_MARK rec);

	//----------------- R�jning --------------------------------
	//BOOL getTrakts(vecTraktData_Roj&);
	BOOL getPlots(TRAKT_DATA_ROJ &trkt);
	BOOL getTrakts(vecTraktData_Roj &vec,CString sSqlQues,int var);
	BOOL getTrakts(vecTraktData_Roj &vec,CString sSqlQues);


	void Load_Trakt_Variables(vecTraktData_Roj&);
	void Load_Trakt_Variables(TRAKT_DATA_ROJ &);
	
	BOOL getTrakt(int Lopnr,TRAKT_DATA_ROJ &);
	BOOL getTraktIndex(vecTraktIndex_Roj &vec);

	void GetKeyDBQuestion(CString *,TRAKT_DATA_ROJ rec);
	void GetKeyPlotDBQuestion(CString *str,PLOT_DATA_ROJ rec);


protected:
	void getSQLTraktQuestion_Plant(LPTSTR sql);
	void getSQLTraktQuestionIndex_Plant(LPTSTR sql);
	void getSQLTraktQuestion_Mark(LPTSTR sql);
	void getSQLTraktQuestionIndex_Mark(LPTSTR sql);
	void getSQLTraktQuestion_Roj(LPTSTR sql);
	void getSQLTraktQuestionIndex_Roj(LPTSTR sql);
	void getSQLPlotQuestion(LPTSTR sql);
};

typedef struct _sql_trakt_question
{
int		nTrakt_Id;
  
  TCHAR szSQL[1024];	// Buffer to hold actual question; 061010 p�d

} SQL_TRAKT_QUESTION;


#endif
