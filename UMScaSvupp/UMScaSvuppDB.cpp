#include "StdAfx.h"

#include "UMScaSvuppDB.h"


void setupForDBConnection(HWND hWndReciv,HWND hWndSend)
{
   if (hWndReciv != NULL)
   {
      DB_CONNECTION_DATA data;
		COPYDATASTRUCT HSData;
		memset(&HSData, 0, sizeof(COPYDATASTRUCT));
		HSData.dwData = 1;
		HSData.cbData = sizeof(DB_CONNECTION_DATA);
		HSData.lpData = &data;
		data.conn = NULL;
		::SendMessage(hWndReciv,WM_COPYDATA,(WPARAM)hWndSend, (LPARAM)&HSData);
   }
}


//////////////////////////////////////////////////////////////////////////////////
// CScaGallBasDBDB; Handle ALL transactions for GallBas

CScaGallBasDB::CScaGallBasDB() 
	: CDBBaseClass_SQLApi(SA_Client_NotSpecified)
{
}

CScaGallBasDB::CScaGallBasDB(SAClient_t client,LPCTSTR db_name,LPCTSTR user_name,LPCTSTR psw)
	: CDBBaseClass_SQLApi(client,db_name,user_name,psw)
{
}

CScaGallBasDB::CScaGallBasDB(DB_CONNECTION_DATA &db_connection)
	: CDBBaseClass_SQLApi(db_connection,1)
{
}
// PUBLIC
BOOL CScaGallBasDB::doTableExists(LPCTSTR table_name)
{
	CString sSQL;
	CString err;
   BOOL bReturn = FALSE;
	try
	{
		// Set SQL question to send to database db_name; 061101 p�d
		sSQL.Format(_T("select object_id('%s','U') as 'objectid'"),table_name);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
	   while(m_saCommand.FetchNext())
		//if(m_saCommand.FetchNext())
			bReturn=TRUE;
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=table_name;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}
	return bReturn;
}

BOOL CScaGallBasDB::createTable(LPCTSTR SQL_SCRPT)
{
	CString sSQL;
	CString err;
   
	try
	{
		sSQL.Format(_T("%s"),SQL_SCRPT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte skapa Tabell ");
		//err+=TBL_FORVALT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}

	return TRUE;
}


void CScaGallBasDB::Load_Trakt_Variables(TRAKT_DATA_ROJ &t)
{
		if(m_saCommand.Field("Lopnr").isNull())
			t.m_nTrakt_Id=MY_NULL;  
		else
			t.m_nTrakt_Id=m_saCommand.Field("Lopnr").asLong(); 	

		if(m_saCommand.Field("Slutford").isNull())
			t.m_nTrakt_Slutford=MY_NULL;  
		else
			t.m_nTrakt_Slutford=m_saCommand.Field("Slutford").asLong(); 	

		if(m_saCommand.Field("Datum").isNull())
			t.m_sTrakt_Datum=_T("");
		else
		{
			SADateTime d;
			d=m_saCommand.Field("Datum").asDateTime();
			t.m_sTrakt_Datum.Format(_T("%4.4d%02d%02d"),d.GetYear(),d.GetMonth(),d.GetDay());
		}
		

		if(m_saCommand.Field("Forvaltning").isNull())
			t.m_sTrakt_Forvaltning=_T("");
		else
			t.m_sTrakt_Forvaltning=(LPCTSTR)m_saCommand.Field("Forvaltning").asString().GetBinaryBuffer(0); 

		if(m_saCommand.Field("Distrikt").isNull())
			t.m_sTrakt_Distrikt=_T("");  
		else
			t.m_sTrakt_Distrikt=(LPCTSTR)m_saCommand.Field("Distrikt").asString().GetBinaryBuffer(0); 

		if(m_saCommand.Field("Ursprung").isNull())
			t.m_nTrakt_Ursprung=MY_NULL;  
		else
			t.m_nTrakt_Ursprung=m_saCommand.Field("Ursprung").asLong(); 	

		if(m_saCommand.Field("KartaX").isNull())
			t.m_fTrakt_KartaX=MY_NULL;  
		else
			t.m_fTrakt_KartaX=m_saCommand.Field("KartaX").asDouble();  	

		if(m_saCommand.Field("KartaY").isNull())
			t.m_fTrakt_KartaY=MY_NULL;  
		else
			t.m_fTrakt_KartaY=m_saCommand.Field("KartaY").asDouble();  	

		if(m_saCommand.Field("Traktnamn").isNull())
			t.m_sTrakt_Traktnamn=_T("");
		else
			t.m_sTrakt_Traktnamn=(LPCTSTR)m_saCommand.Field("Traktnamn").asString().GetBinaryBuffer(0); 

		if(m_saCommand.Field("Areal").isNull())
			t.m_fTrakt_Areal=MY_NULL;  
		else
			t.m_fTrakt_Areal=m_saCommand.Field("Areal").asDouble();  	

		/*if(m_saCommand.Field("ObjNr").isNull())
			t.m_nTrakt_ObjNr=MY_NULL;  
		else
			t.m_nTrakt_ObjNr=m_saCommand.Field("ObjNr").asLong();*/

		if(m_saCommand.Field("Inventerare").isNull())
			t.m_sTrakt_Inventerare=_T("");
		else
			t.m_sTrakt_Inventerare=(LPCTSTR)m_saCommand.Field("Inventerare").asString().GetBinaryBuffer(0); 

		if(m_saCommand.Field("Entreprenor").isNull())
			t.m_sTrakt_Entreprenor=_T("");
		else
			t.m_sTrakt_Entreprenor=(LPCTSTR)m_saCommand.Field("Entreprenor").asString().GetBinaryBuffer(0); 

		if(m_saCommand.Field("Lag").isNull())
			t.m_sTrakt_Lag=_T("");
		else
			t.m_sTrakt_Lag=(LPCTSTR)m_saCommand.Field("Lag").asString().GetBinaryBuffer(0); 

		if(m_saCommand.Field("SI").isNull())
			t.m_sTrakt_SI=_T("");
		else
			t.m_sTrakt_SI=(LPCTSTR)m_saCommand.Field("SI").asString().GetBinaryBuffer(0); 

		if(m_saCommand.Field("AntalYtor").isNull())
			t.m_nTrakt_AntalYtor=MY_NULL;  
		else
			t.m_nTrakt_AntalYtor=m_saCommand.Field("AntalYtor").asLong(); 	

		if(m_saCommand.Field("Anteckningar").isNull())
			t.m_sTrakt_Anteckningar=_T("");
		else
			t.m_sTrakt_Anteckningar=(LPCTSTR)m_saCommand.Field("Anteckningar").asString().GetBinaryBuffer(0); 

		if(m_saCommand.Field("AntalHsTall").isNull())
			t.m_fTrakt_AntalHsTall=MY_NULL;  
		else
			t.m_fTrakt_AntalHsTall=m_saCommand.Field("AntalHsTall").asDouble();  	

		if(m_saCommand.Field("MhojdTall").isNull())
			t.m_nTrakt_MhojdTall=MY_NULL;  
		else
			t.m_nTrakt_MhojdTall=m_saCommand.Field("MhojdTall").asLong(); 	

		if(m_saCommand.Field("StHaTall").isNull())
			t.m_nTrakt_StHaTall=MY_NULL;  
		else
			t.m_nTrakt_StHaTall=m_saCommand.Field("StHaTall").asLong(); 	

		if(m_saCommand.Field("AndelTall").isNull())
			t.m_nTrakt_AndelTall=MY_NULL;  
		else
			t.m_nTrakt_AndelTall=m_saCommand.Field("AndelTall").asLong(); 	

		if(m_saCommand.Field("AntalHsGran").isNull())
			t.m_fTrakt_AntalHsGran=MY_NULL;  
		else
			t.m_fTrakt_AntalHsGran=m_saCommand.Field("AntalHsGran").asDouble();  	

		if(m_saCommand.Field("MhojdGran").isNull())
			t.m_nTrakt_MhojdGran=MY_NULL;  
		else
			t.m_nTrakt_MhojdGran=m_saCommand.Field("MhojdGran").asLong(); 	

		if(m_saCommand.Field("StHaGran").isNull())
			t.m_nTrakt_StHaGran=MY_NULL;  
		else
			t.m_nTrakt_StHaGran=m_saCommand.Field("StHaGran").asLong(); 	

		if(m_saCommand.Field("AndelGran").isNull())
			t.m_nTrakt_AndelGran=MY_NULL;  
		else
			t.m_nTrakt_AndelGran=m_saCommand.Field("AndelGran").asLong(); 	

		if(m_saCommand.Field("AntalHsCont").isNull())
			t.m_fTrakt_AntalHsCont=MY_NULL;  
		else
			t.m_fTrakt_AntalHsCont=m_saCommand.Field("AntalHsCont").asDouble();  	

		if(m_saCommand.Field("MhojdCont").isNull())
			t.m_nTrakt_MhojdCont=MY_NULL;  
		else
			t.m_nTrakt_MhojdCont=m_saCommand.Field("MhojdCont").asLong();

		if(m_saCommand.Field("StHaCont").isNull())
			t.m_nTrakt_StHaCont=MY_NULL;  
		else
			t.m_nTrakt_StHaCont=m_saCommand.Field("StHaCont").asLong();

		if(m_saCommand.Field("AndelCont").isNull())
			t.m_nTrakt_AndelCont=MY_NULL;  
		else
			t.m_nTrakt_AndelCont=m_saCommand.Field("AndelCont").asLong();

		if(m_saCommand.Field("AntalHsBjork").isNull())
			t.m_fTrakt_AntalHsBjork=MY_NULL;  
		else
			t.m_fTrakt_AntalHsBjork=m_saCommand.Field("AntalHsBjork").asDouble(); 

		if(m_saCommand.Field("MhojdBjork").isNull())
			t.m_nTrakt_MhojdBjork=MY_NULL;  
		else
			t.m_nTrakt_MhojdBjork=m_saCommand.Field("MhojdBjork").asLong();

		if(m_saCommand.Field("StHaBjork").isNull())
			t.m_nTrakt_StHaBjork=MY_NULL;  
		else
			t.m_nTrakt_StHaBjork=m_saCommand.Field("StHaBjork").asLong();

		if(m_saCommand.Field("AndelBjork").isNull())
			t.m_nTrakt_AndelBjork=MY_NULL;  
		else
			t.m_nTrakt_AndelBjork=m_saCommand.Field("AndelBjork").asLong();

		if(m_saCommand.Field("AntalBi").isNull())
			t.m_fTrakt_AntalBi=MY_NULL;  
		else
			t.m_fTrakt_AntalBi=m_saCommand.Field("AntalBi").asDouble();

		if(m_saCommand.Field("AntalRoj").isNull())
			t.m_fTrakt_AntalRoj=MY_NULL;  
		else
			t.m_fTrakt_AntalRoj=m_saCommand.Field("AntalRoj").asDouble();

		if(m_saCommand.Field("AntalOroj").isNull())
			t.m_fTrakt_AntalOroj=MY_NULL;  
		else
			t.m_fTrakt_AntalOroj=m_saCommand.Field("AntalOroj").asDouble();

		if(m_saCommand.Field("AntalHansyn").isNull())
			t.m_fTrakt_AntalHansyn=MY_NULL;  
		else
			t.m_fTrakt_AntalHansyn=m_saCommand.Field("AntalHansyn").asDouble();

		if(m_saCommand.Field("HojdBi").isNull())
			t.m_nTrakt_HojdBi=MY_NULL;  
		else
			t.m_nTrakt_HojdBi=m_saCommand.Field("HojdBi").asLong();

		if(m_saCommand.Field("HojdRoj").isNull())
			t.m_nTrakt_HojdRoj=MY_NULL;  
		else
			t.m_nTrakt_HojdRoj=m_saCommand.Field("HojdRoj").asLong();

		if(m_saCommand.Field("HojdOroj").isNull())
			t.m_nTrakt_HojdOroj=MY_NULL;  
		else
			t.m_nTrakt_HojdOroj=m_saCommand.Field("HojdOroj").asLong();

		if(m_saCommand.Field("StHaBi").isNull())
			t.m_nTrakt_StHaBi=MY_NULL;  
		else
			t.m_nTrakt_StHaBi=m_saCommand.Field("StHaBi").asLong();

		if(m_saCommand.Field("StHaRoj").isNull())
			t.m_nTrakt_StHaRoj=MY_NULL;  
		else
			t.m_nTrakt_StHaRoj=m_saCommand.Field("StHaRoj").asLong();

		if(m_saCommand.Field("StHaOroj").isNull())
			t.m_nTrakt_StHaOroj=MY_NULL;  
		else
			t.m_nTrakt_StHaOroj=m_saCommand.Field("StHaOroj").asLong();

		if(m_saCommand.Field("StHaHansyn").isNull())
			t.m_nTrakt_StHaHansyn=MY_NULL;  
		else
			t.m_nTrakt_StHaHansyn=m_saCommand.Field("StHaHansyn").asLong();

		if(m_saCommand.Field("RojdaYtorRojbehov").isNull())
			t.m_nTrakt_RojdaYtorRojbehov=MY_NULL;  
		else
			t.m_nTrakt_RojdaYtorRojbehov=m_saCommand.Field("RojdaYtorRojbehov").asLong();

		if(m_saCommand.Field("OrojdaYtorRojbehov").isNull())
			t.m_nTrakt_OrojdaYtorRojbehov=MY_NULL;  
		else
			t.m_nTrakt_OrojdaYtorRojbehov=m_saCommand.Field("OrojdaYtorRojbehov").asLong();

		if(m_saCommand.Field("RojdaYtorEjRojbehov").isNull())
			t.m_nTrakt_RojdaYtorEjRojbehov=MY_NULL;  
		else
			t.m_nTrakt_RojdaYtorEjRojbehov=m_saCommand.Field("RojdaYtorEjRojbehov").asLong();

		if(m_saCommand.Field("OrojdaYtorEjRojbehov").isNull())
			t.m_nTrakt_OrojdaYtorEjRojbehov=MY_NULL;  
		else
			t.m_nTrakt_OrojdaYtorEjRojbehov=m_saCommand.Field("OrojdaYtorEjRojbehov").asLong();

		if(m_saCommand.Field("RojdaYtor").isNull())
			t.m_nTrakt_RojdaYtor=MY_NULL;  
		else
			t.m_nTrakt_RojdaYtor=m_saCommand.Field("RojdaYtor").asLong();

		if(m_saCommand.Field("Rojbehov").isNull())
			t.m_nTrakt_Rojbehov=MY_NULL;  
		else
			t.m_nTrakt_Rojbehov=m_saCommand.Field("Rojbehov").asLong();

		if(m_saCommand.Field("Tslval").isNull())
			t.m_fTrakt_Tslval=MY_NULL;  
		else
			t.m_fTrakt_Tslval=m_saCommand.Field("Tslval").asDouble(); 

		if(m_saCommand.Field("Stamval").isNull())
			t.m_fTrakt_Stamval=MY_NULL;  
		else
			t.m_fTrakt_Stamval=m_saCommand.Field("Stamval").asDouble();

		if(m_saCommand.Field("Stamantal").isNull())
			t.m_fTrakt_Stamantal=MY_NULL;  
		else
			t.m_fTrakt_Stamantal=m_saCommand.Field("Stamantal").asDouble();

		if(m_saCommand.Field("Hansynsstammar").isNull())
			t.m_fTrakt_Hansynsstammar=MY_NULL;  
		else
			t.m_fTrakt_Hansynsstammar=m_saCommand.Field("Hansynsstammar").asDouble();

		if(m_saCommand.Field("Myr").isNull())
			t.m_fTrakt_Myr=MY_NULL;  
		else
			t.m_fTrakt_Myr=m_saCommand.Field("Myr").asDouble();

		if(m_saCommand.Field("Vattendrag").isNull())
			t.m_fTrakt_Vattendrag=MY_NULL;  
		else
			t.m_fTrakt_Vattendrag=m_saCommand.Field("Vattendrag").asDouble();

		if(m_saCommand.Field("Sjo").isNull())
			t.m_fTrakt_Sjo=MY_NULL;  
		else
			t.m_fTrakt_Sjo=m_saCommand.Field("Sjo").asDouble();

		if(m_saCommand.Field("Annat").isNull())
			t.m_fTrakt_Annat=MY_NULL;  
		else
			t.m_fTrakt_Annat=m_saCommand.Field("Annat").asDouble();

		if(m_saCommand.Field("Surdrag").isNull())
			t.m_fTrakt_Surdrag=MY_NULL;  
		else
			t.m_fTrakt_Surdrag=m_saCommand.Field("Surdrag").asDouble(); 

		if(m_saCommand.Field("OvrBio").isNull())
			t.m_fTrakt_OvrBio=MY_NULL;  
		else
			t.m_fTrakt_OvrBio=m_saCommand.Field("OvrBio").asDouble();

		if(m_saCommand.Field("Kultur").isNull())
			t.m_fTrakt_Kultur=MY_NULL;  
		else
			t.m_fTrakt_Kultur=m_saCommand.Field("Kultur").asDouble();

		if(m_saCommand.Field("TotKvalNatur").isNull())
			t.m_fTrakt_TotKvalNatur=MY_NULL;  
		else
			t.m_fTrakt_TotKvalNatur=m_saCommand.Field("TotKvalNatur").asDouble();

		if(m_saCommand.Field("AlgskNya").isNull())
			t.m_nTrakt_AlgskNya=MY_NULL;  
		else
			t.m_nTrakt_AlgskNya=m_saCommand.Field("AlgskNya").asLong();

		if(m_saCommand.Field("AlgskGamla").isNull())
			t.m_nTrakt_AlgskGamla=MY_NULL;  
		else
			t.m_nTrakt_AlgskGamla=m_saCommand.Field("AlgskGamla").asLong();

		if(m_saCommand.Field("Knackesjuka").isNull())
			t.m_nTrakt_Knackesjuka=MY_NULL;  
		else
			t.m_nTrakt_Knackesjuka=m_saCommand.Field("Knackesjuka").asLong();

		if(m_saCommand.Field("Gremeniella").isNull())
			t.m_nTrakt_Gremeniella=MY_NULL;  
		else
			t.m_nTrakt_Gremeniella=m_saCommand.Field("Gremeniella").asLong();

		if(m_saCommand.Field("Ovriga").isNull())
			t.m_nTrakt_Ovriga=MY_NULL;  
		else
			t.m_nTrakt_Ovriga=m_saCommand.Field("Ovriga").asLong();

		if(m_saCommand.Field("Invtyp").isNull())
			t.m_nTrakt_Invtyp=MY_NULL;  
		else
			t.m_nTrakt_Invtyp=m_saCommand.Field("Invtyp").asLong();

		if(m_saCommand.Field("OwnerId").isNull())
			t.m_sTrakt_OwnerId=_T("");
		else
			t.m_sTrakt_OwnerId=(LPCTSTR)m_saCommand.Field("OwnerId").asString().GetBinaryBuffer(0);

		if(m_saCommand.Field("AtgardsId").isNull())
			t.m_sTrakt_AtgardsId =_T("");
		else
			t.m_sTrakt_AtgardsId = (LPCTSTR)m_saCommand.Field("AtgardsId").asString().GetBinaryBuffer(0);
}

void CScaGallBasDB::Load_Trakt_Variables(TRAKT_DATA_MARK &t)
{
	int spec=0,var=0;


	// 1 int 		
	if(m_saCommand.Field("Lopnr").isNull())
		t.m_nTrakt_Id=MY_NULL;  
	else
		t.m_nTrakt_Id=m_saCommand.Field("Lopnr").asLong();

	// 2 int 
	if(m_saCommand.Field("Slutford").isNull())
		t.m_nTrakt_Slutford=MY_NULL;  
	else
		t.m_nTrakt_Slutford=m_saCommand.Field("Slutford").asLong();

	// 3 CString
	if(m_saCommand.Field("Datum").isNull())
		t.m_sTrakt_Datum=_T("");  
	else
	{
		SADateTime d;
		d=m_saCommand.Field("Datum").asDateTime();
		t.m_sTrakt_Datum.Format(_T("%4.4d%02d%02d"),d.GetYear(),d.GetMonth(),d.GetDay());
		//t.m_sTrakt_Datum=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
	}

	// 4 CString
	if(m_saCommand.Field("Forvaltning").isNull())
		t.m_sTrakt_Forvaltning=_T("");  
	else
		t.m_sTrakt_Forvaltning=(LPCTSTR)m_saCommand.Field("Forvaltning").asString().GetBinaryBuffer(0);  	

	// 5 int
	if(m_saCommand.Field("Ursprung").isNull())
		t.m_nTrakt_Ursprung=MY_NULL;  
	else
		t.m_nTrakt_Ursprung=m_saCommand.Field("Ursprung").asLong();

	// 6 CString
	if(m_saCommand.Field("Distrikt").isNull())
		t.m_sTrakt_Distrikt=_T(""); 
	else
		t.m_sTrakt_Distrikt=(LPCTSTR)m_saCommand.Field("Distrikt").asString().GetBinaryBuffer(0);  	

	// 7 double
	if(m_saCommand.Field("KartaX").isNull())
		t.m_fTrakt_KartaX=MY_NULL;  
	else
		t.m_fTrakt_KartaX=m_saCommand.Field("KartaX").asDouble();

	// 8 double
	if(m_saCommand.Field("KartaY").isNull())
		t.m_fTrakt_KartaY=MY_NULL;  
	else
		t.m_fTrakt_KartaY=m_saCommand.Field("KartaY").asDouble();

	// 9 CString
	if(m_saCommand.Field("Traktnamn").isNull())
		t.m_sTrakt_Traktnamn=_T("");
	else
		t.m_sTrakt_Traktnamn=(LPCTSTR)m_saCommand.Field("Traktnamn").asString().GetBinaryBuffer(0);  	

	// 10 double
	if(m_saCommand.Field("Areal").isNull())
		t.m_fTrakt_Areal=MY_NULL;  
	else
		t.m_fTrakt_Areal=m_saCommand.Field("Areal").asDouble();

	// 11 int
	/*if(m_saCommand.Field("ObjNr").isNull())
		t.m_nTrakt_ObjNr=MY_NULL;  
	else
		t.m_nTrakt_ObjNr=m_saCommand.Field("ObjNr").asLong();*/

	// 12 CString
	if(m_saCommand.Field("Inventerare").isNull())
		t.m_sTrakt_Inventerare=_T("");  
	else
		t.m_sTrakt_Inventerare=(LPCTSTR)m_saCommand.Field("Inventerare").asString().GetBinaryBuffer(0);  	

	// 13 CString
	if(m_saCommand.Field("Entreprenor").isNull())
		t.m_sTrakt_Entreprenor=_T("");  
	else
		t.m_sTrakt_Entreprenor=(LPCTSTR)m_saCommand.Field("Entreprenor").asString().GetBinaryBuffer(0);  	

	// 14 CString
	if(m_saCommand.Field("Lag").isNull())
		t.m_sTrakt_Lag=_T("");  
	else
		t.m_sTrakt_Lag=(LPCTSTR)m_saCommand.Field("Lag").asString().GetBinaryBuffer(0);

	// 15 int
	if(m_saCommand.Field("Metod").isNull())
		t.m_nTrakt_Metod=MY_NULL;  
	else
		t.m_nTrakt_Metod=m_saCommand.Field("Metod").asLong();

	// 16 CString
	if(m_saCommand.Field("Aggr").isNull())
		t.m_sTrakt_Aggr=_T("");  
	else
		t.m_sTrakt_Aggr=(LPCTSTR)m_saCommand.Field("Aggr").asString().GetBinaryBuffer(0);  	

	// 17 int
	if(m_saCommand.Field("MalMbStHa").isNull())
		t.m_nTrakt_MalMbStHa=MY_NULL;  
	else
		t.m_nTrakt_MalMbStHa=m_saCommand.Field("MalMbStHa").asLong();

	// 18 CString
	if(m_saCommand.Field("Anteckningar").isNull())
		t.m_sTrakt_Anteckningar=_T("");
	else
		t.m_sTrakt_Anteckningar=m_saCommand.Field("Anteckningar").asCLob();

	if(m_saCommand.Field("Sum1").isNull())
		t.m_nTrakt_Sum1=MY_NULL;  
	else
		t.m_nTrakt_Sum1=m_saCommand.Field("Sum1").asLong();

	if(m_saCommand.Field("Sum2").isNull())
		t.m_nTrakt_Sum2=MY_NULL;  
	else
		t.m_nTrakt_Sum2=m_saCommand.Field("Sum2").asLong();

	if(m_saCommand.Field("SumM").isNull())
		t.m_nTrakt_SumM=MY_NULL;  
	else
		t.m_nTrakt_SumM=m_saCommand.Field("SumM").asLong();

	if(m_saCommand.Field("SumHT").isNull())
		t.m_nTrakt_SumHT=MY_NULL;  
	else
		t.m_nTrakt_SumHT=m_saCommand.Field("SumHT").asLong();
	
	if(m_saCommand.Field("Sum4").isNull())
		t.m_nTrakt_Sum4=MY_NULL;  
	else
		t.m_nTrakt_Sum4=m_saCommand.Field("Sum4").asLong();
	
	if(m_saCommand.Field("Sum5").isNull())
		t.m_nTrakt_Sum5=MY_NULL;  
	else
		t.m_nTrakt_Sum5=m_saCommand.Field("Sum5").asLong();

	if(m_saCommand.Field("SumPoang1").isNull())
		t.m_nTrakt_SumPoang1=MY_NULL;  
	else
		t.m_nTrakt_SumPoang1=m_saCommand.Field("SumPoang1").asLong();

	if(m_saCommand.Field("SumPoang2").isNull())
		t.m_nTrakt_SumPoang2=MY_NULL;  
	else
		t.m_nTrakt_SumPoang2=m_saCommand.Field("SumPoang2").asLong();

	if(m_saCommand.Field("SumPoangM").isNull())
		t.m_nTrakt_SumPoangM=MY_NULL;  
	else
		t.m_nTrakt_SumPoangM=m_saCommand.Field("SumPoangM").asLong();
	
	if(m_saCommand.Field("SumPoangHT").isNull())
		t.m_nTrakt_SumPoangHT=MY_NULL;  
	else
		t.m_nTrakt_SumPoangHT=m_saCommand.Field("SumPoangHT").asLong();
	
	if(m_saCommand.Field("SumPoang4").isNull())
		t.m_nTrakt_SumPoang4=MY_NULL;  
	else
		t.m_nTrakt_SumPoang4=m_saCommand.Field("SumPoang4").asLong();
	
	if(m_saCommand.Field("SumPoang5").isNull())
		t.m_nTrakt_SumPoang5=MY_NULL;  
	else
		t.m_nTrakt_SumPoang5=m_saCommand.Field("SumPoang5").asLong();

	if(m_saCommand.Field("MedelKvalitet").isNull())
		t.m_nTrakt_MedelKvalitet=MY_NULL;  
	else
		t.m_nTrakt_MedelKvalitet=m_saCommand.Field("MedelKvalitet").asLong();

	if(m_saCommand.Field("GKPerYta").isNull())
		t.m_nTrakt_GKPerYta=MY_NULL;  
	else
		t.m_nTrakt_GKPerYta=m_saCommand.Field("GKPerYta").asLong();

	if(m_saCommand.Field("GKPerHa").isNull())
		t.m_nTrakt_GKPerHa=MY_NULL;  
	else
		t.m_nTrakt_GKPerHa=m_saCommand.Field("GKPerHa").asLong();

	if(m_saCommand.Field("OnodigaSkador").isNull())
		t.m_nTrakt_OnodigaSkador=MY_NULL;  
	else
		t.m_nTrakt_OnodigaSkador=m_saCommand.Field("OnodigaSkador").asLong();

	if(m_saCommand.Field("SkadorFangstgrop").isNull())
		t.m_nTrakt_SkadorFangstgrop=MY_NULL;  
	else
		t.m_nTrakt_SkadorFangstgrop=m_saCommand.Field("SkadorFangstgrop").asLong();

	if(m_saCommand.Field("SkadorMark").isNull())
		t.m_nTrakt_SkadorMark=MY_NULL;  
	else
		t.m_nTrakt_SkadorMark=m_saCommand.Field("SkadorMark").asLong();

	if(m_saCommand.Field("OljaAvfall").isNull())
		t.m_nTrakt_OljaAvfall=MY_NULL;  
	else
		t.m_nTrakt_OljaAvfall=m_saCommand.Field("OljaAvfall").asLong();

	if(m_saCommand.Field("Inlamnat10dagar").isNull())
		t.m_nTrakt_Inlamnat10dagar=MY_NULL;
	else
		t.m_nTrakt_Inlamnat10dagar=m_saCommand.Field("Inlamnat10dagar").asLong();
	
	if(m_saCommand.Field("Sum3").isNull())
		t.m_nTrakt_Sum3=MY_NULL;  
	else
		t.m_nTrakt_Sum3=m_saCommand.Field("Sum3").asLong();

	if(m_saCommand.Field("SumPoang3").isNull())
		t.m_nTrakt_SumPoang3=MY_NULL;  
	else
		t.m_nTrakt_SumPoang3=m_saCommand.Field("SumPoang3").asLong();

	if(m_saCommand.Field("Invtyp").isNull())
		t.m_nTrakt_Invtyp=MY_NULL;  
	else
		t.m_nTrakt_Invtyp=m_saCommand.Field("Invtyp").asLong();

	if(m_saCommand.Field("SumTorv3").isNull())
		t.m_nTrakt_SumTorv3=MY_NULL;  
	else
		t.m_nTrakt_SumTorv3=m_saCommand.Field("SumTorv3").asLong();

	if(m_saCommand.Field("SumMineral3").isNull())
		t.m_nTrakt_SumMineral3=MY_NULL;  
	else
		t.m_nTrakt_SumMineral3=m_saCommand.Field("SumMineral3").asLong();

	if(m_saCommand.Field("SumHumus3").isNull())
		t.m_nTrakt_SumHumus3=MY_NULL;  
	else
		t.m_nTrakt_SumHumus3=m_saCommand.Field("SumHumus3").asLong();
	
	if(m_saCommand.Field("SumPoangTorv3").isNull())
		t.m_nTrakt_SumPoangTorv3=MY_NULL;  
	else
		t.m_nTrakt_SumPoangTorv3=m_saCommand.Field("SumPoangTorv3").asLong();

	if(m_saCommand.Field("SumPoangMineral3").isNull())
		t.m_nTrakt_SumPoangMineral3=MY_NULL;  
	else
		t.m_nTrakt_SumPoangMineral3=m_saCommand.Field("SumPoangMineral3").asLong();

	if(m_saCommand.Field("SumPoangHumus3").isNull())
		t.m_nTrakt_SumPoangHumus3=MY_NULL;  
	else
		t.m_nTrakt_SumPoangHumus3=m_saCommand.Field("SumPoangHumus3").asLong();

	if(m_saCommand.Field("PoengTorv3").isNull())
		t.m_nTrakt_PoangTorv3=MY_NULL;  
	else
		t.m_nTrakt_PoangTorv3=m_saCommand.Field("PoengTorv3").asLong();

	if(m_saCommand.Field("Sum7x7").isNull())
		t.m_nTrakt_Sum7x7=MY_NULL;  
	else
		t.m_nTrakt_Sum7x7=m_saCommand.Field("Sum7x7").asLong();

	if(m_saCommand.Field("SumSkadadeLagor").isNull())
		t.m_nTrakt_SumSkadadeLagor=MY_NULL;  
	else
		t.m_nTrakt_SumSkadadeLagor=m_saCommand.Field("SumSkadadeLagor").asLong();
	
	if(m_saCommand.Field("SumAntalLagor").isNull())
		t.m_nTrakt_SumAntalLagor=MY_NULL;  
	else
		t.m_nTrakt_SumAntalLagor=m_saCommand.Field("SumAntalLagor").asLong();

	if(m_saCommand.Field("OwnerId").isNull())
		t.m_sTrakt_OwnerId=_T("");  
	else
		t.m_sTrakt_OwnerId=(LPCTSTR)m_saCommand.Field("OwnerId").asString().GetBinaryBuffer(0);

	if(m_saCommand.Field("AtgardsId").isNull())
		t.m_sTrakt_AtgardsId =_T("");  
	else
		t.m_sTrakt_AtgardsId = (LPCTSTR)m_saCommand.Field("AtgardsId").asString().GetBinaryBuffer(0);

	if(m_saCommand.Field("HogtSnytbaggetryck").isNull())
		t.m_nTrakt_HogtSnytbaggetryck = MY_NULL;  
	else
		t.m_nTrakt_HogtSnytbaggetryck = m_saCommand.Field("HogtSnytbaggetryck").asLong();

	if(m_saCommand.Field("SumSkadadeFornminnen").isNull())
		t.m_nTrakt_SumSkadadeFornminnen = MY_NULL;  
	else
		t.m_nTrakt_SumSkadadeFornminnen = m_saCommand.Field("SumSkadadeFornminnen").asLong();

	if(m_saCommand.Field("SumSkadadeKulturminnen").isNull())
		t.m_nTrakt_SumSkadadeKulturminnen = MY_NULL;  
	else
		t.m_nTrakt_SumSkadadeKulturminnen = m_saCommand.Field("SumSkadadeKulturminnen").asLong();

	if(m_saCommand.Field("SumKorskador").isNull())
		t.m_nTrakt_SumKorskador = MY_NULL;  
	else
		t.m_nTrakt_SumKorskador = m_saCommand.Field("SumKorskador").asLong();
}

void CScaGallBasDB::Load_Trakt_Variables(TRAKT_DATA_PLANT &t)
{
	int spec=0,var=0;


	// 1 int 		
	if(m_saCommand.Field("Lopnr").isNull())
		t.m_nTrakt_Id=MY_NULL;  
	else
		t.m_nTrakt_Id=m_saCommand.Field("Lopnr").asLong();

	// 2 int 
	if(m_saCommand.Field("Slutford").isNull())
		t.m_nTrakt_Slutford=MY_NULL;  
	else
		t.m_nTrakt_Slutford=m_saCommand.Field("Slutford").asLong();

	// 3 CString
	if(m_saCommand.Field("Datum").isNull())
		t.m_sTrakt_Datum=_T("");  
	else
	{
		SADateTime d;
		d=m_saCommand.Field("Datum").asDateTime();
		t.m_sTrakt_Datum.Format(_T("%4.4d%02d%02d"),d.GetYear(),d.GetMonth(),d.GetDay());
		//t.m_sTrakt_Datum=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
	}
		
	// 4 CString
	if(m_saCommand.Field("Forvaltning").isNull())
		t.m_sTrakt_Forvaltning=_T("");  
	else
		t.m_sTrakt_Forvaltning=(LPCTSTR)m_saCommand.Field("Forvaltning").asString().GetBinaryBuffer(0);  	
		
	// 5 CString
	if(m_saCommand.Field("Distrikt").isNull())
		t.m_sTrakt_Distrikt=_T(""); 
	else
		t.m_sTrakt_Distrikt=(LPCTSTR)m_saCommand.Field("Distrikt").asString().GetBinaryBuffer(0);  	
		
	// 6 int
	if(m_saCommand.Field("Ursprung").isNull())
		t.m_nTrakt_Ursprung=MY_NULL;  
	else
		t.m_nTrakt_Ursprung=m_saCommand.Field("Ursprung").asLong();

	// 7 double
	if(m_saCommand.Field("KartaX").isNull())
		t.m_fTrakt_KartaX=MY_NULL;  
	else
		t.m_fTrakt_KartaX=m_saCommand.Field("KartaX").asDouble();

	// 8 double
	if(m_saCommand.Field("KartaY").isNull())
		t.m_fTrakt_KartaY=MY_NULL;  
	else
		t.m_fTrakt_KartaY=m_saCommand.Field("KartaY").asDouble();

	// 9 CString
	if(m_saCommand.Field("Traktnamn").isNull())
		t.m_sTrakt_Traktnamn=_T("");
	else
		t.m_sTrakt_Traktnamn=(LPCTSTR)m_saCommand.Field("Traktnamn").asString().GetBinaryBuffer(0);  	
		
	// 10 double
	if(m_saCommand.Field("Areal").isNull())
		t.m_fTrakt_Areal=MY_NULL;  
	else
		t.m_fTrakt_Areal=m_saCommand.Field("Areal").asDouble();

	// 11 int
	/*if(m_saCommand.Field("ObjNr").isNull())
		t.m_nTrakt_ObjNr=MY_NULL;  
	else
		t.m_nTrakt_ObjNr=m_saCommand.Field("ObjNr").asLong();*/

	// 12 CString
	if(m_saCommand.Field("Inventerare").isNull())
		t.m_sTrakt_Inventerare=_T("");  
	else
		t.m_sTrakt_Inventerare=(LPCTSTR)m_saCommand.Field("Inventerare").asString().GetBinaryBuffer(0);  	
		
	// 13 CString
	if(m_saCommand.Field("Entreprenor").isNull())
		t.m_sTrakt_Entreprenor=_T("");  
	else
		t.m_sTrakt_Entreprenor=(LPCTSTR)m_saCommand.Field("Entreprenor").asString().GetBinaryBuffer(0);  	
		
	// 14 CString
	if(m_saCommand.Field("Lag").isNull())
		t.m_sTrakt_Lag=_T("");  
	else
		t.m_sTrakt_Lag=(LPCTSTR)m_saCommand.Field("Lag").asString().GetBinaryBuffer(0);  	
		
	// 15 int
	if(m_saCommand.Field("MB_typ").isNull())
		t.m_nTrakt_MB_typ=MY_NULL;  
	else
		t.m_nTrakt_MB_typ=m_saCommand.Field("MB_typ").asLong();

	// 16 int
	if(m_saCommand.Field("Mal_PlHa").isNull())
		t.m_nTrakt_Mal_PlHa=MY_NULL;  
	else
		t.m_nTrakt_Mal_PlHa=m_saCommand.Field("Mal_PlHa").asLong();

	// 17 int
	if(m_saCommand.Field("Yta").isNull())
		t.m_nTrakt_Yta=MY_NULL;  
	else
		t.m_nTrakt_Yta=m_saCommand.Field("Yta").asLong();

	// 18 CString
	if(m_saCommand.Field("Anteckningar").isNull())
		t.m_sTrakt_Anteckningar=_T("");
	else
		t.m_sTrakt_Anteckningar=m_saCommand.Field("Anteckningar").asCLob();  	
	
	// 19 int
	if(m_saCommand.Field("GP1").isNull())
		t.m_nTrakt_GP1=MY_NULL;  
	else
		t.m_nTrakt_GP1=m_saCommand.Field("GP1").asLong();

	// 20 int	
	if(m_saCommand.Field("GP2").isNull())
		t.m_nTrakt_GP2=MY_NULL;  
	else
		t.m_nTrakt_GP2=m_saCommand.Field("GP2").asLong();

	// 21 int	
	if(m_saCommand.Field("GP3").isNull())
		t.m_nTrakt_GP3=MY_NULL;  
	else
		t.m_nTrakt_GP3=m_saCommand.Field("GP3").asLong();

	// 22 int	
	if(m_saCommand.Field("GP4").isNull())
		t.m_nTrakt_GP4=MY_NULL;  
	else
		t.m_nTrakt_GP4=m_saCommand.Field("GP4").asLong();

	// 23 int	
	if(m_saCommand.Field("GP5").isNull())
		t.m_nTrakt_GP5=MY_NULL;  
	else
		t.m_nTrakt_GP5=m_saCommand.Field("GP5").asLong();

	// 24 int
	if(m_saCommand.Field("EjGP1").isNull())
		t.m_nTrakt_EjGP1=MY_NULL;  
	else
		t.m_nTrakt_EjGP1=m_saCommand.Field("EjGP1").asLong();

	// 25 int
	if(m_saCommand.Field("EjGP2").isNull())
		t.m_nTrakt_EjGP2=MY_NULL;  
	else
		t.m_nTrakt_EjGP2=m_saCommand.Field("EjGP2").asLong();

	// 26 int
	if(m_saCommand.Field("EjGP3").isNull())
		t.m_nTrakt_EjGP3=MY_NULL;  
	else
		t.m_nTrakt_EjGP3=m_saCommand.Field("EjGP3").asLong();

	// 27 int		
	if(m_saCommand.Field("EjGP4").isNull())
		t.m_nTrakt_EjGP4=MY_NULL;  
	else
		t.m_nTrakt_EjGP4=m_saCommand.Field("EjGP4").asLong();

	// 28 int	
	if(m_saCommand.Field("EjGP5").isNull())
		t.m_nTrakt_EjGP5=MY_NULL;  
	else
		t.m_nTrakt_EjGP5=m_saCommand.Field("EjGP5").asLong();

	// 29 int
	if(m_saCommand.Field("EjGP6").isNull())
		t.m_nTrakt_EjGP6=MY_NULL;  
	else
		t.m_nTrakt_EjGP6=m_saCommand.Field("EjGP6").asLong();

	// 30 int	
	if(m_saCommand.Field("BattrePlantp").isNull())
		t.m_nTrakt_BattrePlantp=MY_NULL;  
	else
		t.m_nTrakt_BattrePlantp=m_saCommand.Field("BattrePlantp").asLong();

	// 31 int
	if(m_saCommand.Field("OutnPlantp").isNull())
		t.m_nTrakt_OutnPlantp=MY_NULL;  
	else
		t.m_nTrakt_OutnPlantp=m_saCommand.Field("OutnPlantp").asLong();

	// 32 int
	if(m_saCommand.Field("Plantvard").isNull())
		t.m_nTrakt_Plantvard=MY_NULL;  
	else
		t.m_nTrakt_Plantvard=m_saCommand.Field("Plantvard").asLong();

	// 33 int
	if(m_saCommand.Field("Stadat").isNull())
		t.m_nTrakt_Stadat=MY_NULL;  
	else
		t.m_nTrakt_Stadat=m_saCommand.Field("Stadat").asLong();

	// 34 int
	if(m_saCommand.Field("SkildaS").isNull())
		t.m_nTrakt_SkildaS=MY_NULL;  
	else
		t.m_nTrakt_SkildaS=m_saCommand.Field("SkildaS").asLong();

	// 35 int	
	if(m_saCommand.Field("Redovisat").isNull())
		t.m_nTrakt_Redovisat=MY_NULL;  
	else
		t.m_nTrakt_Redovisat=m_saCommand.Field("Redovisat").asLong();

	// 36 int	
	if(m_saCommand.Field("Invtyp").isNull())
		t.m_nTrakt_InvTyp=MY_NULL;  
	else
		t.m_nTrakt_InvTyp=m_saCommand.Field("Invtyp").asLong();

	// 37 int	
	if(m_saCommand.Field("GP6").isNull())
		t.m_nTrakt_GP6=MY_NULL;  
	else
		t.m_nTrakt_GP6=m_saCommand.Field("GP6").asLong();

	if(m_saCommand.Field("OwnerId").isNull())
		t.m_sTrakt_OwnerId=_T("");
	else
		t.m_sTrakt_OwnerId=(LPCTSTR)m_saCommand.Field("OwnerId").asString().GetBinaryBuffer(0);  	

	if(m_saCommand.Field("AtgardsId").isNull())
		t.m_sTrakt_AtgardsId=_T("");
	else
		t.m_sTrakt_AtgardsId=(LPCTSTR)m_saCommand.Field("AtgardsId").asString().GetBinaryBuffer(0);  	
	
	if(m_saCommand.Field("HogtSnytbaggetryck").isNull())
		t.m_nTrakt_HogtSnytbaggetryck = MY_NULL;  
	else
		t.m_nTrakt_HogtSnytbaggetryck = m_saCommand.Field("HogtSnytbaggetryck").asLong();
}

void CScaGallBasDB::Load_Trakt_Variables(vecTraktData_Mark &vec)
{
	int i=1,spec=0,var=0;
	TRAKT_DATA_MARK t;
	t=TRAKT_DATA_MARK();

	Load_Trakt_Variables(t);
	vec.push_back(t);
}

void CScaGallBasDB::Load_Trakt_Variables(vecTraktData_Plant &vec)
{
	int i=1,spec=0,var=0;
	TRAKT_DATA_PLANT t;
	t=TRAKT_DATA_PLANT();

	Load_Trakt_Variables(t);
	vec.push_back(t);

}

void CScaGallBasDB::Load_Trakt_Variables(vecTraktData_Roj &vec)
{
	int i=1,spec=0,var=0;
	TRAKT_DATA_ROJ t;
	t=TRAKT_DATA_ROJ();

	Load_Trakt_Variables(t);
	vec.push_back(t);
}

BOOL CScaGallBasDB::getTraktIndex(vecTraktIndex_Mark &vec)
{
	TCHAR szBuffer[1024];

	CString err;
	int check=0;
	double check2=0.0;
	try
	{
		vec.clear();
		getSQLTraktQuestionIndex_Mark(szBuffer);
		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
				vec.push_back(_trakt_index_mark(
					m_saCommand.Field(1).asLong()));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_MARK_TRAKT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}

BOOL CScaGallBasDB::getTraktIndex(vecTraktIndex_Plant &vec)
{
	TCHAR szBuffer[1024];

	CString err;
	int check=0;
	double check2=0.0;
	try
	{
		vec.clear();
		getSQLTraktQuestionIndex_Plant(szBuffer);
		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
				vec.push_back(_trakt_index_plant(
					m_saCommand.Field(1).asLong()));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_PLANT_TRAKT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}


BOOL CScaGallBasDB::getTraktIndex(vecTraktIndex_Roj &vec)
{
	TCHAR szBuffer[1024];

	CString err;
	int check=0;
	double check2=0.0;
	try
	{
		vec.clear();
		getSQLTraktQuestionIndex_Roj(szBuffer);
		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
				vec.push_back(_trakt_index_roj(
					m_saCommand.Field(1).asLong()));
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_ROJ_TRAKT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}

BOOL CScaGallBasDB::getTrakt(int Lopnr,TRAKT_DATA_MARK &trkt)
{
	CString str=_T(""),err=_T("");
	try
	{
		str.Format(_T("select * from %s where Lopnr = %d"),TBL_MARK_TRAKT,Lopnr);

		m_saCommand.setCommandText((SAString)str);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			Load_Trakt_Variables(trkt);
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_MARK_TRAKT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}

BOOL CScaGallBasDB::getTrakt(int Lopnr,TRAKT_DATA_PLANT &trkt)
{
	CString str=_T(""),err=_T("");
	try
	{
		str.Format(_T("select * from %s where Lopnr = %d"),TBL_PLANT_TRAKT,Lopnr);

		m_saCommand.setCommandText((SAString)str);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			Load_Trakt_Variables(trkt);
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_PLANT_TRAKT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}

BOOL CScaGallBasDB::getTrakt(int Lopnr,TRAKT_DATA_ROJ &trkt)
{
	CString str=_T(""),err=_T("");
	try
	{
		str.Format(_T("select * from %s where Lopnr = %d"),TBL_ROJ_TRAKT,Lopnr);

		m_saCommand.setCommandText((SAString)str);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			Load_Trakt_Variables(trkt);
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_ROJ_TRAKT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}

BOOL CScaGallBasDB::getTrakts(vecTraktData_Mark &vec,CString sSqlQues,int var)
{
	int check=-1,i=1;
	TRAKT_DATA_MARK t;
	t=TRAKT_DATA_MARK();
	try
	{
		vec.clear();
		m_saCommand.setCommandText((SAString)sSqlQues);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			switch(var)
			{
			case 0:
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_Forvaltning=_T("");  
				else
					t.m_sTrakt_Forvaltning=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
				vec.push_back(t);
				break;
			case 1:	
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_Distrikt=_T(""); 
				else
					t.m_sTrakt_Distrikt=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
				vec.push_back(t);
				break;
			case 2:	
				if(m_saCommand.Field(i).isNull())
					t.m_nTrakt_Ursprung=MY_NULL; 
				else
					t.m_nTrakt_Ursprung=m_saCommand.Field(i).asLong();  	
				vec.push_back(t);
				break;	
			case 3:	
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_AtgardsId = _T("");
				else
					t.m_sTrakt_AtgardsId = (LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);
				vec.push_back(t);
				break;
			case 4:	
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_Entreprenor=_T(""); 
				else
					t.m_sTrakt_Entreprenor=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
				vec.push_back(t);
				break;
			case 5:	
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_Lag=_T(""); 
				else
					t.m_sTrakt_Lag=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
				vec.push_back(t);
				break;
			case 6:	
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_Inventerare=_T(""); 
				else
					t.m_sTrakt_Inventerare=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
				vec.push_back(t);
				break;
			case 7:	
				if(m_saCommand.Field(i).isNull())
					t.m_nTrakt_Metod=MY_NULL; 
				else
					t.m_nTrakt_Metod=m_saCommand.Field(i).asLong();  	
				vec.push_back(t);
				break;
			case 8:	
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_Aggr=_T(""); 
				else
					t.m_sTrakt_Aggr=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
				vec.push_back(t);
				break;

			}
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message

		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}


BOOL CScaGallBasDB::getTrakts(vecTraktData_Plant &vec,CString sSqlQues,int var)
{
	int check=-1,i=1;
	TRAKT_DATA_PLANT t;
	t=TRAKT_DATA_PLANT();
	try
	{
		vec.clear();
		m_saCommand.setCommandText((SAString)sSqlQues);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			switch(var)
			{
			case 0:
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_Forvaltning=_T("");  
				else
					t.m_sTrakt_Forvaltning=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
				vec.push_back(t);
				break;
			case 1:	
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_Distrikt=_T(""); 
				else
					t.m_sTrakt_Distrikt=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
				vec.push_back(t);
				break;
			case 2:	
				if(m_saCommand.Field(i).isNull())
					t.m_nTrakt_Ursprung=MY_NULL; 
				else
					t.m_nTrakt_Ursprung=m_saCommand.Field(i).asLong();  	
				vec.push_back(t);
				break;	
			case 3:	
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_AtgardsId = _T("");
				else
					t.m_sTrakt_AtgardsId = (LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
				vec.push_back(t);
				break;
			case 4:	
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_Entreprenor=_T(""); 
				else
					t.m_sTrakt_Entreprenor=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
				vec.push_back(t);
				break;
			case 5:	
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_Lag=_T(""); 
				else
					t.m_sTrakt_Lag=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
				vec.push_back(t);
				break;
			case 6:	
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_Inventerare=_T(""); 
				else
					t.m_sTrakt_Inventerare=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
				vec.push_back(t);
				break;
			case 7:	
				if(m_saCommand.Field(i).isNull())
					t.m_nTrakt_MB_typ=MY_NULL; 
				else
					t.m_nTrakt_MB_typ=m_saCommand.Field(i).asLong();  	
				vec.push_back(t);
				break;

			
			}
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message

		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}


BOOL CScaGallBasDB::getTrakts(vecTraktData_Roj &vec,CString sSqlQues,int var)
{
	int check=-1,i=1;
	TRAKT_DATA_ROJ t;
	t=TRAKT_DATA_ROJ();
	try
	{
		vec.clear();
		m_saCommand.setCommandText((SAString)sSqlQues);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			switch(var)
			{
			case 0:
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_Forvaltning=_T("");  
				else
					t.m_sTrakt_Forvaltning=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
				vec.push_back(t);
				break;
			case 1:	
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_Distrikt=_T(""); 
				else
					t.m_sTrakt_Distrikt=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
				vec.push_back(t);
				break;
			case 2:	
				if(m_saCommand.Field(i).isNull())
					t.m_nTrakt_Ursprung=MY_NULL; 
				else
					t.m_nTrakt_Ursprung=m_saCommand.Field(i).asLong();  	
				vec.push_back(t);
				break;	
			case 3:	
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_AtgardsId = _T("");
				else
					t.m_sTrakt_AtgardsId = (LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);
				vec.push_back(t);
				break;
			case 4:	
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_Entreprenor=_T(""); 
				else
					t.m_sTrakt_Entreprenor=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
				vec.push_back(t);
				break;
			case 5:	
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_Lag=_T(""); 
				else
					t.m_sTrakt_Lag=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
				vec.push_back(t);
				break;
			case 6:	
				if(m_saCommand.Field(i).isNull())
					t.m_sTrakt_Inventerare=_T(""); 
				else
					t.m_sTrakt_Inventerare=(LPCTSTR)m_saCommand.Field(i).asString().GetBinaryBuffer(0);  	
				vec.push_back(t);
				break;
			}
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message

		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}

BOOL CScaGallBasDB::getTrakts(vecTraktData_Mark &vec,CString sSqlQues)
{
	TRAKT_DATA_MARK t;
	t=TRAKT_DATA_MARK();
	try
	{
		vec.clear();
		m_saCommand.setCommandText((SAString)sSqlQues);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			Load_Trakt_Variables(vec);
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}

BOOL CScaGallBasDB::getTrakts(vecTraktData_Plant &vec,CString sSqlQues)
{
	TRAKT_DATA_PLANT t;
	t=TRAKT_DATA_PLANT();
	try
	{
		vec.clear();
		m_saCommand.setCommandText((SAString)sSqlQues);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			Load_Trakt_Variables(vec);
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}

BOOL CScaGallBasDB::getTrakts(vecTraktData_Roj &vec,CString sSqlQues)
{
	TRAKT_DATA_ROJ t;
	t=TRAKT_DATA_ROJ();
	try
	{
		vec.clear();
		m_saCommand.setCommandText((SAString)sSqlQues);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			Load_Trakt_Variables(vec);
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}

/*BOOL CScaGallBasDB::getTrakts(vecTraktData_Mark &vec)
{
	TCHAR szBuffer[1024];
	int check=-1;
	try
	{
		vec.clear();
		getSQLTraktQuestion_Mark(szBuffer);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			Load_Trakt_Variables(vec);
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message

		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}

BOOL CScaGallBasDB::getTrakts(vecTraktData_Plant &vec)
{
	TCHAR szBuffer[1024];
	int check=-1;
	try
	{
		vec.clear();
		getSQLTraktQuestion_Plant(szBuffer);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			Load_Trakt_Variables(vec);
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message

		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}

BOOL CScaGallBasDB::getTrakts(vecTraktData_Roj &vec)
{
	TCHAR szBuffer[1024];
	int check=-1;
	try
	{
		vec.clear();
		getSQLTraktQuestion_Roj(szBuffer);

		m_saCommand.setCommandText((SAString)szBuffer);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			Load_Trakt_Variables(vec);
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message

		AfxMessageBox((LPCTSTR)(e.ErrText().GetBinaryBuffer(0)));
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}*/

BOOL CScaGallBasDB::getPlots(TRAKT_DATA_MARK &trkt)
{
	CString str=_T(""),err=_T("");
	PLOT_DATA_MARK tPlot;
	tPlot=PLOT_DATA_MARK();
	try
	{
		trkt.m_vPlotData.clear();
		str.Format(_T("select * from %s where traktId = %d  order by YtNr"),
			TBL_MARK_PLOT,
			trkt.m_nTrakt_Id);

		m_saCommand.setCommandText((SAString)str);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			if(m_saCommand.Field("traktId").isNull())
				tPlot.m_nPlot_Trakt_Id=MY_NULL;  
			else
				tPlot.m_nPlot_Trakt_Id=m_saCommand.Field("traktId").asLong();
			
			if(m_saCommand.Field("YtNr").isNull())
				tPlot.m_nPlot_YtNr=MY_NULL;  
			else
				tPlot.m_nPlot_YtNr=m_saCommand.Field("YtNr").asLong();
			
			if(m_saCommand.Field("KoordinatX").isNull())
				tPlot.m_fPlot_KoordinatX=MY_NULL;  
			else
				tPlot.m_fPlot_KoordinatX=m_saCommand.Field("KoordinatX").asDouble(); 
			
			if(m_saCommand.Field("KoordinatY").isNull())
				tPlot.m_fPlot_KoordinatY=MY_NULL;  
			else
				tPlot.m_fPlot_KoordinatY=m_saCommand.Field("KoordinatY").asDouble(); 
			
			if(m_saCommand.Field("Anteckningar").isNull())
				tPlot.m_sPlot_Anteckningar=_T("");
			else
				tPlot.m_sPlot_Anteckningar=m_saCommand.Field("Anteckningar").asCLob();
			
			if(m_saCommand.Field("Rader100m").isNull())
				tPlot.m_nPlot_Rader=MY_NULL;  
			else
				tPlot.m_nPlot_Rader=m_saCommand.Field("Rader100m").asLong();
			
			if(m_saCommand.Field("Standort").isNull())
				tPlot.m_sPlot_Standort==_T("");  
			else
				tPlot.m_sPlot_Standort=m_saCommand.Field("Standort").asCLob();
			
			if(m_saCommand.Field("poeng1").isNull())
				tPlot.m_nPlot_poeng1=MY_NULL;  
			else
				tPlot.m_nPlot_poeng1=m_saCommand.Field("poeng1").asLong();
			
			if(m_saCommand.Field("poeng2").isNull())
				tPlot.m_nPlot_poeng2=MY_NULL;  
			else
				tPlot.m_nPlot_poeng2=m_saCommand.Field("poeng2").asLong();
			
			if(m_saCommand.Field("poengM").isNull())
				tPlot.m_nPlot_poengM=MY_NULL;  
			else
				tPlot.m_nPlot_poengM=m_saCommand.Field("poengM").asLong();
			
			if(m_saCommand.Field("poengHT").isNull())
				tPlot.m_nPlot_poengHT=MY_NULL;  
			else
				tPlot.m_nPlot_poengHT=m_saCommand.Field("poengHT").asLong();
			
			if(m_saCommand.Field("poeng4").isNull())
				tPlot.m_nPlot_poeng4=MY_NULL;  
			else
				tPlot.m_nPlot_poeng4=m_saCommand.Field("poeng4").asLong();
			
			if(m_saCommand.Field("poeng5").isNull())
				tPlot.m_nPlot_poeng5=MY_NULL;  
			else
				tPlot.m_nPlot_poeng5=m_saCommand.Field("poeng5").asLong();
			
			if(m_saCommand.Field("poeng7x7").isNull())
				tPlot.m_nPlot_poeng7x7=MY_NULL;  
			else
				tPlot.m_nPlot_poeng7x7=m_saCommand.Field("poeng7x7").asLong();
			
			if(m_saCommand.Field("Poeng3").isNull())
				tPlot.m_nPlot_poeng3=MY_NULL;  
			else
				tPlot.m_nPlot_poeng3=m_saCommand.Field("Poeng3").asLong();
			
			if(m_saCommand.Field("PoengSkadadeLagor").isNull())
				tPlot.m_nPlot_PoengSkadadeLagor=MY_NULL;  
			else
				tPlot.m_nPlot_PoengSkadadeLagor=m_saCommand.Field("PoengSkadadeLagor").asLong();
			
			if(m_saCommand.Field("PoengTorv3").isNull())
				tPlot.m_nPlot_PoengTorv3=MY_NULL;  
			else
				tPlot.m_nPlot_PoengTorv3=m_saCommand.Field("PoengTorv3").asLong();
			
			if(m_saCommand.Field("PoengMineral3").isNull())
				tPlot.m_nPlot_PoengMineral3=MY_NULL;  
			else
				tPlot.m_nPlot_PoengMineral3=m_saCommand.Field("PoengMineral3").asLong();
			
			if(m_saCommand.Field("PoengHumus3").isNull())
				tPlot.m_nPlot_PoengHumus3=MY_NULL;  
			else
				tPlot.m_nPlot_PoengHumus3=m_saCommand.Field("PoengHumus3").asLong();
			
			if(m_saCommand.Field("PoengAntalLagor").isNull())
				tPlot.m_nPlot_PoengAntalLagor=MY_NULL;  
			else
				tPlot.m_nPlot_PoengAntalLagor=m_saCommand.Field("PoengAntalLagor").asLong();


			if(m_saCommand.Field("SkadadeFornminnen").isNull())
				tPlot.m_nPlot_SkadadeFornminnen = MY_NULL;  
			else
				tPlot.m_nPlot_SkadadeFornminnen = m_saCommand.Field("SkadadeFornminnen").asLong();

			if(m_saCommand.Field("SkadadeKulturminnen").isNull())
				tPlot.m_nPlot_SkadadeKulturminnen = MY_NULL;  
			else
				tPlot.m_nPlot_SkadadeKulturminnen = m_saCommand.Field("SkadadeKulturminnen").asLong();

			if(m_saCommand.Field("Korskador").isNull())
				tPlot.m_nPlot_Korskador = MY_NULL;  
			else
				tPlot.m_nPlot_Korskador = m_saCommand.Field("Korskador").asLong();

			trkt.m_vPlotData.push_back(tPlot);
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_PLANT_PLOT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}



BOOL CScaGallBasDB::getPlots(TRAKT_DATA_PLANT &trkt)
{
	CString str=_T(""),err=_T("");

	PLOT_DATA_PLANT tPlot;
	tPlot=PLOT_DATA_PLANT();
	try
	{
		trkt.m_vPlotData.clear();
		str.Format(_T("select * from %s where traktId = %d  order by YtNr"),
				TBL_PLANT_PLOT,
				trkt.m_nTrakt_Id);

		m_saCommand.setCommandText((SAString)str);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			// 1 int 
			if(m_saCommand.Field("traktId").isNull())
				tPlot.m_nPlot_Trakt_Id=MY_NULL;  
			else
				tPlot.m_nPlot_Trakt_Id=m_saCommand.Field("traktId").asLong();
			
			// 2 int 
			if(m_saCommand.Field("YtNr").isNull())
				tPlot.m_nPlot_YtNr=MY_NULL;  
			else
				tPlot.m_nPlot_YtNr=m_saCommand.Field("YtNr").asLong();
			
			// 3 double 
			if(m_saCommand.Field("KoordinatX").isNull())
				tPlot.m_fPlot_KoordinatX=MY_NULL;  
			else
				tPlot.m_fPlot_KoordinatX=m_saCommand.Field("KoordinatX").asDouble(); 
			
			// 4 double 
			if(m_saCommand.Field("KoordinatY").isNull())
				tPlot.m_fPlot_KoordinatY=MY_NULL;  
			else
				tPlot.m_fPlot_KoordinatY=m_saCommand.Field("KoordinatY").asDouble(); 
			
			// 5 int 
			if(m_saCommand.Field("Med_mineral").isNull())
				tPlot.m_nPlot_Med_mineral=MY_NULL;  
			else
				tPlot.m_nPlot_Med_mineral=m_saCommand.Field("Med_mineral").asLong();
			
			// 6 int 
			if(m_saCommand.Field("Utan_mineral").isNull())
				tPlot.m_nPlot_Utan_mineral=MY_NULL;  
			else
				tPlot.m_nPlot_Utan_mineral=m_saCommand.Field("Utan_mineral").asLong();
			
			// 7 int 
			if(m_saCommand.Field("Hogt_mineral").isNull())
				tPlot.m_nPlot_Hogt_mineral=MY_NULL;  
			else
				tPlot.m_nPlot_Hogt_mineral=m_saCommand.Field("Hogt_mineral").asLong();
			
			// 8 int 
			if(m_saCommand.Field("Hogt_humus").isNull())
				tPlot.m_nPlot_Hogt_humus=MY_NULL;  
			else
				tPlot.m_nPlot_Hogt_humus=m_saCommand.Field("Hogt_humus").asLong();
			
			// 9 int 
			if(m_saCommand.Field("Behandlad").isNull())
				tPlot.m_nPlot_Behandlad=MY_NULL;  
			else
				tPlot.m_nPlot_Behandlad=m_saCommand.Field("Behandlad").asLong();
			
			// 10 int 
			if(m_saCommand.Field("Summa_GK").isNull())
				tPlot.m_nPlot_Summa_GK=MY_NULL;  
			else
				tPlot.m_nPlot_Summa_GK=m_saCommand.Field("Summa_GK").asLong();
			
			// 11 int 
			if(m_saCommand.Field("Ej_djup").isNull())
				tPlot.m_nPlot_Ej_djup=MY_NULL;  
			else
				tPlot.m_nPlot_Ej_djup=m_saCommand.Field("Ej_djup").asLong();
			
			// 12 int 
			if(m_saCommand.Field("Ej_punkt").isNull())
				tPlot.m_nPlot_Ej_punkt=MY_NULL;  
			else
				tPlot.m_nPlot_Ej_punkt=m_saCommand.Field("Ej_punkt").asLong();
			
			// 13 int 
			if(m_saCommand.Field("Ej_10cm").isNull())
				tPlot.m_nPlot_Ej_10cm=MY_NULL;  
			else
				tPlot.m_nPlot_Ej_10cm=m_saCommand.Field("Ej_10cm").asLong();
			
			// 14 int 
			if(m_saCommand.Field("Ej_avstand").isNull())
				tPlot.m_nPlot_Ej_avstand=MY_NULL;  
			else
				tPlot.m_nPlot_Ej_avstand=m_saCommand.Field("Ej_avstand").asLong();
			
			// 15 int 
			if(m_saCommand.Field("Hogt").isNull())
				tPlot.m_nPlot_Hogt=MY_NULL;  
			else
				tPlot.m_nPlot_Hogt=m_saCommand.Field("Hogt").asLong();
			
			// 16 int 
			if(m_saCommand.Field("Ovriga").isNull())
				tPlot.m_nPlot_Ovriga=MY_NULL;  
			else
				tPlot.m_nPlot_Ovriga=m_saCommand.Field("Ovriga").asLong();
			
			// 17 int 
			if(m_saCommand.Field("Summg_EGK").isNull())
				tPlot.m_nPlot_Summg_EGK=MY_NULL;  
			else
				tPlot.m_nPlot_Summg_EGK=m_saCommand.Field("Summg_EGK").asLong();
			
			// 18 int 
			if(m_saCommand.Field("Battre").isNull())
				tPlot.m_nPlot_Battre=MY_NULL;  
			else
				tPlot.m_nPlot_Battre=m_saCommand.Field("Battre").asLong();
			
			// 19 int 
			if(m_saCommand.Field("Outnyttj").isNull())
				tPlot.m_nPlot_Outnyttj=MY_NULL;  
			else
				tPlot.m_nPlot_Outnyttj=m_saCommand.Field("Outnyttj").asLong();
			
			// 20 CString 
			if(m_saCommand.Field("Anteckningar").isNull())
				tPlot.m_sPlot_Anteckningar=_T("");  
			else
				tPlot.m_sPlot_Anteckningar=m_saCommand.Field("Anteckningar").asCLob();
			
			// 21 int 
			if(m_saCommand.Field("Hogt_torv").isNull())
				tPlot.m_nPlot_Hogt_torv=MY_NULL;  
			else
				tPlot.m_nPlot_Hogt_torv=m_saCommand.Field("Hogt_torv").asLong();

			// 22 int
			if(m_saCommand.Field("Standort").isNull())
				tPlot.m_nPlot_Standort = MY_NULL;  
			else
				tPlot.m_nPlot_Standort = m_saCommand.Field("Standort").asLong();

			trkt.m_vPlotData.push_back(tPlot);
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_PLANT_PLOT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}

BOOL CScaGallBasDB::getPlots(TRAKT_DATA_ROJ &trkt)
{
	CString str=_T(""),err=_T("");
	
	PLOT_DATA_ROJ tPlot;
	tPlot=PLOT_DATA_ROJ();
	try
	{
		trkt.m_vPlotData.clear();
		str.Format(_T("select * from %s where traktId = %d  order by YtNr"),
				TBL_ROJ_PLOT,
				trkt.m_nTrakt_Id);

		m_saCommand.setCommandText((SAString)str);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			if(m_saCommand.Field("traktId").isNull())
				tPlot.m_nPlot_Trakt_Id=MY_NULL;  
			else
				tPlot.m_nPlot_Trakt_Id=m_saCommand.Field("traktId").asLong();
			
			if(m_saCommand.Field("YtNr").isNull())
				tPlot.m_nPlot_YtNr=MY_NULL;  
			else
				tPlot.m_nPlot_YtNr=m_saCommand.Field("YtNr").asLong();
			
			if(m_saCommand.Field("KoordinatX").isNull())
				tPlot.m_fPlot_KoordinatX=MY_NULL;  
			else
				tPlot.m_fPlot_KoordinatX=m_saCommand.Field("KoordinatX").asDouble(); 
			
			if(m_saCommand.Field("KoordinatY").isNull())
				tPlot.m_fPlot_KoordinatY=MY_NULL;  
			else
				tPlot.m_fPlot_KoordinatY=m_saCommand.Field("KoordinatY").asDouble(); 
			
			if(m_saCommand.Field("StPerHa").isNull())
				tPlot.m_nPlot_StPerHa=MY_NULL;  
			else
				tPlot.m_nPlot_StPerHa=m_saCommand.Field("StPerHa").asLong();
			
			if(m_saCommand.Field("Rojd").isNull())
				tPlot.m_nPlot_Rojd=MY_NULL;  
			else
				tPlot.m_nPlot_Rojd=m_saCommand.Field("Rojd").asLong();
			
			if(m_saCommand.Field("Rojbehov").isNull())
				tPlot.m_nPlot_Rojbehov=MY_NULL;  
			else
				tPlot.m_nPlot_Rojbehov=m_saCommand.Field("Rojbehov").asLong();
			
			if(m_saCommand.Field("TrslVal").isNull())
				tPlot.m_nPlot_TrslVal=MY_NULL;  
			else
				tPlot.m_nPlot_TrslVal=m_saCommand.Field("TrslVal").asLong();
			
			if(m_saCommand.Field("Stamval").isNull())
				tPlot.m_nPlot_Stamval=MY_NULL;  
			else
				tPlot.m_nPlot_Stamval=m_saCommand.Field("Stamval").asLong();
			
			if(m_saCommand.Field("Stamantal").isNull())
				tPlot.m_nPlot_Stamantal=MY_NULL;  
			else
				tPlot.m_nPlot_Stamantal=m_saCommand.Field("Stamantal").asLong();
			
			if(m_saCommand.Field("AntalHsTall").isNull())
				tPlot.m_nPlot_AntalHsTall=MY_NULL;  
			else
				tPlot.m_nPlot_AntalHsTall=m_saCommand.Field("AntalHsTall").asLong();
			
			if(m_saCommand.Field("MhojdTall").isNull())
				tPlot.m_nPlot_MhojdTall=MY_NULL;  
			else
				tPlot.m_nPlot_MhojdTall=m_saCommand.Field("MhojdTall").asLong();
			
			if(m_saCommand.Field("AntalHsGran").isNull())
				tPlot.m_nPlot_AntalHsGran=MY_NULL;  
			else
				tPlot.m_nPlot_AntalHsGran=m_saCommand.Field("AntalHsGran").asLong();
			
			if(m_saCommand.Field("MhojdGran").isNull())
				tPlot.m_nPlot_MhojdGran=MY_NULL;  
			else
				tPlot.m_nPlot_MhojdGran=m_saCommand.Field("MhojdGran").asLong();
			
			if(m_saCommand.Field("AntalHsCont").isNull())
				tPlot.m_nPlot_AntalHsCont=MY_NULL;  
			else
				tPlot.m_nPlot_AntalHsCont=m_saCommand.Field("AntalHsCont").asLong();
			
			if(m_saCommand.Field("MhojdCont").isNull())
				tPlot.m_nPlot_MhojdCont=MY_NULL;  
			else
				tPlot.m_nPlot_MhojdCont=m_saCommand.Field("MhojdCont").asLong();
			
			if(m_saCommand.Field("AntalHsBjork").isNull())
				tPlot.m_nPlot_AntalHsBjork=MY_NULL;  
			else
				tPlot.m_nPlot_AntalHsBjork=m_saCommand.Field("AntalHsBjork").asLong();
			
			if(m_saCommand.Field("MhojdBjork").isNull())
				tPlot.m_nPlot_MhojdBjork=MY_NULL;  
			else
				tPlot.m_nPlot_MhojdBjork=m_saCommand.Field("MhojdBjork").asLong();
			
			if(m_saCommand.Field("AntalBi").isNull())
				tPlot.m_nPlot_AntalBi=MY_NULL;  
			else
				tPlot.m_nPlot_AntalBi=m_saCommand.Field("AntalBi").asLong();
			
			if(m_saCommand.Field("MhojdBi").isNull())
				tPlot.m_nPlot_MhojdBi=MY_NULL;  
			else
				tPlot.m_nPlot_MhojdBi=m_saCommand.Field("MhojdBi").asLong();
			
			if(m_saCommand.Field("AntalRo").isNull())
				tPlot.m_nPlot_AntalRo=MY_NULL;  
			else
				tPlot.m_nPlot_AntalRo=m_saCommand.Field("AntalRo").asLong();
			
			if(m_saCommand.Field("MhojdRo").isNull())
				tPlot.m_nPlot_MhojdRo=MY_NULL;  
			else
				tPlot.m_nPlot_MhojdRo=m_saCommand.Field("MhojdRo").asLong();
			
			if(m_saCommand.Field("AntalOr").isNull())
				tPlot.m_nPlot_AntalOr=MY_NULL;  
			else
				tPlot.m_nPlot_AntalOr=m_saCommand.Field("AntalOr").asLong();
			
			if(m_saCommand.Field("MhojdOr").isNull())
				tPlot.m_nPlot_MhojdOr=MY_NULL;  
			else
				tPlot.m_nPlot_MhojdOr=m_saCommand.Field("MhojdOr").asLong();
			
			if(m_saCommand.Field("AntalHa").isNull())
				tPlot.m_nPlot_AntalHa=MY_NULL;  
			else
				tPlot.m_nPlot_AntalHa=m_saCommand.Field("AntalHa").asLong();
			
			if(m_saCommand.Field("HansynsSt").isNull())
				tPlot.m_nPlot_HansynsSt=MY_NULL;  
			else
				tPlot.m_nPlot_HansynsSt=m_saCommand.Field("HansynsSt").asLong();
			
			if(m_saCommand.Field("Myr").isNull())
				tPlot.m_nPlot_Myr=MY_NULL;  
			else
				tPlot.m_nPlot_Myr=m_saCommand.Field("Myr").asLong();
			
			if(m_saCommand.Field("Vattendrag").isNull())
				tPlot.m_nPlot_Vattendrag=MY_NULL;  
			else
				tPlot.m_nPlot_Vattendrag=m_saCommand.Field("Vattendrag").asLong();
			
			if(m_saCommand.Field("Sjo").isNull())
				tPlot.m_nPlot_Sjo=MY_NULL;  
			else
				tPlot.m_nPlot_Sjo=m_saCommand.Field("Sjo").asLong();
			
			if(m_saCommand.Field("Annat").isNull())
				tPlot.m_nPlot_Annat=MY_NULL;  
			else
				tPlot.m_nPlot_Annat=m_saCommand.Field("Annat").asLong();
			
			if(m_saCommand.Field("Surdrag").isNull())
				tPlot.m_nPlot_Surdrag=MY_NULL;  
			else
				tPlot.m_nPlot_Surdrag=m_saCommand.Field("Surdrag").asLong();
			
			if(m_saCommand.Field("OvrigaBio").isNull())
				tPlot.m_nPlot_OvrigaBio=MY_NULL;  
			else
				tPlot.m_nPlot_OvrigaBio=m_saCommand.Field("OvrigaBio").asLong();
			
			if(m_saCommand.Field("Kultur").isNull())
				tPlot.m_nPlot_Kultur=MY_NULL;  
			else
				tPlot.m_nPlot_Kultur=m_saCommand.Field("Kultur").asLong();
			
			if(m_saCommand.Field("AlgskadorNya").isNull())
				tPlot.m_nPlot_AlgskadorNya=MY_NULL;  
			else
				tPlot.m_nPlot_AlgskadorNya=m_saCommand.Field("AlgskadorNya").asLong();
			
			if(m_saCommand.Field("AlgskadorGamla").isNull())
				tPlot.m_nPlot_AlgskadorGamla=MY_NULL;  
			else
				tPlot.m_nPlot_AlgskadorGamla=m_saCommand.Field("AlgskadorGamla").asLong();
			
			if(m_saCommand.Field("Knackesjuka").isNull())
				tPlot.m_nPlot_Knackesjuka=MY_NULL;  
			else
				tPlot.m_nPlot_Knackesjuka=m_saCommand.Field("Knackesjuka").asLong();
			
			if(m_saCommand.Field("Gremeniella").isNull())
				tPlot.m_nPlot_Gremeniella=MY_NULL;  
			else
				tPlot.m_nPlot_Gremeniella=m_saCommand.Field("Gremeniella").asLong();
			
			if(m_saCommand.Field("Ovriga").isNull())
				tPlot.m_nPlot_Ovriga=MY_NULL;  
			else
				tPlot.m_nPlot_Ovriga=m_saCommand.Field("Ovriga").asLong();
			
			if(m_saCommand.Field("Anteckningar").isNull())
				tPlot.m_sPlot_Anteckningar=_T("");  
			else
				tPlot.m_sPlot_Anteckningar=m_saCommand.Field("Anteckningar").asCLob();

			trkt.m_vPlotData.push_back(tPlot);
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		err=e.ErrText();
		err+=_T("\nKan inte l�sa data fr�n Tabell= ");
		err+=TBL_ROJ_PLOT;
		AfxMessageBox(err);
		m_saConnection.Rollback();
		return FALSE;
	}
	return TRUE;
}


void CScaGallBasDB::getSQLPlotQuestion(LPTSTR sql)
{
	_stprintf(sql,SQL_PLOT_QUESTION1,TBL_PLANT_PLOT);
}


/*void CScaGallBasDB::getSQLTraktQuestion_Plant(LPTSTR sql)
{
	_stprintf(sql,SQL_TRAKT_QUESTION1,TBL_PLANT_TRAKT);
}

void CScaGallBasDB::getSQLTraktQuestion_Mark(LPTSTR sql)
{
	_stprintf(sql,SQL_TRAKT_QUESTION1,TBL_MARK_TRAKT);
}

void CScaGallBasDB::getSQLTraktQuestion_Roj(LPTSTR sql)
{
	_stprintf(sql,SQL_TRAKT_QUESTION1,TBL_ROJ_TRAKT);
}*/

void CScaGallBasDB::getSQLTraktQuestionIndex_Plant(LPTSTR sql)
{
	_stprintf(sql,SQL_TRAKT_QUESTION_INDEX1,TBL_PLANT_TRAKT);
}

void CScaGallBasDB::getSQLTraktQuestionIndex_Mark(LPTSTR sql)
{
	_stprintf(sql,SQL_TRAKT_QUESTION_INDEX1,TBL_MARK_TRAKT);
}

void CScaGallBasDB::getSQLTraktQuestionIndex_Roj(LPTSTR sql)
{
	_stprintf(sql,SQL_TRAKT_QUESTION_INDEX1,TBL_ROJ_TRAKT);
}

void CScaGallBasDB::GetKeyDBQuestion(CString *str,TRAKT_DATA_PLANT rec)
{
										  
	str->Format(_T("select * from %s where Lopnr = %d "),TBL_PLANT_TRAKT,rec.m_nTrakt_Id);
}

void CScaGallBasDB::GetKeyDBQuestion(CString *str,TRAKT_DATA_MARK rec)
{
										  
	str->Format(_T("select * from %s where Lopnr = %d "),TBL_MARK_TRAKT,rec.m_nTrakt_Id);
}

void CScaGallBasDB::GetKeyDBQuestion(CString *str,TRAKT_DATA_ROJ rec)
{
										  
	str->Format(_T("select * from %s where Lopnr = %d "),TBL_ROJ_TRAKT,rec.m_nTrakt_Id);
}

void CScaGallBasDB::GetKeyPlotDBQuestion(CString *str,PLOT_DATA_PLANT rec)
{
	str->Format(_T("select * from %s where traktId = %d"),TBL_PLANT_PLOT,rec.m_nPlot_Trakt_Id);
}

void CScaGallBasDB::GetKeyPlotDBQuestion(CString *str,PLOT_DATA_MARK rec)
{
	str->Format(_T("select * from %s where traktId = %d"),TBL_MARK_PLOT,rec.m_nPlot_Trakt_Id);
}

void CScaGallBasDB::GetKeyPlotDBQuestion(CString *str,PLOT_DATA_ROJ rec)
{
	str->Format(_T("select * from %s where traktId = %d"),TBL_ROJ_PLOT,rec.m_nPlot_Trakt_Id);
}

