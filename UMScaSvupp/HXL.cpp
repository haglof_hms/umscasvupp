#include "stdafx.h"
#include "UMScaSvuppDB.h"
#include "ResLangFileReader.h"
#include "HXL.h"
#include "MyMessageDlg.h"

int GetNextFileNumber(CString sDir)
{
	// h�mta ut l�pnummer ur filnamn
	int nNum = _tstoi(sDir.Mid(sDir.GetLength() - 3, 2));

	// kolla efter n�sta tillg�ngliga nummer
	bool bFound = false;
	CFile cfFile;
	CString sTmpDir;

	do
	{
		// �ka l�pnummer och f�rs�k �ppna filen
		nNum++;
		
		sTmpDir.Format(_T("%s%02d\\"), sDir.Left(sDir.GetLength()-3), nNum);
		if( CreateDirectory(sTmpDir, NULL) != 0 )
		{
			bFound = true;
		}
	}
	while(bFound == false && nNum < 100);
	if(nNum > 99) return -1;

	if( bFound == true)
		return nNum;
	else
		return -1;
}

int Check_File(CString sName, int *n_Replace_Type)
{
	CFile cfFile;
	bool bRet=false;

//	CString cap = _T("Fil");
	CString yes_btn = _T("Ja");
	CString no_btn = _T("Nej");
	CString yes_to_all = _T("Ja till alla");
	CString no_to_all = _T("Nej till alla");
	CString msg = _T("Filen Finns! Ers�tt filen?");
	CString rename = _T("�ka l�pnr");

	switch(*n_Replace_Type)
	{
	case REPLACE_ALL:
		return true;
	case NOT_REPLACE_ALL:
		return false;
	case DO_DIALOG:
		break;
	default:
		*n_Replace_Type=DO_DIALOG;
		break;
	}
	
	//Kolla om filen finns
	if(cfFile.Open(sName,CFile::modeReadWrite) == 0)//Filen finns inte
	{
		return true;
	}
	else//Filen finns
	{
		cfFile.Close();
		//Tryck upp dialog om filen skall skrivas �ver

		int bReturn = FALSE;
		CMyFileMessageDlg *dlg = new CMyFileMessageDlg(msg, yes_btn, no_btn, yes_to_all, no_to_all, sName, rename);

		bReturn = dlg->DoModal();
		delete dlg;
		switch(bReturn)
		{
		case IDCANCEL:
			*n_Replace_Type=DO_DIALOG;
			return false;
		case IDNO:
			*n_Replace_Type=NOT_REPLACE_ALL;
			return false;
		case IDOK:
			*n_Replace_Type=DO_DIALOG;
			return true;
		case IDYES:
			*n_Replace_Type=REPLACE_ALL;
			return true;
		case IDRETRY:	// rename
			*n_Replace_Type=RENAME;
			return 2;
		}
	}
	return bRet;
}

CString ReplaceIllegal(CString *pcsIn)
{
	CString csOut = *pcsIn;

	csOut.Replace(_T("&"), _T("&amp;"));
	csOut.Replace(_T("<"), _T("&lt;"));
	csOut.Replace(_T(">"), _T("&gt;"));
	csOut.Replace(_T("\""), _T("&quot;"));

	return csOut;
}

int HXL_Make_File_Roj(TRAKT_DATA_ROJ t,CString sName)
{

	CStringA csLine,csTmp,sDate;
	CString sTmp=_T(""),sVersion=_T("");
	unsigned int i=0;
	CFile cfFile;
	sVersion	= getVersionInfo(g_hInstance,VER_NUMBER);
	CTime tid = CTime::GetCurrentTime();
	sDate.Format("%04d%02d%02d",
		tid.GetYear(),
		tid.GetMonth(),
		tid.GetDay());
	if(cfFile.Open(sName, CFile::modeCreate|CFile::modeReadWrite) == 0)
	{
		sTmp.Format(_T("Kan inte skapa r�jningsfil %s"), sName);
		AfxMessageBox(sTmp);
		return 0;
	}
	csLine.Format("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");													cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("<XML>");																								cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("<Document>");																						cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("%s%s%s","<FileDate>",				sDate,								"</FileDate>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_OwnerId.CompareNoCase(_T(""))==0)
		csLine.Format("<OwnerId/>");
	else	
		csLine.Format("%s%S%s","<OwnerId>",				t.m_sTrakt_OwnerId,					"</OwnerId>");				cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("<Type>SCA Rojning egen</Type>");																		cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("%s%S%S%s","<Ver>",					PROGRAM_NAME,sVersion,				"</Ver>");					cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("</Document>");																						cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("<Trakt>");																							cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("<!-- Invtyp. 0 Egen, 1 Kontroll -->");																cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_Invtyp==MY_NULL)
		csLine.Format("<Invtyp/>");
	else
		csLine.Format("%s%d%s","<Invtyp>",					0,					"</Invtyp>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_Slutford==MY_NULL)
		csLine.Format("<Slutford/>");
	else
	csLine.Format("%s%d%s","<Slutford>",				t.m_nTrakt_Slutford,				"</Slutford>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Datum.CompareNoCase(_T(""))==0)
		csLine.Format("<Datum/>");
	else
	csLine.Format("%s%S%s","<Datum>",					t.m_sTrakt_Datum,					"</Datum>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Forvaltning.CompareNoCase(_T(""))==0)
		csLine.Format("<Forvaltning/>");
	else
	csLine.Format("%s%S%s","<Forvaltning>",				t.m_sTrakt_Forvaltning,				"</Forvaltning>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Distrikt.CompareNoCase(_T(""))==0)
		csLine.Format("<Distrikt/>");
	else
	csLine.Format("%s%S%s","<Distrikt>",				t.m_sTrakt_Distrikt,				"</Distrikt>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_Ursprung==MY_NULL)
		csLine.Format("<Ursprung/>");
	else
	csLine.Format("%s%d%s","<Ursprung>",				t.m_nTrakt_Ursprung,				"</Ursprung>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_KartaX==MY_NULL)
		csLine.Format("<KartaX/>");
	else	
	csLine.Format("%s%.10f%s","<KartaX>",					t.m_fTrakt_KartaX,					"</KartaX>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_KartaY==MY_NULL)
		csLine.Format("<KartaY/>");
	else	
	csLine.Format("%s%.10f%s","<KartaY>",					t.m_fTrakt_KartaY,					"</KartaY>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Traktnamn.CompareNoCase(_T(""))==0)
		csLine.Format("<Traktnamn/>");
	else	
	csLine.Format("%s%S%s","<Traktnamn>",				ReplaceIllegal(&t.m_sTrakt_Traktnamn),				"</Traktnamn>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_Areal==MY_NULL)
		csLine.Format("<Areal/>");
	else	
	csLine.Format("%s%f%s","<Areal>",					t.m_fTrakt_Areal,					"</Areal>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_AtgardsId.CompareNoCase(_T(""))==0)
		csLine.Format("<AtgardsID/>");
	else		
		csLine.Format("%s%S%s","<AtgardsID>",					t.m_sTrakt_AtgardsId,					"</AtgardsID>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Inventerare.CompareNoCase(_T(""))==0)
		csLine.Format("<Inventerare/>");
	else		
	csLine.Format("%s%S%s","<Inventerare>",				ReplaceIllegal(&t.m_sTrakt_Inventerare),				"</Inventerare>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Entreprenor.CompareNoCase(_T(""))==0)
		csLine.Format("<Entreprenor/>");
	else		
	csLine.Format("%s%S%s","<Entreprenor>",				ReplaceIllegal(&t.m_sTrakt_Entreprenor),				"</Entreprenor>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Lag.CompareNoCase(_T(""))==0)
		csLine.Format("<Lag/>");
	else	
		csLine.Format("%s%S%s","<Lag>",						t.m_sTrakt_Lag,						"</Lag>");					cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_SI.CompareNoCase(_T(""))==0)
		csLine.Format("<SI/>");
	else	
		csLine.Format("%s%S%s","<SI>",						t.m_sTrakt_SI,						"</SI>");					cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Anteckningar.CompareNoCase(_T(""))==0)
		csLine.Format("<Anteckningar/>");
	else	
		csLine.Format("%s%S%s","<Anteckningar>",			ReplaceIllegal(&t.m_sTrakt_Anteckningar),			"</Anteckningar>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_AntalYtor==MY_NULL)
		csLine.Format("<AntalYtor/>");
	else	
		csLine.Format("%s%d%s","<AntalYtor>",				t.m_nTrakt_AntalYtor,				"</AntalYtor>");			cfFile.Write(csLine,csLine.GetLength());

	csLine.Format("%s","<!-- Start Medelv�rden -->");																	cfFile.Write(csLine,csLine.GetLength());
	
	if(t.m_fTrakt_AntalHsTall==MY_NULL)
		csLine.Format("<AntalHsTall/>");
	else	
		csLine.Format("%s%f%s","<AntalHsTall>",				t.m_fTrakt_AntalHsTall,				"</AntalHsTall>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_MhojdTall==MY_NULL)
		csLine.Format("<MhojdTall/>");
	else	
		csLine.Format("%s%d%s","<MhojdTall>",				t.m_nTrakt_MhojdTall,				"</MhojdTall>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_StHaTall==MY_NULL)
		csLine.Format("<StHaTall/>");
	else	
		csLine.Format("%s%d%s","<StHaTall>",				t.m_nTrakt_StHaTall,				"</StHaTall>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_AndelTall==MY_NULL)
		csLine.Format("<AndelTall/>");
	else	
		csLine.Format("%s%d%s","<AndelTall>",				t.m_nTrakt_AndelTall,				"</AndelTall>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_AntalHsGran==MY_NULL)
		csLine.Format("<AntalHsGran/>");
	else	
		csLine.Format("%s%f%s","<AntalHsGran>",				t.m_fTrakt_AntalHsGran,				"</AntalHsGran>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_MhojdGran==MY_NULL)
		csLine.Format("<MhojdGran/>");
	else	
		csLine.Format("%s%d%s","<MhojdGran>",				t.m_nTrakt_MhojdGran,				"</MhojdGran>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_StHaGran==MY_NULL)
		csLine.Format("<StHaGran/>");
	else	
		csLine.Format("%s%d%s","<StHaGran>",				t.m_nTrakt_StHaGran,				"</StHaGran>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_AndelGran==MY_NULL)
		csLine.Format("<AndelGran/>");
	else	
		csLine.Format("%s%d%s","<AndelGran>",				t.m_nTrakt_AndelGran,				"</AndelGran>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_AntalHsCont==MY_NULL)
		csLine.Format("<AntalHsCont/>");
	else	
		csLine.Format("%s%f%s","<AntalHsCont>",				t.m_fTrakt_AntalHsCont,				"</AntalHsCont>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_MhojdCont==MY_NULL)
		csLine.Format("<MhojdCont/>");
	else	
		csLine.Format("%s%d%s","<MhojdCont>",				t.m_nTrakt_MhojdCont,				"</MhojdCont>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_StHaCont==MY_NULL)
		csLine.Format("<StHaCont/>");
	else	
		csLine.Format("%s%d%s","<StHaCont>",				t.m_nTrakt_StHaCont,				"</StHaCont>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_AndelCont==MY_NULL)
		csLine.Format("<AndelCont/>");
	else	
		csLine.Format("%s%d%s","<AndelCont>",				t.m_nTrakt_AndelCont,				"</AndelCont>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_AntalHsBjork==MY_NULL)
		csLine.Format("<AntalHsBjork/>");
	else	
		csLine.Format("%s%f%s","<AntalHsBjork>",			t.m_fTrakt_AntalHsBjork,			"</AntalHsBjork>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_MhojdBjork==MY_NULL)
		csLine.Format("<MhojdBjork/>");
	else	
		csLine.Format("%s%d%s","<MhojdBjork>",				t.m_nTrakt_MhojdBjork,				"</MhojdBjork>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_StHaBjork==MY_NULL)
		csLine.Format("<StHaBjork/>");
	else	
		csLine.Format("%s%d%s","<StHaBjork>",				t.m_nTrakt_StHaBjork,				"</StHaBjork>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_AndelBjork==MY_NULL)
		csLine.Format("<AndelBjork/>");
	else	
		csLine.Format("%s%d%s","<AndelBjork>",				t.m_nTrakt_AndelBjork,				"</AndelBjork>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_AntalRoj==MY_NULL)
		csLine.Format("<AntalRoj/>");
	else	
		csLine.Format("%s%f%s","<AntalRoj>",				t.m_fTrakt_AntalRoj,				"</AntalRoj>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_AntalOroj==MY_NULL)
		csLine.Format("<AntalOroj/>");
	else	
		csLine.Format("%s%f%s","<AntalOroj>",				t.m_fTrakt_AntalOroj,				"</AntalOroj>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_AntalHansyn==MY_NULL)
		csLine.Format("<AntalHansyn/>");
	else	
		csLine.Format("%s%f%s","<AntalHansyn>",				t.m_fTrakt_AntalHansyn,				"</AntalHansyn>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_HojdRoj==MY_NULL)
		csLine.Format("<HojdRoj/>");
	else	
		csLine.Format("%s%d%s","<HojdRoj>",					t.m_nTrakt_HojdRoj,					"</HojdRoj>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_HojdOroj==MY_NULL)
		csLine.Format("<HojdOroj/>");
	else	
		csLine.Format("%s%d%s","<HojdOroj>",				t.m_nTrakt_HojdOroj,				"</HojdOroj>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_StHaRoj==MY_NULL)
		csLine.Format("<StHaRoj/>");
	else	
		csLine.Format("%s%d%s","<StHaRoj>",					t.m_nTrakt_StHaRoj,					"</StHaRoj>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_StHaOroj==MY_NULL)
		csLine.Format("<StHaOroj/>");
	else	
		csLine.Format("%s%d%s","<StHaOroj>",				t.m_nTrakt_StHaOroj,				"</StHaOroj>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_StHaHansyn==MY_NULL)
		csLine.Format("<StHaHansyn/>");
	else	
		csLine.Format("%s%d%s","<StHaHansyn>",				t.m_nTrakt_StHaHansyn,				"</StHaHansyn>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_RojdaYtorRojbehov==MY_NULL)
		csLine.Format("<RojdaYtorRojbehov/>");
	else	
		csLine.Format("%s%d%s","<RojdaYtorRojbehov>",		t.m_nTrakt_RojdaYtorRojbehov,		"</RojdaYtorRojbehov>");	cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_OrojdaYtorRojbehov==MY_NULL)
		csLine.Format("<OrojdaYtorRojbehov/>");
	else	
		csLine.Format("%s%d%s","<OrojdaYtorRojbehov>",		t.m_nTrakt_OrojdaYtorRojbehov,		"</OrojdaYtorRojbehov>");	cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_RojdaYtorEjRojbehov==MY_NULL)
		csLine.Format("<RojdaYtorEjRojbehov/>");
	else	
		csLine.Format("%s%d%s","<RojdaYtorEjRojbehov>",		t.m_nTrakt_RojdaYtorEjRojbehov,		"</RojdaYtorEjRojbehov>");	cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_OrojdaYtorEjRojbehov==MY_NULL)
		csLine.Format("<OrojdaYtorEjRojbehov/>");
	else	
		csLine.Format("%s%d%s","<OrojdaYtorEjRojbehov>",	t.m_nTrakt_OrojdaYtorEjRojbehov,	"</OrojdaYtorEjRojbehov>");	cfFile.Write(csLine,csLine.GetLength());

	csLine.Format("%s","<!-- Kvalitet till virkesprod -->");															cfFile.Write(csLine,csLine.GetLength());

	if(t.m_fTrakt_Tslval==MY_NULL)
		csLine.Format("<Tslval/>");
	else	
		csLine.Format("%s%f%s","<Tslval>",					t.m_fTrakt_Tslval,					"</Tslval>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_Stamval==MY_NULL)
		csLine.Format("<Stamval/>");
	else	
		csLine.Format("%s%f%s","<Stamval>",					t.m_fTrakt_Stamval,					"</Stamval>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_Stamantal==MY_NULL)
		csLine.Format("<Stamantal/>");
	else	
		csLine.Format("%s%f%s","<Stamantal>",				t.m_fTrakt_Stamantal,				"</Stamantal>");			cfFile.Write(csLine,csLine.GetLength());

	csLine.Format("%s","<!-- Kvalitet naturh�nsyn -->");																cfFile.Write(csLine,csLine.GetLength());

	if(t.m_fTrakt_Hansynsstammar==MY_NULL)
		csLine.Format("<Hansynsstammar/>");
	else	
		csLine.Format("%s%f%s","<Hansynsstammar>",			t.m_fTrakt_Hansynsstammar,			"</Hansynsstammar>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_Myr==MY_NULL)
		csLine.Format("<Myr/>");
	else	
		csLine.Format("%s%f%s","<Myr>",						t.m_fTrakt_Myr,						"</Myr>");					cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_Vattendrag==MY_NULL)
		csLine.Format("<Vattendrag/>");
	else	
		csLine.Format("%s%f%s","<Vattendrag>",				t.m_fTrakt_Vattendrag,				"</Vattendrag>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_Sjo==MY_NULL)
		csLine.Format("<Sjo/>");
	else	
		csLine.Format("%s%f%s","<Sjo>",						t.m_fTrakt_Sjo,						"</Sjo>");					cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_Annat==MY_NULL)
		csLine.Format("<Annat/>");
	else	
		csLine.Format("%s%f%s","<Annat>",					t.m_fTrakt_Annat,					"</Annat>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_Surdrag==MY_NULL)
		csLine.Format("<Surdrag/>");
	else	
		csLine.Format("%s%f%s","<Surdrag>",					t.m_fTrakt_Surdrag,					"</Surdrag>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_OvrBio==MY_NULL)
		csLine.Format("<OvrBio/>");
	else	
		csLine.Format("%s%f%s","<OvrBio>",					t.m_fTrakt_OvrBio,					"</OvrBio>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_Kultur==MY_NULL)
		csLine.Format("<Kultur/>");
	else	
		csLine.Format("%s%f%s","<Kultur>",					t.m_fTrakt_Kultur,					"</Kultur>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_TotKvalNatur==MY_NULL)
		csLine.Format("<TotKvalNatur/>");
	else	
		csLine.Format("%s%f%s","<TotKvalNatur>",			t.m_fTrakt_TotKvalNatur,			"</TotKvalNatur>");			cfFile.Write(csLine,csLine.GetLength());
	
		csLine.Format("%s","<!-- Slut Medelv�rden -->");																	cfFile.Write(csLine,csLine.GetLength());	
	
	
	
	for(i=0;i<t.m_vPlotData.size();i++)
	{
		PLOT_DATA_ROJ p;
		p=t.m_vPlotData[i];
		csLine.Format("%s","<Yta>");																	cfFile.Write(csLine,csLine.GetLength());
		
		if(p.m_nPlot_YtNr==MY_NULL)
			csLine.Format("<YtNr/>");
		else
		csLine.Format("%s%d%s","<YtNr>",			p.m_nPlot_YtNr,				"</YtNr>");				cfFile.Write(csLine,csLine.GetLength());
		if(p.m_fPlot_KoordinatX==MY_NULL)
			csLine.Format("<KoordinatX/>");
		else
		csLine.Format("%s%.10f%s","<KoordinatX>",		p.m_fPlot_KoordinatX,		"</KoordinatX>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_fPlot_KoordinatY==MY_NULL)
			csLine.Format("<KoordinatY/>");
		else		
		csLine.Format("%s%.10f%s","<KoordinatY>",		p.m_fPlot_KoordinatY,		"</KoordinatY>");		cfFile.Write(csLine,csLine.GetLength());

		csLine.Format("%s","<!-- 100m2 yta -->");														cfFile.Write(csLine,csLine.GetLength());
		
		if(p.m_nPlot_Rojd==MY_NULL)
			csLine.Format("<Rojd/>");
		else
		csLine.Format("%s%d%s","<Rojd>",			p.m_nPlot_Rojd,				"</Rojd>");				cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Rojbehov==MY_NULL)
			csLine.Format("<Rojbehov/>");
		else		
		csLine.Format("%s%d%s","<Rojbehov>",		p.m_nPlot_Rojbehov,			"</Rojbehov>");			cfFile.Write(csLine,csLine.GetLength());

		csLine.Format("%s","<!-- Kvalitet till virkesprod. (1-3) 100m2 yta -->");						cfFile.Write(csLine,csLine.GetLength());
		
		if(p.m_nPlot_TrslVal==MY_NULL)
			csLine.Format("<Trslval/>");
		else		
		csLine.Format("%s%d%s","<Trslval>",			p.m_nPlot_TrslVal,			"</Trslval>");			cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Stamval==MY_NULL)
			csLine.Format("<Stamval/>");
		else		
		csLine.Format("%s%d%s","<Stamval>",			p.m_nPlot_Stamval,			"</Stamval>");			cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Stamantal==MY_NULL)
			csLine.Format("<Stamantal/>");
		else		
		csLine.Format("%s%d%s","<Stamantal>",		p.m_nPlot_Stamantal,		"</Stamantal>");		cfFile.Write(csLine,csLine.GetLength());

		csLine.Format("%s","<!-- Huvudstammar per traedslag 25m2 yta -->");								cfFile.Write(csLine,csLine.GetLength());
		
		if(p.m_nPlot_AntalHsTall==MY_NULL)
			csLine.Format("<AntalHsTall/>");
		else		
		csLine.Format("%s%d%s","<AntalHsTall>",		p.m_nPlot_AntalHsTall,		"</AntalHsTall>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_MhojdTall==MY_NULL)
			csLine.Format("<MhojdTall/>");
		else		
		csLine.Format("%s%d%s","<MhojdTall>",		p.m_nPlot_MhojdTall,		"</MhojdTall>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_AntalHsGran==MY_NULL)
			csLine.Format("<AntalHsGran/>");
		else		
		csLine.Format("%s%d%s","<AntalHsGran>",		p.m_nPlot_AntalHsGran,		"</AntalHsGran>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_MhojdGran==MY_NULL)
			csLine.Format("<MhojdGran/>");
		else		
		csLine.Format("%s%d%s","<MhojdGran>",		p.m_nPlot_MhojdGran,		"</MhojdGran>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_AntalHsCont==MY_NULL)
			csLine.Format("<AntalHsCont/>");
		else		
		csLine.Format("%s%d%s","<AntalHsCont>",		p.m_nPlot_AntalHsCont,		"</AntalHsCont>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_MhojdCont==MY_NULL)
			csLine.Format("<MhojdCont/>");
		else		
		csLine.Format("%s%d%s","<MhojdCont>",		p.m_nPlot_MhojdCont,		"</MhojdCont>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_AntalHsBjork==MY_NULL)
			csLine.Format("<AntalHsBjork/>");
		else		
		csLine.Format("%s%d%s","<AntalHsBjork>",	p.m_nPlot_AntalHsBjork,		"</AntalHsBjork>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_MhojdBjork==MY_NULL)
			csLine.Format("<MhojdBjork/>");
		else		
		csLine.Format("%s%d%s","<MhojdBjork>",		p.m_nPlot_MhojdBjork,		"</MhojdBjork>");		cfFile.Write(csLine,csLine.GetLength());

		csLine.Format("%s","<!-- Roejda stammar -->");													cfFile.Write(csLine,csLine.GetLength());
		
		if(p.m_nPlot_AntalRo==MY_NULL)
			csLine.Format("<AntalRo/>");
		else		
		csLine.Format("%s%d%s","<AntalRo>",			p.m_nPlot_AntalRo,			"</AntalRo>");			cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_MhojdRo==MY_NULL)
			csLine.Format("<MhojdRo/>");
		else		
		csLine.Format("%s%d%s","<MhojdRo>",			p.m_nPlot_MhojdRo,			"</MhojdRo>");			cfFile.Write(csLine,csLine.GetLength());

		csLine.Format("%s","<!-- Oroejda roejstammar -->");												cfFile.Write(csLine,csLine.GetLength());
		
		if(p.m_nPlot_AntalOr==MY_NULL)
			csLine.Format("<AntalOr/>");
		else		
		csLine.Format("%s%d%s","<AntalOr>",			p.m_nPlot_AntalOr,			"</AntalOr>");			cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_MhojdOr==MY_NULL)
			csLine.Format("<MhojdOr/>");
		else		
		csLine.Format("%s%d%s","<MhojdOr>",			p.m_nPlot_MhojdOr,			"</MhojdOr>");			cfFile.Write(csLine,csLine.GetLength());

		csLine.Format("%s","<!-- Haensynsstammar -->");													cfFile.Write(csLine,csLine.GetLength());
		
		if(p.m_nPlot_AntalHa==MY_NULL)
			csLine.Format("<AntalHa/>");
		else		
		csLine.Format("%s%d%s","<AntalHa>",			p.m_nPlot_AntalHa,			"</AntalHa>");			cfFile.Write(csLine,csLine.GetLength());

		csLine.Format("%s","<!-- Kvalitet naturhaensyn (1-3) -->");										cfFile.Write(csLine,csLine.GetLength());

		if(p.m_nPlot_HansynsSt==MY_NULL)
			csLine.Format("<HansynsSt/>");
		else		
		csLine.Format("%s%d%s","<HansynsSt>",		p.m_nPlot_HansynsSt,		"</HansynsSt>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Myr==MY_NULL)
			csLine.Format("<Myr/>");
		else		
		csLine.Format("%s%d%s","<Myr>",				p.m_nPlot_Myr,				"</Myr>");				cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Vattendrag==MY_NULL)
			csLine.Format("<Vattendrag/>");
		else		
		csLine.Format("%s%d%s","<Vattendrag>",		p.m_nPlot_Vattendrag,		"</Vattendrag>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Sjo==MY_NULL)
			csLine.Format("<Sjo/>");
		else		
		csLine.Format("%s%d%s","<Sjo>",				p.m_nPlot_Sjo,				"</Sjo>");				cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Annat==MY_NULL)
			csLine.Format("<Annat/>");
		else		
		csLine.Format("%s%d%s","<Annat>",			p.m_nPlot_Annat,			"</Annat>");			cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Surdrag==MY_NULL)
			csLine.Format("<Surdrag/>");
		else		
		csLine.Format("%s%d%s","<Surdrag>",			p.m_nPlot_Surdrag,			"</Surdrag>");			cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_OvrigaBio==MY_NULL)
			csLine.Format("<OvrigaBio/>");
		else		
		csLine.Format("%s%d%s","<OvrigaBio>",		p.m_nPlot_OvrigaBio,		"</OvrigaBio>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Kultur==MY_NULL)
			csLine.Format("<Kultur/>");
		else		
		csLine.Format("%s%d%s","<Kultur>",			p.m_nPlot_Kultur,			"</Kultur>");			cfFile.Write(csLine,csLine.GetLength());

		csLine.Format("%s","<!-- Skador pao tall -->");													cfFile.Write(csLine,csLine.GetLength());

		if(p.m_nPlot_AlgskadorNya==MY_NULL)
			csLine.Format("<AlgskadorNya/>");
		else		
		csLine.Format("%s%d%s","<AlgskadorNya>",	p.m_nPlot_AlgskadorNya,		"</AlgskadorNya>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_AlgskadorGamla==MY_NULL)
			csLine.Format("<AlgskadorGamla/>");
		else		
		csLine.Format("%s%d%s","<AlgskadorGamla>",	p.m_nPlot_AlgskadorGamla,	"</AlgskadorGamla>");	cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Knackesjuka==MY_NULL)
			csLine.Format("<Knackesjuka/>");
		else		
		csLine.Format("%s%d%s","<Knackesjuka>",		p.m_nPlot_Knackesjuka,		"</Knackesjuka>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Gremeniella==MY_NULL)
			csLine.Format("<Gremeniella/>");
		else		
		csLine.Format("%s%d%s","<Gremeniella>",		p.m_nPlot_Gremeniella,		"</Gremeniella>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Ovriga==MY_NULL)
			csLine.Format("<Ovriga/>");
		else		
		csLine.Format("%s%d%s","<Ovriga>",			p.m_nPlot_Ovriga,			"</Ovriga>");			cfFile.Write(csLine,csLine.GetLength());
		if(p.m_sPlot_Anteckningar.CompareNoCase(_T(""))==0)
			csLine.Format("<Anteckningar/>");
		else		
		csLine.Format("%s%S%s","<Anteckningar>",	p.m_sPlot_Anteckningar,		"</Anteckningar>");		cfFile.Write(csLine,csLine.GetLength());
		
		csLine.Format("%s","</Yta>");																	cfFile.Write(csLine,csLine.GetLength());
	}	
	csLine.Format("%s","</Trakt>");																	cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("%s","</XML>	");																	cfFile.Write(csLine,csLine.GetLength());
	cfFile.Close();
	return 1;
}

int HXL_Make_File_Mark(TRAKT_DATA_MARK t,CString sName)
{
	CStringA csLine,csTmp,sDate;
	CString sTmp=_T(""),sVersion=_T("");
	unsigned int i=0;
	CFile cfFile;
	sVersion	= getVersionInfo(g_hInstance,VER_NUMBER);
	CTime tid = CTime::GetCurrentTime();
	sDate.Format("%04d%02d%02d",
		tid.GetYear(),
		tid.GetMonth(),
		tid.GetDay());
	if(cfFile.Open(sName, CFile::modeCreate|CFile::modeReadWrite) == 0)
	{
		sTmp.Format(_T("Kan inte skapa markberedningsfil %s"), sName);
		AfxMessageBox(sTmp);
		return 0;
	}
	csLine.Format("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");								cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("<XML>");																			cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("<Document>");																	cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("%s%s%s","<FileDate>",		sDate,						"</FileDate>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_OwnerId.CompareNoCase(_T(""))==0)
		csLine.Format("<OwnerId/>");
	else	
		csLine.Format("%s%S%s","<OwnerId>",		t.m_sTrakt_OwnerId,			"</OwnerId>");			cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("<Type>SCA Markberedning egen</Type>");											cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("%s%S%S%s","<Ver>",			PROGRAM_NAME,sVersion,		"</Ver>");				cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("</Document>");																	cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("<Trakt>");																		cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("<!-- Invtyp. 0 Egen, 1 Kontroll -->");											cfFile.Write(csLine,csLine.GetLength());
	
	if(t.m_nTrakt_Invtyp==MY_NULL)
	csLine.Format("<Invtyp/>");
	else
	csLine.Format("%s%d%s","<Invtyp>",			0,			"</Invtyp>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_Slutford==MY_NULL)
	csLine.Format("<Slutford/>");
	else
	csLine.Format("%s%d%s","<Slutford>",		t.m_nTrakt_Slutford,		"</Slutford>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Datum.CompareNoCase(_T(""))==0)
	csLine.Format("<Datum/>");
	else	
	csLine.Format("%s%S%s","<Datum>",			t.m_sTrakt_Datum,			"</Datum>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Forvaltning.CompareNoCase(_T(""))==0)
	csLine.Format("<Forvaltning/>");
	else	
	csLine.Format("%s%S%s","<Forvaltning>",		t.m_sTrakt_Forvaltning,		"</Forvaltning>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Distrikt.CompareNoCase(_T(""))==0)
	csLine.Format("<Distrikt/>");
	else	
	csLine.Format("%s%S%s","<Distrikt>",		t.m_sTrakt_Distrikt,		"</Distrikt>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_Ursprung==MY_NULL)
	csLine.Format("<Ursprung/>");
	else
	csLine.Format("%s%d%s","<Ursprung>",		t.m_nTrakt_Ursprung,		"</Ursprung>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_KartaX==MY_NULL)
	csLine.Format("<KartaX/>");
	else	
	csLine.Format("%s%.10f%s","<KartaX>",			t.m_fTrakt_KartaX,			"</KartaX>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_KartaY==MY_NULL)
	csLine.Format("<KartaY/>");
	else	
	csLine.Format("%s%.10f%s","<KartaY>",			t.m_fTrakt_KartaY,			"</KartaY>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Traktnamn.CompareNoCase(_T(""))==0)
	csLine.Format("<Traktnamn/>");
	else	
	csLine.Format("%s%S%s","<Traktnamn>",		ReplaceIllegal(&t.m_sTrakt_Traktnamn),		"</Traktnamn>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_Areal==MY_NULL)
	csLine.Format("<Areal/>");
	else	
	csLine.Format("%s%f%s","<Areal>",			t.m_fTrakt_Areal,			"</Areal>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_AtgardsId.CompareNoCase(_T(""))==0)
		csLine.Format("<AtgardsID/>");
	else	
		csLine.Format("%s%S%s","<AtgardsID>",			t.m_sTrakt_AtgardsId,			"</AtgardsID>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Inventerare.CompareNoCase(_T(""))==0)
	csLine.Format("<Inventerare/>");
	else	
	csLine.Format("%s%S%s","<Inventerare>",	ReplaceIllegal(&t.m_sTrakt_Inventerare),			"</Inventerare>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Entreprenor.CompareNoCase(_T(""))==0)
	csLine.Format("<Entreprenor/>");
	else	
	csLine.Format("%s%S%s","<Entreprenor>",	ReplaceIllegal(&t.m_sTrakt_Entreprenor),			"</Entreprenor>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Lag.CompareNoCase(_T(""))==0)
	csLine.Format("<Lag/>");
	else	
	csLine.Format("%s%S%s","<Lag>",				t.m_sTrakt_Lag,				"</Lag>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_Metod==MY_NULL)
	csLine.Format("<Metod/>");
	else	
	csLine.Format("%s%d%s","<Metod>",			t.m_nTrakt_Metod,			"</Metod>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Aggr.CompareNoCase(_T(""))==0)
	csLine.Format("<Aggr/>");
	else	
	csLine.Format("%s%S%s","<Aggr>",			t.m_sTrakt_Aggr,			"</Aggr>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_MalMbStHa==MY_NULL)
	csLine.Format("<MalMbStHa/>");
	else	
	csLine.Format("%s%d%s","<MalMbStHa>",		t.m_nTrakt_MalMbStHa,		"</MalMbStHa>");		cfFile.Write(csLine,csLine.GetLength());

	if(t.m_nTrakt_HogtSnytbaggetryck == MY_NULL)
		csLine.Format("<HogtSnytbaggetryck/>");
	else
		csLine.Format("<HogtSnytbaggetryck>%d</HogtSnytbaggetryck>", t.m_nTrakt_HogtSnytbaggetryck);
	cfFile.Write(csLine,csLine.GetLength());

	if(t.m_sTrakt_Anteckningar.CompareNoCase(_T(""))==0)
	csLine.Format("<Anteckningar/>");
	else	
	csLine.Format("%s%S%s","<Anteckningar>",	ReplaceIllegal(&t.m_sTrakt_Anteckningar),	"</Anteckningar>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_Sum5==MY_NULL)
	csLine.Format("<Sum5/>");
	else	
	csLine.Format("%s%d%s","<Sum5>",			t.m_nTrakt_Sum5,			"</Sum5>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_Sum4==MY_NULL)
	csLine.Format("<Sum4/>");
	else	
	csLine.Format("%s%d%s","<Sum4>",			t.m_nTrakt_Sum4,			"</Sum4>");				cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_SumTorv3==MY_NULL)
	csLine.Format("<SumTorv3/>");
	else	
	csLine.Format("%s%d%s","<SumTorv3>",		t.m_nTrakt_SumTorv3,		"</SumTorv3>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_SumMineral3==MY_NULL)
	csLine.Format("<SumMineral3/>");
	else
	csLine.Format("%s%d%s","<SumMineral3>",		t.m_nTrakt_SumMineral3,		"</SumMineral3>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_SumHumus3==MY_NULL)
	csLine.Format("<SumHumus3/>");
	else	
	csLine.Format("%s%d%s","<SumHumus3>",		t.m_nTrakt_SumHumus3,		"</SumHumus3>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_Sum7x7==MY_NULL)
	csLine.Format("<Sum7x7/>");
	else	
	csLine.Format("%s%d%s","<Sum7x7>",			t.m_nTrakt_Sum7x7,			"</Sum7x7>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_SumSkadadeLagor==MY_NULL)
	csLine.Format("<SumSkadadeLagor/>");
	else	
	csLine.Format("%s%d%s","<SumSkadadeLagor>",	t.m_nTrakt_SumSkadadeLagor,	"</SumSkadadeLagor>");	cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_SumPoang5==MY_NULL)
	csLine.Format("<SumPoang5/>");
	else	
	csLine.Format("%s%d%s","<SumPoang5>",		t.m_nTrakt_SumPoang5,		"</SumPoang5>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_SumPoang4==MY_NULL)
	csLine.Format("<SumPoang4/>");
	else	
	csLine.Format("%s%d%s","<SumPoang4>",		t.m_nTrakt_SumPoang4,		"</SumPoang4>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_SumPoangTorv3==MY_NULL)
	csLine.Format("<SumPoangTorv3/>");
	else	
	csLine.Format("%s%d%s","<SumPoangTorv3>",	t.m_nTrakt_SumPoangTorv3,	"</SumPoangTorv3>");	cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_SumPoangMineral3==MY_NULL)
	csLine.Format("<SumPoangMineral3/>");
	else	
	csLine.Format("%s%d%s","<SumPoangMineral3>",t.m_nTrakt_SumPoangMineral3,"</SumPoangMineral3>");	cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_SumPoangHumus3==MY_NULL)
	csLine.Format("<SumPoangHumus3/>");
	else	
	csLine.Format("%s%d%s","<SumPoangHumus3>",	t.m_nTrakt_SumPoangHumus3,	"</SumPoangHumus3>");	cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_GKPerYta==MY_NULL)
	csLine.Format("<GKPerYta/>");
	else	
	csLine.Format("%s%d%s","<GKPerYta>",		t.m_nTrakt_GKPerYta,		"</GKPerYta>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_GKPerHa==MY_NULL)
	csLine.Format("<GKPerHa/>");
	else	
	csLine.Format("%s%d%s","<GKPerHa>",			t.m_nTrakt_GKPerHa,			"</GKPerHa>");			cfFile.Write(csLine,csLine.GetLength());

	if(t.m_nTrakt_SumSkadadeFornminnen == MY_NULL)
		csLine.Format("<SumSkadadeFornminnen/>");
	else		
		csLine.Format("<SumSkadadeFornminnen>%d</SumSkadadeFornminnen>", t.m_nTrakt_SumSkadadeFornminnen); cfFile.Write(csLine, csLine.GetLength());

	if(t.m_nTrakt_SumSkadadeKulturminnen == MY_NULL)
		csLine.Format("<SumSkadadeKulturminnen/>");
	else		
		csLine.Format("<SumSkadadeKulturminnen>%d</SumSkadadeKulturminnen>", t.m_nTrakt_SumSkadadeKulturminnen); cfFile.Write(csLine, csLine.GetLength());

	if(t.m_nTrakt_SumKorskador == MY_NULL)
		csLine.Format("<SumKorskador/>");
	else		
		csLine.Format("<SumKorskador>%d</SumKorskador>", t.m_nTrakt_SumKorskador); cfFile.Write(csLine, csLine.GetLength());

	for(i=0;i<t.m_vPlotData.size();i++)
	{
		PLOT_DATA_MARK p;
		p=t.m_vPlotData[i];
		csLine.Format("%s","<Yta>");																			cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_YtNr==MY_NULL)
		csLine.Format("<YtNr/>");
		else
		csLine.Format("%s%d%s","<YtNr>",			p.m_nPlot_YtNr,					"</YtNr>");					cfFile.Write(csLine,csLine.GetLength());
		if(p.m_fPlot_KoordinatX==MY_NULL)
		csLine.Format("<KoordinatX/>");
		else
		csLine.Format("%s%.10f%s","<KoordinatX>",		p.m_fPlot_KoordinatX,			"</KoordinatX>");			cfFile.Write(csLine,csLine.GetLength());
		if(p.m_fPlot_KoordinatY==MY_NULL)
		csLine.Format("<KoordinatY/>");
		else		
		csLine.Format("%s%.10f%s","<KoordinatY>",		p.m_fPlot_KoordinatY,			"</KoordinatY>");			cfFile.Write(csLine,csLine.GetLength());
		if(p.m_sPlot_Anteckningar.CompareNoCase(_T(""))==0)
		csLine.Format("<Anteckningar/>");
		else		
		csLine.Format("%s%S%s","<Anteckningar>",	p.m_sPlot_Anteckningar,			"</Anteckningar>");			cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Rader==MY_NULL)
		csLine.Format("<Rader100m/>");
		else		
		csLine.Format("%s%d%s","<Rader100m>",		p.m_nPlot_Rader,				"</Rader100m>");			cfFile.Write(csLine,csLine.GetLength());
		if(p.m_sPlot_Standort.CompareNoCase(_T(""))==0)
		csLine.Format("<Standort/>");
		else		
		csLine.Format("%s%S%s","<Standort>",		p.m_sPlot_Standort,				"</Standort>");				cfFile.Write(csLine,csLine.GetLength());
		
		csLine.Format("<!-- Kvalitetspoeng -->");																cfFile.Write(csLine,csLine.GetLength());
		
		if(p.m_nPlot_poeng5==MY_NULL)
		csLine.Format("<Poeng5/>");
		else
		csLine.Format("%s%d%s","<Poeng5>",			p.m_nPlot_poeng5,				"</Poeng5>");				cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_poeng4==MY_NULL)
		csLine.Format("<Poeng4/>");
		else		
		csLine.Format("%s%d%s","<Poeng4>",			p.m_nPlot_poeng4,				"</Poeng4>");				cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_PoengTorv3==MY_NULL)
		csLine.Format("<PoengTorv3/>");
		else		
		csLine.Format("%s%d%s","<PoengTorv3>",		p.m_nPlot_PoengTorv3,			"</PoengTorv3>");			cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_PoengMineral3==MY_NULL)
		csLine.Format("<PoengMineral3/>");
		else		
		csLine.Format("%s%d%s","<PoengMineral3>",	p.m_nPlot_PoengMineral3,		"</PoengMineral3>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_PoengHumus3==MY_NULL)
		csLine.Format("<PoengHumus3/>");
		else		
		csLine.Format("%s%d%s","<PoengHumus3>",		p.m_nPlot_PoengHumus3,			"</PoengHumus3>");			cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_poeng7x7==MY_NULL)
		csLine.Format("<Poeng7x7/>");
		else		
		csLine.Format("%s%d%s","<Poeng7x7>",		p.m_nPlot_poeng7x7,				"</Poeng7x7>");				cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_PoengSkadadeLagor==MY_NULL)
		csLine.Format("<PoengSkadadeLagor/>");
		else		
		csLine.Format("%s%d%s","<PoengSkadadeLagor>",p.m_nPlot_PoengSkadadeLagor,	"</PoengSkadadeLagor>");	cfFile.Write(csLine,csLine.GetLength());

		if(p.m_nPlot_SkadadeFornminnen == MY_NULL)
			csLine.Format("<SkadadeFornminnen/>");
		else		
			csLine.Format("<SkadadeFornminnen>%d</SkadadeFornminnen>", p.m_nPlot_SkadadeFornminnen); cfFile.Write(csLine, csLine.GetLength());

		if(p.m_nPlot_SkadadeKulturminnen == MY_NULL)
			csLine.Format("<SkadadeKulturminnen/>");
		else		
			csLine.Format("<SkadadeKulturminnen>%d</SkadadeKulturminnen>", p.m_nPlot_SkadadeKulturminnen); cfFile.Write(csLine, csLine.GetLength());

		if(p.m_nPlot_Korskador == MY_NULL)
			csLine.Format("<Korskador/>");
		else		
			csLine.Format("<Korskador>%d</Korskador>", p.m_nPlot_Korskador); cfFile.Write(csLine, csLine.GetLength());

		csLine.Format("</Yta>");																				cfFile.Write(csLine,csLine.GetLength());	
	}
	csLine.Format("</Trakt>");																	cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("</XML>");																	cfFile.Write(csLine,csLine.GetLength());
	cfFile.Close();
	return 1;
}



int HXL_Make_File_Plant(TRAKT_DATA_PLANT t,CString sName)
{
	CStringA csLine;
	CString sTmp=_T(""),sVersion=_T(""),sDate=_T("");
	unsigned int i=0;
	CFile cfFile;
	sVersion	= getVersionInfo(g_hInstance,VER_NUMBER);
	CTime tid = CTime::GetCurrentTime();
	sDate.Format(_T("%04d%02d%02d"),
		tid.GetYear(),
		tid.GetMonth(),
		tid.GetDay());


	if(cfFile.Open(sName, CFile::modeCreate|CFile::modeReadWrite) == 0)
	{
		sTmp.Format(_T("Kan inte skapa planteringsfil %s"), sName);
		AfxMessageBox(sTmp);
		return 0;
	}
	csLine.Format("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");							cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("<XML>");																		cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("<Document>");																cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("%s%S%s","<FileDate>",		sDate,						"</FileDate>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_OwnerId.CompareNoCase(_T(""))==0)
		csLine.Format("<OwnerId/>");
	else	
		csLine.Format("%s%S%s","<OwnerId>",		t.m_sTrakt_OwnerId,			"</OwnerId>");		cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("<Type>SCA Plantupp egen</Type>");											cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("%s%S%S%s","<Ver>",			PROGRAM_NAME,sVersion,		"</Ver>");			cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("</Document>");																cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("<Trakt>");																	cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("<!-- Invtyp. 0 Egen, 1 Kontroll -->");										cfFile.Write(csLine,csLine.GetLength());
	
	if(t.m_nTrakt_InvTyp==MY_NULL)
	csLine.Format("<Invtyp/>");
	else
	csLine.Format("%s%d%s","<Invtyp>",			t.m_nTrakt_InvTyp,			"</Invtyp>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_Slutford==MY_NULL)
	csLine.Format("<Slutford/>");
	else
	csLine.Format("%s%d%s","<Slutford>",		t.m_nTrakt_Slutford,		"</Slutford>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Datum.CompareNoCase(_T(""))==0)
	csLine.Format("<Datum/>");
	else	
	csLine.Format("%s%S%s","<Datum>",			t.m_sTrakt_Datum,			"</Datum>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Forvaltning.CompareNoCase(_T(""))==0)
	csLine.Format("<Forvaltning/>");
	else	
	csLine.Format("%s%S%s","<Forvaltning>",	t.m_sTrakt_Forvaltning,			"</Forvaltning>");	cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Distrikt.CompareNoCase(_T(""))==0)
	csLine.Format("<Distrikt/>");
	else	
	csLine.Format("%s%S%s","<Distrikt>",		t.m_sTrakt_Distrikt,		"</Distrikt>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_Ursprung==MY_NULL)
	csLine.Format("<Ursprung/>");
	else	
	csLine.Format("%s%d%s","<Ursprung>",		t.m_nTrakt_Ursprung,		"</Ursprung>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_KartaX==MY_NULL)
	csLine.Format("<KartaX/>");
	else	
	csLine.Format("%s%.10f%s","<KartaX>",			t.m_fTrakt_KartaX,			"</KartaX>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_KartaY==MY_NULL)
	csLine.Format("<KartaY/>");
	else	
	csLine.Format("%s%.10f%s","<KartaY>",			t.m_fTrakt_KartaY,			"</KartaY>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Traktnamn.CompareNoCase(_T(""))==0)
	csLine.Format("<Traktnamn/>");
	else	
	csLine.Format("%s%S%s","<Traktnamn>",		ReplaceIllegal(&t.m_sTrakt_Traktnamn),		"</Traktnamn>");	cfFile.Write(csLine,csLine.GetLength());
	if(t.m_fTrakt_Areal==MY_NULL)
	csLine.Format("<Areal/>");
	else	
	csLine.Format("%s%f%s","<Areal>",			t.m_fTrakt_Areal,			"</Areal>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_AtgardsId.CompareNoCase(_T(""))==0)
		csLine.Format("<AtgardsID/>");
	else	
		csLine.Format("%s%S%s","<AtgardsID>",			t.m_sTrakt_AtgardsId,			"</AtgardsID>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Inventerare.CompareNoCase(_T(""))==0)
	csLine.Format("<Inventerare/>");
	else	
	csLine.Format("%s%S%s","<Inventerare>",	ReplaceIllegal(&t.m_sTrakt_Inventerare),			"</Inventerare>");	cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Entreprenor.CompareNoCase(_T(""))==0)
	csLine.Format("<Entreprenor/>");
	else	
	csLine.Format("%s%S%s","<Entreprenor>",	ReplaceIllegal(&t.m_sTrakt_Entreprenor),			"</Entreprenor>");	cfFile.Write(csLine,csLine.GetLength());
	if(t.m_sTrakt_Lag.CompareNoCase(_T(""))==0)
	csLine.Format("<Lag/>");
	else	
	csLine.Format("%s%S%s","<Lag>",				t.m_sTrakt_Lag,				"</Lag>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_MB_typ==MY_NULL)
	csLine.Format("<MB_typ/>");
	else	
	csLine.Format("%s%d%s","<MB_typ>",			t.m_nTrakt_MB_typ,			"</MB_typ>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_Mal_PlHa==MY_NULL)
	csLine.Format("<Mal_PlHa/>");
	else	
	csLine.Format("%s%d%s","<Mal_PlHa>",		t.m_nTrakt_Mal_PlHa,		"</Mal_PlHa>");		cfFile.Write(csLine,csLine.GetLength());

	if(t.m_nTrakt_HogtSnytbaggetryck == MY_NULL)
		csLine.Format("<HogtSnytbaggetryck/>");
	else
		csLine.Format("<HogtSnytbaggetryck>%d</HogtSnytbaggetryck>", t.m_nTrakt_HogtSnytbaggetryck);
	cfFile.Write(csLine,csLine.GetLength());

	if(t.m_sTrakt_Anteckningar.CompareNoCase(_T(""))==0)
	csLine.Format("<Anteckningar/>");
	else	
	csLine.Format("%s%S%s","<Anteckningar>",	ReplaceIllegal(&t.m_sTrakt_Anteckningar),	"</Anteckningar>");	cfFile.Write(csLine,csLine.GetLength());
    
	csLine.Format("%s","<!-- Kvalitetsuppfoljning -->");										cfFile.Write(csLine,csLine.GetLength());

	if(t.m_nTrakt_GP1==MY_NULL)
	csLine.Format("<GP1/>");
	else
	csLine.Format("%s%d%s","<GP1>",				t.m_nTrakt_GP1,				"</GP1>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_GP2==MY_NULL)
	csLine.Format("<GP2/>");
	else	
	csLine.Format("%s%d%s","<GP2>",				t.m_nTrakt_GP2,				"</GP2>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_GP3==MY_NULL)
	csLine.Format("<GP3/>");
	else	
	csLine.Format("%s%d%s","<GP3>",				t.m_nTrakt_GP3,				"</GP3>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_GP4==MY_NULL)
	csLine.Format("<GP4/>");
	else	
	csLine.Format("%s%d%s","<GP4>",				t.m_nTrakt_GP4,				"</GP4>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_GP5==MY_NULL)
	csLine.Format("<GP5/>");
	else	
	csLine.Format("%s%d%s","<GP5>",				t.m_nTrakt_GP5,				"</GP5>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_GP6==MY_NULL)
	csLine.Format("<GP6/>");
	else	
	csLine.Format("%s%d%s","<GP6>",				t.m_nTrakt_GP6,				"</GP6>");			cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_EjGP1==MY_NULL)
	csLine.Format("<EjGP1/>");
	else	
	csLine.Format("%s%d%s","<EjGP1>",			t.m_nTrakt_EjGP1,			"</EjGP1>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_EjGP2==MY_NULL)
	csLine.Format("<EjGP2/>");
	else	
	csLine.Format("%s%d%s","<EjGP2>",			t.m_nTrakt_EjGP2,			"</EjGP2>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_EjGP3==MY_NULL)
	csLine.Format("<EjGP3/>");
	else	
	csLine.Format("%s%d%s","<EjGP3>",			t.m_nTrakt_EjGP3,			"</EjGP3>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_EjGP4==MY_NULL)
	csLine.Format("<EjGP4/>");
	else	
	csLine.Format("%s%d%s","<EjGP4>",			t.m_nTrakt_EjGP4,			"</EjGP4>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_EjGP5==MY_NULL)
	csLine.Format("<EjGP5/>");
	else	
	csLine.Format("%s%d%s","<EjGP5>",			t.m_nTrakt_EjGP5,			"</EjGP5>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_EjGP6==MY_NULL)
	csLine.Format("<EjGP6/>");
	else	
	csLine.Format("%s%d%s","<EjGP6>",			t.m_nTrakt_EjGP6,			"</EjGP6>");		cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_BattrePlantp==MY_NULL)
	csLine.Format("<BattrePlantp/>");
	else	
	csLine.Format("%s%d%s","<BattrePlantp>",	t.m_nTrakt_BattrePlantp,	"</BattrePlantp>");	cfFile.Write(csLine,csLine.GetLength());
	if(t.m_nTrakt_OutnPlantp==MY_NULL)
	csLine.Format("<OutnPlantp/>");
	else	
	csLine.Format("%s%d%s","<OutnPlantp>",		t.m_nTrakt_OutnPlantp,		"</OutnPlantp>");	cfFile.Write(csLine,csLine.GetLength());	

	for(i=0;i<t.m_vPlotData.size();i++)
	{
		PLOT_DATA_PLANT p;
		p=t.m_vPlotData[i];
		csLine.Format("%s","<Yta>");															cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_YtNr==MY_NULL)
		csLine.Format("<YtNr/>");
		else
		csLine.Format("%s%d%s","<YtNr>",			p.m_nPlot_YtNr,			"</YtNr>");			cfFile.Write(csLine,csLine.GetLength());

		if(p.m_nPlot_Standort == MY_NULL)
			csLine.Format("<Standort/>");
		else		
			csLine.Format("<Standort>%d</Standort>", p.m_nPlot_Standort); cfFile.Write(csLine, csLine.GetLength());


		if(p.m_fPlot_KoordinatX==MY_NULL)
		csLine.Format("<KoordinatX/>");
		else
		csLine.Format("%s%.10f%s","<KoordinatX>",		p.m_fPlot_KoordinatX,	"</KoordinatX>");	cfFile.Write(csLine,csLine.GetLength());
		if(p.m_fPlot_KoordinatY==MY_NULL)
		csLine.Format("<KoordinatY/>");
		else		
		csLine.Format("%s%.10f%s","<KoordinatY>",		p.m_fPlot_KoordinatY,	"</KoordinatY>");	cfFile.Write(csLine,csLine.GetLength());
		
		csLine.Format("%s","<!-- Godkandt planterade -->");										cfFile.Write(csLine,csLine.GetLength());
		
		if(p.m_nPlot_Med_mineral==MY_NULL)
		csLine.Format("<Med_mineral/>");
		else
		csLine.Format("%s%d%s","<Med_mineral>",		p.m_nPlot_Med_mineral,	"</Med_mineral>");	cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Utan_mineral==MY_NULL)
		csLine.Format("<Utan_mineral/>");
		else		
		csLine.Format("%s%d%s","<Utan_mineral>",	p.m_nPlot_Utan_mineral,	"</Utan_mineral>");	cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Hogt_torv==MY_NULL)
		csLine.Format("<Hogt_torv/>");
		else		
		csLine.Format("%s%d%s","<Hogt_torv>",		p.m_nPlot_Hogt_torv,	"</Hogt_torv>");	cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Hogt_humus==MY_NULL)
		csLine.Format("<Hogt_humus/>");
		else		
		csLine.Format("%s%d%s","<Hogt_humus>",		p.m_nPlot_Hogt_humus,	"</Hogt_humus>");	cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Hogt_mineral==MY_NULL)
		csLine.Format("<Hogt_mineral/>");
		else		
		csLine.Format("%s%d%s","<Hogt_mineral>",	p.m_nPlot_Hogt_mineral,	"</Hogt_mineral>");	cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Behandlad==MY_NULL)
		csLine.Format("<Behandlad/>");
		else		
		csLine.Format("%s%d%s","<Behandlad>",		p.m_nPlot_Behandlad,	"</Behandlad>");	cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Summa_GK==MY_NULL)
		csLine.Format("<Summa_GK/>");
		else		
		csLine.Format("%s%d%s","<Summa_GK>",		p.m_nPlot_Summa_GK,		"</Summa_GK>");		cfFile.Write(csLine,csLine.GetLength());
		
		csLine.Format("%s","<!-- EJ godkant planterade -->");									cfFile.Write(csLine,csLine.GetLength());
		
		if(p.m_nPlot_Ej_djup==MY_NULL)
		csLine.Format("<Ej_djup/>");
		else
		csLine.Format("%s%d%s","<Ej_djup>",			p.m_nPlot_Ej_djup,		"</Ej_djup>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Ej_punkt==MY_NULL)
		csLine.Format("<Ej_punkt/>");
		else		
		csLine.Format("%s%d%s","<Ej_punkt>",		p.m_nPlot_Ej_punkt,		"</Ej_punkt>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Ej_10cm==MY_NULL)
		csLine.Format("<Ej_10cm/>");
		else		
		csLine.Format("%s%d%s","<Ej_10cm>",			p.m_nPlot_Ej_10cm,		"</Ej_10cm>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Ej_avstand==MY_NULL)
		csLine.Format("<Ej_avstand/>");
		else		
		csLine.Format("%s%d%s","<Ej_avstand>",		p.m_nPlot_Ej_avstand,	"</Ej_avstand>");	cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Hogt==MY_NULL)
		csLine.Format("<Hogt/>");
		else		
		csLine.Format("%s%d%s","<Hogt>",			p.m_nPlot_Hogt,			"</Hogt>");			cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Ovriga==MY_NULL)
		csLine.Format("<Ovriga/>");
		else		
		csLine.Format("%s%d%s","<Ovriga>",			p.m_nPlot_Ovriga,		"</Ovriga>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Summg_EGK==MY_NULL)
		csLine.Format("<Summg_EGK/>");
		else		
		csLine.Format("%s%d%s","<Summg_EGK>",		p.m_nPlot_Summg_EGK,	"</Summg_EGK>");	cfFile.Write(csLine,csLine.GetLength());
		
		csLine.Format("%s","<!-- Ovrigt -->");													cfFile.Write(csLine,csLine.GetLength());
		
		if(p.m_nPlot_Battre==MY_NULL)
		csLine.Format("<Battre/>");
		else
		csLine.Format("%s%d%s","<Battre>",			p.m_nPlot_Battre,		"</Battre>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_nPlot_Outnyttj==MY_NULL)
		csLine.Format("<Outnyttj/>");
		else		
		csLine.Format("%s%d%s","<Outnyttj>",		p.m_nPlot_Outnyttj,		"</Outnyttj>");		cfFile.Write(csLine,csLine.GetLength());
		if(p.m_sPlot_Anteckningar.CompareNoCase(_T(""))==0)
		csLine.Format("<Anteckningar/>");
		else		
		csLine.Format("%s%S%s","<Anteckningar>",	p.m_sPlot_Anteckningar,	"</Anteckningar>");	cfFile.Write(csLine,csLine.GetLength());
		
		csLine.Format("%s","</Yta>");															cfFile.Write(csLine,csLine.GetLength());		
	}		
	csLine.Format("%s","</Trakt>");															cfFile.Write(csLine,csLine.GetLength());
	csLine.Format("%s","</XML>");															cfFile.Write(csLine,csLine.GetLength());
	cfFile.Close();
	return 1;
}