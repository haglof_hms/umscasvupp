// SetPlantDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UMScaSvupp.h"
#include "SetPlantDlg.h"
#include "ResLangFileReader.h"
#include "XBrowseForFolder.h"


// CSetPlantDlg dialog

IMPLEMENT_DYNAMIC(CSetPlantDlg, CDialog)

CSetPlantDlg::CSetPlantDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSetPlantDlg::IDD, pParent)
{

}

CSetPlantDlg::~CSetPlantDlg()
{



}

void CSetPlantDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BOOL CSetPlantDlg::OnInitDialog()
{
	RECT rect;
	CDialog::OnInitDialog();

	//Ladda s�kv�gar
	m_sFilepathPlant=regGetStr(REG_ROOT,
		PROGRAM_NAME,
		REG_SCASVUPP_XML_PLANT_FILEPATH_KEY_NAME,
		_T(""));

	m_sFilepathMark=regGetStr(REG_ROOT,
		PROGRAM_NAME,
		REG_SCASVUPP_XML_MARK_FILEPATH_KEY_NAME,
		_T(""));

	m_sFilepathRoj=regGetStr(REG_ROOT,
		PROGRAM_NAME,
		REG_SCASVUPP_XML_ROJ_FILEPATH_KEY_NAME,
		_T(""));
													
	m_sLangAbbrev = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sLangAbbrev,LANGUAGE_FN_EXT);
	setLanguage();
	GetClientRect(&rect);
	SetWindowText(m_sCaption);

	
	if ( m_wndPropertyGrid.Create( rect, this, IDC_PROPERTY_GRID ) )
	{
		m_wndPropertyGrid.SetTheme(xtpGridThemeOffice2003);
		m_wndPropertyGrid.SetVariableItemsHeight(TRUE);
		if (fileExists(m_sLangFN))
		{	
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				// create global settings category.
				CXTPPropertyGridItem* pPaths = m_wndPropertyGrid.AddCategory(xml->str(IDS_STRING8583));
				// add child items to category.
				m_pItemPathPlant = pPaths->AddChildItem(new CCustomItemFileBox(xml->str(IDS_STRING8584), m_sFilepathPlant));
				m_pItemPathPlant->SetDescription(xml->str(IDS_STRING8587));
				m_pItemPathMark = pPaths->AddChildItem(new CCustomItemFileBox(xml->str(IDS_STRING8585), m_sFilepathMark));
				m_pItemPathMark->SetDescription(xml->str(IDS_STRING8587));
				m_pItemPathRoj = pPaths->AddChildItem(new CCustomItemFileBox(xml->str(IDS_STRING8586), m_sFilepathRoj));
				m_pItemPathRoj->SetDescription(xml->str(IDS_STRING8587));
				pPaths->Expand();
			}
			delete xml;
		}
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


///////////////////////////////////////////////////////////////////////////////
CCustomItemFileBox::CCustomItemFileBox(CString strCaption, CString strValue)
	: CXTPPropertyGridItem(strCaption, strValue)
{
	m_nFlags = xtpGridItemHasExpandButton|xtpGridItemHasEdit;
}

//void CCustomItemFileBox::OnInplaceButtonDown()
void CCustomItemFileBox::OnInplaceButtonDown(CXTPPropertyGridInplaceButton* /*pButton*/)
{
	TCHAR szFolder[MAX_PATH*2];
	szFolder[0] = _T('\0');
	BOOL bRet = XBrowseForFolder(GetParentItem()->GetGrid()->m_hWnd,
								 GetValue(),
								 szFolder,
								 sizeof(szFolder)/sizeof(TCHAR)-2);

	if(bRet == TRUE)
	{
		OnValueChanged( szFolder );
		m_pGrid->Invalidate( FALSE );
	}
};




void CSetPlantDlg::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_sCaption=xml->str(IDS_STRING8582);
		}
		delete xml;
	}
}

BEGIN_MESSAGE_MAP(CSetPlantDlg, CDialog)
END_MESSAGE_MAP()


// CSetPlantDlg message handlers
