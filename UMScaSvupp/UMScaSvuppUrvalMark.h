#pragma once
#include "stdafx.h"
#include "UMScaSvuppDB.h"
#include "Resource.h"
#include "UMHmsMisc.h"
#include "Urval.h"
extern int n_Global_Replace_All_files;
class CUMScaSvuppUrvalMark : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CUMScaSvuppUrvalMark)
	CString m_sLangAbbrev;
	CString m_sLangFN;
	CString m_sSqlQues;

protected:
	CUMScaSvuppUrvalMark();   // standard constructor
	virtual ~CUMScaSvuppUrvalMark();

	int n_Replace_All_Files;
	CMyReportCtrl2 m_wndTraktReport;
	//CMyReportCtrl2 m_wnd_Plant_UrvalReport;
	CImageList pImageList;

	CString m_wnd_caption;
	int m_nInit_Done;
	int m_nNumOfUrval;
	int m_nNumOfRepColumns;
	
	CStringArray m_sDBVarNames;
		
	int m_nIdc[MAX_NUM_OF_URVAL];
	int Xml_StringId_Urval[MAX_NUM_OF_URVAL];
	int Xml_StringId_Rep_Columns[NUM_OF_REP_COL_HEADER_MARK];

	int m_nWinPos[MAX_NUM_OF_URVAL][4];

	BOOL setupReport(void);
	void populateReport(void);
	//void getTraktsFromDB(void);
	int SetSqlQues(void);

	void populateReport_Urval(int var);
	
	BOOL setupReport_Urval(int var);

	int SetSqlQues(int var);
	void SetOrder(int var);
	void setsizes(int var);
	void UpdateUrvalFrom(int var);
	
	int m_nOrder_Urval[MAX_NUM_OF_URVAL];
	
	int m_nColHeaderWidth[MAX_NUM_OF_URVAL];
	int IdcToVar(int nIdc);

	//F�rvaltning text
	CMyExtStatic m_wndLbl_1;
	//Distrikt text
	CMyExtStatic m_wndLbl_2;
	//Entrepren�r text
	CMyExtStatic m_wndLbl_3;
	//Lag text
	CMyExtStatic m_wndLbl_4;
	//Inventerare text
	CMyExtStatic m_wndLbl_5;
	//Mbtyp text
	CMyExtStatic m_wndLbl_6;
	//ObjektsId text
	CMyExtStatic m_wndLbl_7;
	//Ursprung text
	CMyExtStatic m_wndLbl_8;

	//Ursprung text
	CMyExtStatic m_wndLbl_9;

	//Datum
	CMyExtStatic m_wndLbl_Datum;
	CMyExtStatic m_wndLbl_Datum_From;
	CMyExtStatic m_wndLbl_Datum_To;

	//Datum fr�n
	CDateTimeCtrl m_wndDateTimeCtrlFrom;
	CString sDateFromSql;
	int nCheckedTo;
	//Datum till
	CDateTimeCtrl m_wndDateTimeCtrlTo;
	CString sDateToSql;
	int nCheckedFrom;

	//Areal
	CMyExtStatic m_wndLbl_Areal;
	CMyExtStatic m_wndLbl_Areal_From;
	CMyExtStatic m_wndLbl_Areal_To;
	CMyExtEdit2 m_wndEdit_Areal_From;
	CMyExtEdit2 m_wndEdit_Areal_To;
	CString sArealFromSql;
	CString sArealToSql;

	//Skapa filer knapp
	CButton m_wndMakeFiles;

	void OnClickedPlantButtons(int var);
	void ClearAndUpdateUrval(int var);
	void Populate_TraktReport(void);

	void Init();

	//int m_nForenklad;
	std::vector<CMyReportCtrl2 *> vecReportControls;
	
	vecTraktData_Plant m_vecTraktData_Plant;
	vecTraktData_Mark m_vecTraktData_Mark;
	vecTraktData_Roj m_vecTraktData_Roj;

	CString m_sEnteredManual;
	CString m_sEnteredFromFile;

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

// Dialog Data
	enum { IDD = IDD_FORMVIEW4 };

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	//void forenklad(int forenklad);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);


	void setLanguage(void);
	


 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	bool OnChangeDtpicker1();
	afx_msg void OnChangeDatetimepicker4_1( NMHDR* pNMHDR, LRESULT* pResult );
	afx_msg void OnChangeDatetimepicker4_2( NMHDR* pNMHDR, LRESULT* pResult );

	afx_msg void OnArealFromChange();
	afx_msg void OnArealToChange();

	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * );

	afx_msg void OnReportUrvalClick(NMHDR * pNotifyStruct, LRESULT * );
	afx_msg LRESULT OnArealChange( WPARAM wParam, LPARAM lParam );	
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnBnCreateFiles(void);
	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};