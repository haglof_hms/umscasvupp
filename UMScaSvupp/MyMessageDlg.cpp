// MessageDlg.cpp : implementation file
//

#include "stdafx.h"

#include "MyMessageDlg.h"



// CMyMessageDlg dialog
IMPLEMENT_DYNAMIC(CMyMessageDlg, CDialog)

BEGIN_MESSAGE_MAP(CMyMessageDlg, CDialog)
END_MESSAGE_MAP()

CMyMessageDlg::CMyMessageDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMyMessageDlg::IDD, pParent)
{
}

CMyMessageDlg::CMyMessageDlg(LPCTSTR cap, LPCTSTR ok_btn, LPCTSTR cancel_btn, LPCTSTR msg, CWnd* pParent /*=NULL*/)
	: CDialog(CMyMessageDlg::IDD, pParent)
{
	m_sCaption = cap;
	m_sOKBtn = ok_btn;
	m_sCancelBtn = cancel_btn;
	m_sMsgText = msg;
}

CMyMessageDlg::~CMyMessageDlg()
{
}

void CMyMessageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_HTML_TEXT, m_wndHTML);
	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);
	//}}AFX_DATA_MAP

}

BOOL CMyMessageDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowText(m_sCaption);

	m_wndHTML.SetWindowText(m_sMsgText);
	m_wndHTML.ModifyStyleEx(0, WS_EX_TRANSPARENT);

	m_wndOKBtn.SetWindowText(m_sOKBtn);
	m_wndCancelBtn.SetWindowText(m_sCancelBtn);


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


// CMyFileMessageDlg dialog
IMPLEMENT_DYNAMIC(CMyFileMessageDlg, CDialog)

BEGIN_MESSAGE_MAP(CMyFileMessageDlg, CDialog)
	
	ON_BN_CLICKED(IDC_BUTTON_RENAME, &CMyFileMessageDlg::OnBnClickedButtonRename)
END_MESSAGE_MAP()

CMyFileMessageDlg::CMyFileMessageDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMyFileMessageDlg::IDD, pParent)
{
}

CMyFileMessageDlg::CMyFileMessageDlg(CString cap, CString yes_btn, CString no_btn, CString yes_to_all_btn, CString no_to_all_btn, CString msg, CString rename, CWnd* pParent/*=NULL*/)
: CDialog(CMyFileMessageDlg::IDD, pParent)
{
	m_sCaption = cap;
	m_sYesBtn = yes_btn;
	m_sYesToAllBtn = yes_to_all_btn;
	m_sNoBtn = no_btn;
	m_sNoToAllBtn = no_to_all_btn;
	m_sMsgText = msg;
	m_sRename = rename;
}

CMyFileMessageDlg::~CMyFileMessageDlg()
{
}

void CMyFileMessageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_HTML_TEXT_FILEDLG	, m_wndHTML);
	DDX_Control(pDX, IDOK					, m_wndYesBtn);
	DDX_Control(pDX, IDYES					, m_wndYesToAllBtn);
	DDX_Control(pDX, IDCANCEL				, m_wndNoBtn);
	DDX_Control(pDX, IDNO					, m_wndNoToAllBtn);
	DDX_Control(pDX, IDC_BUTTON_RENAME		, m_wndRename);
	//}}AFX_DATA_MAP

}

BOOL CMyFileMessageDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowText(m_sCaption);
	m_wndYesBtn.SetWindowText(m_sYesBtn);
	m_wndYesToAllBtn.SetWindowText(m_sYesToAllBtn);
	m_wndNoBtn.SetWindowText(m_sNoBtn);
	m_wndNoToAllBtn.SetWindowText(m_sNoToAllBtn);
	m_wndHTML.SetWindowText(m_sMsgText);
	m_wndHTML.ModifyStyleEx(0, WS_EX_TRANSPARENT);
	m_wndRename.SetWindowText(m_sRename);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void CMyFileMessageDlg::OnBnClickedButtonRename()
{
	CDialog::EndDialog(IDRETRY);
}
