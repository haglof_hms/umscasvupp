// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500                 // Target Windows 2000

#define _WIN32_WINNT 0x0500


#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#define _CRT_SECURE_NO_WARNING
#define _CRT_SECURE_NO_DEPRECATE

#define _CRT_NON_CONFORMING_SWPRINTFS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

//#ifndef _AFX_NO_DAO_SUPPORT
//#include <afxdao.h>			// MFC DAO database classes
//#endif // _AFX_NO_DAO_SUPPORT

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <vector>
#include <XTToolkitPro.h>	// Xtreme Toolkit MFC extensions

#include "SQLAPI.h"
#include "DBBaseClass_ADODirect.h"
#include "pad_hms_miscfunc.h"

#include "UMScaSvuppDB.h"
#include <afxdhtml.h>

static BOOL bShowLicenseDialog=TRUE;

extern HINSTANCE g_hInstance;

const LPCTSTR LICENSE_FILE_NAME						= _T("License.dll");
const LPCTSTR LICENSE_NAME								= _T("License");
const LPCTSTR UMSCAGALLBAS_LIC_ID					= _T("H9001");
const LPCTSTR LICENSE_CHECK							= _T("CheckLicense");

const LPCTSTR PROGRAM_NAME								= _T("UMScaSvupp");	



const LPCTSTR REG_WP_SCASVUPP_TRAKT_PLANT_KEY			= _T("UMScaSvupp\\Trakt\\Plant\\Placement");
const LPCTSTR REG_WP_SCASVUPP_LIST_TRAKT_PLANT_KEY		= _T("UMScaSvupp\\ListTrakt\\Plant\\Placement");
const LPCTSTR REG_WP_SCASVUPP_TRAKT_PLANT_FILEPATH_KEY= _T("UMScaSvupp\\ListTrakt\\Plant\\Placement");
const LPCTSTR REG_WP_SCASVUPP_SETTINGS						= _T("UMScaSvupp\\Settings\\Placement");

const LPCTSTR REG_SCASVUPP_XML_PLANT_FILEPATH_KEY_NAME= _T("PathPlant");
const LPCTSTR REG_SCASVUPP_XML_MARK_FILEPATH_KEY_NAME	= _T("PathMark");
const LPCTSTR REG_SCASVUPP_XML_ROJ_FILEPATH_KEY_NAME	= _T("PathRoj");

//const LPCTSTR REG_WP_SCAGB_TEST_KEY					= _T("UMScaSvupp\\Test\\Placement");



const LPCTSTR TBL_PLANT_TRAKT							= _T("dbo.plantupp_trakt");
const LPCTSTR TBL_PLANT_PLOT 							= _T("dbo.plantupp_yta");

const LPCTSTR TBL_MARK_TRAKT							= _T("dbo.markberedning_trakt");
const LPCTSTR TBL_MARK_PLOT 							= _T("dbo.markberedning_yta");

const LPCTSTR TBL_ROJ_TRAKT								= _T("dbo.rojning_trakt");
const LPCTSTR TBL_ROJ_PLOT								= _T("dbo.rojning_yta");

#define ID_UPDATE_ITEM									19999

#define WM_USER_MSG_SUITE								(WM_USER + 2)
#define MSG_IN_SUITE										(WM_USER + 10)

#define ID_NEW_ITEM										32786
#define ID_OPEN_ITEM										32787
#define ID_SAVE_ITEM										32788
#define ID_DELETE_ITEM									32789
#define ID_PREVIEW_ITEM									32790

// Defines for Database navigation button, in HMSShell toolbar; 060412 p�d
#define ID_DBNAVIG_START								32778
#define ID_DBNAVIG_NEXT									32779
#define ID_DBNAVIG_PREV									32780
#define ID_DBNAVIG_END									32781
#define ID_DBNAVIG_LIST									32791

//strings in xml-file
#define IDS_STRING8500	8500
#define IDS_STRING8501	8501
#define IDS_STRING8502	8502
#define IDS_STRING8503	8503
#define IDS_STRING8504	8504
#define IDS_STRING8505	8505
#define IDS_STRING8506	8506
#define IDS_STRING8507	8507
#define IDS_STRING8508	8508
#define IDS_STRING8509	8509
#define IDS_STRING8510	8510
#define IDS_STRING8511	8511
#define IDS_STRING8512	8512
#define IDS_STRING8513	8513
#define IDS_STRING8514	8514
#define IDS_STRING8515	8515
#define IDS_STRING8516	8516
#define IDS_STRING8517	8517

#define IDS_STRING8518	8518
#define IDS_STRING8519	8519


#define IDS_STRING8520	8520
#define IDS_STRING8521	8521
#define IDS_STRING8522	8522
#define IDS_STRING8523	8523
#define IDS_STRING8524	8524
#define IDS_STRING8525	8525
#define IDS_STRING8526	8526
#define IDS_STRING8527	8527
#define IDS_STRING8528	8528
#define IDS_STRING8529	8529
#define IDS_STRING8530	8530
#define IDS_STRING8531	8531
#define IDS_STRING8532	8532
#define IDS_STRING8533	8533
#define IDS_STRING8534	8534
#define IDS_STRING8535	8535
#define IDS_STRING8536	8536
#define IDS_STRING8537	8537
#define IDS_STRING8538	8538
#define IDS_STRING8539	8539



#define IDS_STRING_FORV		8540
#define IDS_STRING_DISTR	8541
#define IDS_STRING_URSPR	8542
#define IDS_STRING_ATGARDSID	8543
#define IDS_STRING_NAMN		8544
#define IDS_STRING_AREAL	8545
#define IDS_STRING_ENTR		8546
#define IDS_STRING_LAG		8547
#define IDS_STRING_DATUM	8548
#define IDS_STRING_INVENT	8549
#define IDS_STRING_MBTYP	8550
#define IDS_STRING_MALPL	8551
#define IDS_STRING_INVTYP	8552

#define IDS_STRING_TRAKT	8560
#define IDS_STRING_YTOR		8561

#define IDS_STRING8570	8570
#define IDS_STRING8571	8571

#define IDS_STRING8580	8580
#define IDS_STRING8581	8581
#define IDS_STRING8582	8582
#define IDS_STRING8583	8583
#define IDS_STRING8584	8584
#define IDS_STRING8585	8585
#define IDS_STRING8586	8586
#define IDS_STRING8587	8587

#define IDS_STRING_METOD	8590
#define IDS_STRING_AGGR		8591
#define IDS_STRING_MALMB	8592
#define IDS_STRING_HOGT_SNYTBAGGETRYCK 8597
#define IDS_STRING_SKADADE_FORNMINNEN 8600
#define IDS_STRING_SKADADE_KULTURMINNEN 8601
#define IDS_STRING_KORSKADOR 8602

#define IDS_STRING_SI		8593


#define IDS_STRING_URVAL_PLANT		8594
#define IDS_STRING_URVAL_MARK		8595
#define IDS_STRING_URVAL_ROJ		8596


#define IDC_TABCONTROL_TRAKT							0x8001
#define IDC_TRAKT_REPORT_3								0x8002
#define IDC_TABCONTROL_1								0x8003
#define IDC_PROPERTY_GRID								0x8004

#define IDC_3_1_REPORT							0x8005
#define IDC_3_2_REPORT							0x8006
#define IDC_3_3_REPORT							0x8007
#define IDC_3_4_REPORT							0x8008
#define IDC_3_5_REPORT							0x8009
#define IDC_3_6_REPORT							0x8010
#define IDC_3_7_REPORT							0x8011
#define IDC_3_8_REPORT							0x8012

#define IDC_4_1_REPORT							0x8013
#define IDC_4_2_REPORT							0x8014
#define IDC_4_3_REPORT							0x8015
#define IDC_4_4_REPORT							0x8016
#define IDC_4_5_REPORT							0x8017
#define IDC_4_6_REPORT							0x8018
#define IDC_4_7_REPORT							0x8019
#define IDC_4_8_REPORT							0x8020
#define IDC_4_9_REPORT							0x8021

#define IDC_5_1_REPORT							0x8022
#define IDC_5_2_REPORT							0x8023
#define IDC_5_3_REPORT							0x8024
#define IDC_5_4_REPORT							0x8025
#define IDC_5_5_REPORT							0x8026
#define IDC_5_6_REPORT							0x8027
#define IDC_5_7_REPORT							0x8028

#define IDC_TRAKT_REPORT_4						0x8029
#define IDC_TRAKT_REPORT_5						0x8030


const int MIN_X_SIZE_SCASVUPP_TRAKT					= 680;
const int MIN_Y_SIZE_SCASVUPP_TRAKT					= 560;

const int MIN_X_SIZE_SCASVUPP_LIST_TRAKT			= 600;
const int MIN_Y_SIZE_SCASVUPP_LIST_TRAKT			= 400;

const int MIN_Y_SIZE_SCASVUPP_SETTINGS				=200;
const int MIN_X_SIZE_SCASVUPP_SETTINGS				=200;

const int COL_SIZE_TAB_PLANT_TRAKT					= 80;
const int COL_SIZE_TAB_PLANT_PLOT					= 80;

const int X_REPORT_1	= 10;
const int Y_REPORT_1	= 30;
const int W_REPORT_1	= 120;
const int H_REPORT_1	= 100;

const int X_REPORT_2	= 135;
const int Y_REPORT_2	= 30;
const int W_REPORT_2	= 120;
const int H_REPORT_2	= 100;

const int X_REPORT_3	= 260;
const int Y_REPORT_3	= 30;
const int W_REPORT_3	= 120;
const int H_REPORT_3	= 100;

const int X_REPORT_4	= 385;
const int Y_REPORT_4	= 30;
const int W_REPORT_4	= 120;
const int H_REPORT_4	= 100;

const int X_REPORT_5	= 510;
const int Y_REPORT_5	= 30;
const int W_REPORT_5	= 185;
const int H_REPORT_5	= 100;

const int X_REPORT_6	= 700;
const int Y_REPORT_6	= 30;
const int W_REPORT_6	= 120;
const int H_REPORT_6	= 100;

const int X_REPORT_7	= 825;
const int Y_REPORT_7	= 30;
const int W_REPORT_7	= 120;
const int H_REPORT_7	= 100;

const int X_REPORT_8	= 950;
const int Y_REPORT_8	= 30;
const int W_REPORT_8	= 120;
const int H_REPORT_8	= 100;

const int X_REPORT_9	= 1075;
const int Y_REPORT_9	= 30;
const int W_REPORT_9	= 120;
const int H_REPORT_9	= 100;


const int X_REPORT_PLANT_AREAL	= 200;
const int Y_REPORT_PLANT_AREAL	= 160;
const int W_REPORT_PLANT_AREAL	= 30;
const int H_REPORT_PLANT_AREAL	= 15;

const int X_REPORT_PLANT_AREAL_FROM	= 200;
const int Y_REPORT_PLANT_AREAL_FROM	= 180;
const int W_REPORT_PLANT_AREAL_FROM	= 30;
const int H_REPORT_PLANT_AREAL_FROM	= 15;

const int X_REPORT_PLANT_AREAL_EDIT_FROM	= 235;
const int Y_REPORT_PLANT_AREAL_EDIT_FROM	= 180;
const int W_REPORT_PLANT_AREAL_EDIT_FROM	= 110;
const int H_REPORT_PLANT_AREAL_EDIT_FROM	= 20;

const int X_REPORT_PLANT_AREAL_TO	= 200;
const int Y_REPORT_PLANT_AREAL_TO	= 200;
const int W_REPORT_PLANT_AREAL_TO	= 30;
const int H_REPORT_PLANT_AREAL_TO	= 15;

const int X_REPORT_PLANT_AREAL_EDIT_TO	= 235;
const int Y_REPORT_PLANT_AREAL_EDIT_TO	= 200;
const int W_REPORT_PLANT_AREAL_EDIT_TO	= 110;
const int H_REPORT_PLANT_AREAL_EDIT_TO	= 20;

const int X_REPORT_PLANT_DATUM	= 10;
const int Y_REPORT_PLANT_DATUM	= 160;
const int W_REPORT_PLANT_DATUM	= 30;
const int H_REPORT_PLANT_DATUM	= 15;

const int X_REPORT_PLANT_DATUM_FROM	= 10;
const int Y_REPORT_PLANT_DATUM_FROM	= 180;
const int W_REPORT_PLANT_DATUM_FROM	= 30;
const int H_REPORT_PLANT_DATUM_FROM	= 15;

const int X_REPORT_PLANT_DATUM_EDIT_FROM	= 45;
const int Y_REPORT_PLANT_DATUM_EDIT_FROM	= 180;
const int W_REPORT_PLANT_DATUM_EDIT_FROM	= 110;
const int H_REPORT_PLANT_DATUM_EDIT_FROM	= 20;

const int X_REPORT_PLANT_DATUM_TO	= 10;
const int Y_REPORT_PLANT_DATUM_TO	= 200;
const int W_REPORT_PLANT_DATUM_TO	= 30;
const int H_REPORT_PLANT_DATUM_TO	= 15;

const int X_REPORT_PLANT_DATUM_EDIT_TO	= 45;
const int Y_REPORT_PLANT_DATUM_EDIT_TO	= 200;
const int W_REPORT_PLANT_DATUM_EDIT_TO	= 110;
const int H_REPORT_PLANT_DATUM_EDIT_TO	= 20;

const int X_REPORT_PLANT_FILES	= 45;
const int Y_REPORT_PLANT_FILES	= 225;
const int W_REPORT_PLANT_FILES	= 80;
const int H_REPORT_PLANT_FILES	= 30;

const int X_REPORT			= 1;
const int Y_REPORT			= 280;
const int W_REPORT			= 2;
const int H_REPORT			= 200;

#define MAX_NUM_OF_URVAL	10
typedef enum _inventtypes {PLANT_TYPE,MARK_TYPE,ROJ_TYPE} enumINVENTTYPES;

#define NUM_OF_PLANT_URVAL 8
#define NUM_OF_MARK_URVAL 9
#define NUM_OF_ROJ_URVAL 7
//typedef enum _planturval {PLANT_URVAL_FORVALTNING,PLANT_URVAL_DISTRIKT,PLANT_URVAL_ENTREPRENOR,PLANT_URVAL_LAG,PLANT_URVAL_INVENT,PLANT_URVAL_MBTYP,PLANT_URVAL_OBJID,PLANT_URVAL_URSPR} enumPLANTURVAL;

typedef enum _action { MANUALLY,FROM_FILE,NOTHING,CHANGED } enumACTION;

typedef enum _traktpages { PAGE_TRAKT,PAGE_YTA} enumTRAKTPAGES;

typedef enum _reset {	RESET_TO_FIRST_SET_NB,					// Set to first item and set Navigationbar
								RESET_TO_FIRST_NO_NB,					// Set to first item and no Navigationbar (disable all)
								RESET_TO_LAST_SET_NB,					// Set to last item and set Navigationbar
								RESET_TO_LAST_NO_NB,						// Set to last item and no Navigationbar (disable all)
								RESET_TO_JUST_ENTERED_SET_NB,			// Set to just entered data (from file or manually) and set Navigationbar
								RESET_TO_JUST_ENTERED_NO_NB			// Set to just entered data (from file or manually) and no Navigationbar
								} enumRESET;

// Question 1 selects ALL "trakts"
//const LPCTSTR SQL_TRAKT_QUESTION1			= _T("select * from %s order by Datum");

const LPCTSTR SQL_TRAKT_QUESTION_INDEX1	= _T("select Lopnr from %s order by Lopnr");

const LPCTSTR SQL_PLOT_QUESTION1		= _T("select * from %s order by YtNr");


