#pragma once
#include "stdafx.h"
#include "Resource.h"

// CScaSvuppSettingsFormView dialog

class CScaSvuppSettingsFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CScaSvuppSettingsFormView)

protected:		
	CScaSvuppSettingsFormView();   // standard constructor
	virtual ~CScaSvuppSettingsFormView();

public:
// Dialog Data
	enum { IDD = IDD_FORMVIEW };

	CString m_sCaption;
	CString m_sLangFN;
	CString m_sFilepathPlant;
	CString m_sFilepathMark;
	CString m_sFilepathRoj;

	CString m_sLangAbbrev;
	void setLanguage(void);


public:
	
protected:

	CXTPPropertyGridItem* m_pItemPathPlant;
	CXTPPropertyGridItem* m_pItemPathMark;
	CXTPPropertyGridItem* m_pItemPathRoj;
	CXTPPropertyGrid m_wndPropertyGrid;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate();
	
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	//afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnClose();
	//afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg LRESULT OnValueChanged(WPARAM wParam, LPARAM lParam);
};

class CCustomItemFileBox : public CXTPPropertyGridItem
{
public:
	CCustomItemFileBox(CString strCaption, CString strValue);


protected:
//	virtual void OnInplaceButtonDown();
	virtual void OnInplaceButtonDown(CXTPPropertyGridInplaceButton* pButton);
};