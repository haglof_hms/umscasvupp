#include "stdafx.h"
#include "ScaGBTraktPages.h"
#include "ResLangFileReader.h"
#include "MyProgressBarDlg.h"
	
IMPLEMENT_DYNCREATE(CMyReportPageBase,CXTPReportView)
//------------------------------------------------------------------------------------------------------
//		BASKLASS FLIKAR
//------------------------------------------------------------------------------------------------------
CMyReportPageBase::CMyReportPageBase()
	:CXTPReportView()
{

		
		m_nVarArray[0]=IDS_STRING8500;
		m_nVarArray[1]=IDS_STRING8501;
		m_nVarArray[2]=IDS_STRING8502;
		m_nVarArray[3]=IDS_STRING8503;
		m_nVarArray[4]=IDS_STRING8504;
		m_nVarArray[5]=IDS_STRING8505;
		m_nVarArray[6]=IDS_STRING8506;
		m_nVarArray[7]=IDS_STRING8507;
		m_nVarArray[8]=IDS_STRING8508;
		m_nVarArray[9]=IDS_STRING8509;
		m_nVarArray[10]=IDS_STRING8510;
		m_nVarArray[11]=IDS_STRING8511;
		m_nVarArray[12]=IDS_STRING8512;
		m_nVarArray[13]=IDS_STRING8513;
		m_nVarArray[14]=IDS_STRING8514;
		m_nVarArray[15]=IDS_STRING8515;
		m_nVarArray[16]=IDS_STRING8516;
		m_nVarArray[17]=IDS_STRING8517;


		m_nYtArray[0]=IDS_STRING8520;
		m_nYtArray[1]=IDS_STRING8521;
		m_nYtArray[2]=IDS_STRING8522;
		m_nYtArray[3]=IDS_STRING8523;
		m_nYtArray[4]=IDS_STRING8524;
		m_nYtArray[5]=IDS_STRING8525;
		m_nYtArray[6]=IDS_STRING8526;
		m_nYtArray[7]=IDS_STRING8527;
		m_nYtArray[8]=IDS_STRING8528;
		m_nYtArray[9]=IDS_STRING8529;
		m_nYtArray[10]=IDS_STRING8530;
		m_nYtArray[11]=IDS_STRING8531;
		m_nYtArray[12]=IDS_STRING8532;
		m_nYtArray[13]=IDS_STRING8533;
		m_nYtArray[14]=IDS_STRING8534;
		m_nYtArray[15]=IDS_STRING8535;
		m_nYtArray[16]=IDS_STRING8536;
		m_nYtArray[17]=IDS_STRING8537;
		m_nYtArray[18]=IDS_STRING8538;
		m_nYtArray[19]=IDS_STRING8539;

		// Setup language filename; 051214 p�d
		m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);


}

void CMyReportPageBase::ClearReport(CXTPReportControl *rep)
{
	if (rep->GetSafeHwnd() != NULL)
	{
		CXTPReportRecords* pRecords = rep->GetRecords();
		if (pRecords)
		{
			pRecords->RemoveAll();
			return;
		}
	}		
}


//------------------------------------------------------------------------------------------------------
//		KVARVARANDE
//------------------------------------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(CMyReportPage1, CMyReportPageBase)

BEGIN_MESSAGE_MAP(CMyReportPage1, CMyReportPageBase)

END_MESSAGE_MAP()

CMyReportPage1::CMyReportPage1()
	: CMyReportPageBase()
{
m_bDoInit=TRUE;
}

// Initiera kolumnheaders i fliken
void CMyReportPage1::OnInitialUpdate()
{
	int nCol=0;
	if(m_bDoInit)
	{
	CXTPReportColumn *pCol = NULL;
	CXTPReportControl *m_ReportCtrl;
   CMyReportPageBase::OnInitialUpdate();
	m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	
	// L�gger till kol
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{					
			m_ReportCtrl->ShowWindow( SW_NORMAL );
			for(nCol=0;nCol<NUM_OF_PLANT_TRAKT_VAR;nCol++)
			{
				pCol = m_ReportCtrl->AddColumn(new CXTPReportColumn(nCol, xml->str(m_nVarArray[nCol]), COL_SIZE_TAB_PLANT_TRAKT));									
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
				pCol->GetEditOptions()->m_dwEditStyle  = ES_CENTER;	
				pCol->SetEditable( TRUE );

				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );
			}
			pCol->SetHeaderAlignment( DT_CENTER );
			pCol->SetAlignment( DT_CENTER );

			m_ReportCtrl->GetReportHeader()->AllowColumnSort(FALSE);
			m_ReportCtrl->AllowEdit(FALSE);

			m_ReportCtrl->GetReportHeader()->AllowColumnRemove(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnReorder(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnResize(TRUE);
			m_ReportCtrl->GetReportHeader()->SetAutoColumnSizing(FALSE);
			m_ReportCtrl->EnableScrollBar(SB_HORZ, TRUE );
			m_ReportCtrl->EnableScrollBar(SB_VERT, TRUE );
			m_ReportCtrl->SetMultipleSelection(FALSE);
			//m_ReportCtrl.SetGridStyle(TRUE, xtpReportGridSolid);
			//m_ReportCtrl.SetGridStyle(FALSE, xtpReportGridSolid);	
			m_ReportCtrl->FocusSubItems(TRUE);

		}			
	}
	m_bDoInit=FALSE;
	}
}
//---------------------------
// Visa Data f�r trakt
//---------------------------
void CMyReportPage1::showData(TRAKT_DATA_PLANT data)
{
	
	CXTPReportControl *m_ReportCtrl;
	int nSpec=0;

   m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_ReportCtrl->AddRecord(new CTraktViewData(data));
			m_ReportCtrl->Populate();
			m_ReportCtrl->UpdateWindow();
		}
	}
}



//------------------------------------------------------------------------------------------------------
//		YTOR
//------------------------------------------------------------------------------------------------------
IMPLEMENT_DYNCREATE(CMyReportPage2, CMyReportPageBase)

BEGIN_MESSAGE_MAP(CMyReportPage2, CMyReportPageBase)	
END_MESSAGE_MAP()

CMyReportPage2::CMyReportPage2()
	: CMyReportPageBase()
{
m_bDoInit=TRUE;
}

void CMyReportPage2::OnInitialUpdate()
{
	int nCol=0,nCol2=0;
	if(m_bDoInit)
	{
	CXTPReportColumn *pCol = NULL;
	CXTPReportControl *m_ReportCtrl;
   CMyReportPageBase::OnInitialUpdate();
	m_ReportCtrl=&GetReportCtrl();
	ClearReport(m_ReportCtrl);
	// L�gger till kol
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{					
			m_ReportCtrl->ShowWindow( SW_NORMAL );
			nCol2=0;
			for(nCol=0;nCol<NUM_OF_PLANT_TRAKT_YTVAR;nCol++)
			{

				pCol = m_ReportCtrl->AddColumn(new CXTPReportColumn(nCol2, xml->str(m_nYtArray[nCol]), COL_SIZE_TAB_PLANT_PLOT));

				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
				pCol->GetEditOptions()->m_dwEditStyle = ES_CENTER;	   //only allow numbers
				pCol->SetEditable( FALSE );

				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );
				nCol2++;

			}
			m_ReportCtrl->GetReportHeader()->AllowColumnSort(TRUE);
			m_ReportCtrl->AllowEdit(FALSE);						
			m_ReportCtrl->GetReportHeader()->AllowColumnRemove(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnReorder(FALSE);
			m_ReportCtrl->GetReportHeader()->AllowColumnResize(TRUE);
			m_ReportCtrl->GetReportHeader()->SetAutoColumnSizing(FALSE);
			m_ReportCtrl->EnableScrollBar(SB_HORZ, TRUE );
			m_ReportCtrl->EnableScrollBar(SB_VERT, TRUE );
			m_ReportCtrl->SetMultipleSelection(FALSE);
			//m_ReportCtrl.SetGridStyle(TRUE, xtpReportGridSolid);
			//m_ReportCtrl.SetGridStyle(FALSE, xtpReportGridSolid);	
			m_ReportCtrl->FocusSubItems(TRUE);			
		}			
	} 
	m_bDoInit=FALSE;
	}
}

//---------------------------
// Visa Data f�r ytor
//---------------------------
void CMyReportPage2::showData(TRAKT_DATA_PLANT data)
{
	CXTPReportControl *m_ReportCtrl;
	int nSpec=0;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_ReportCtrl=&GetReportCtrl();
			ClearReport(m_ReportCtrl);
			for(unsigned int pi=0;pi<data.m_vPlotData.size();pi++)
			{							
				m_ReportCtrl->AddRecord(new CTraktViewData(data.m_vPlotData[pi]));		
			}
			m_ReportCtrl->Populate();
			m_ReportCtrl->UpdateWindow();	
		}
	}
}



