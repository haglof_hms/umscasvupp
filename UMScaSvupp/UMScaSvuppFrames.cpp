// MDIScaGBTraktFrame.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
//#include "MDIScaGBTraktFrame.h"
#include "UMScaSvuppFrames.h"
#include "UMScaSvuppTraktFormView.h"
#include "ResLangFileReader.h"
#include "UMScaSvuppUrvalPlant.h"

// CMDIScaGBTraktFrame dialog

IMPLEMENT_DYNCREATE(CMDIScaGBTraktFrame, CMDIChildWnd)

CMDIScaGBTraktFrame::CMDIScaGBTraktFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
}

CMDIScaGBTraktFrame::~CMDIScaGBTraktFrame()
{
}

BEGIN_MESSAGE_MAP(CMDIScaGBTraktFrame, CMDIChildWnd)
		ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


void CMDIScaGBTraktFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SCASVUPP_TRAKT_PLANT_KEY);
	
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}


void CMDIScaGBTraktFrame::OnClose(void)
{
	CMDIChildWnd::OnClose();
}

int CMDIScaGBTraktFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	//if (!isDBConnection(m_sLangFN))
	//	return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CMDIScaGBTraktFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SCASVUPP_TRAKT_PLANT_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIScaGBTraktFrame::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_PREVIEW_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
/*	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,TRUE);*/
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

}

BOOL CMDIScaGBTraktFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIScaGBTraktFrame diagnostics

#ifdef _DEBUG
void CMDIScaGBTraktFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIScaGBTraktFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDIScaGBTraktFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_SCASVUPP_TRAKT;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_SCASVUPP_TRAKT;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIScaGBTraktFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIScaGBTraktFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIScaGBTraktFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	if (wParam == ID_DBNAVIG_LIST)
	{
		::SetCursor(::LoadCursor(NULL,IDC_WAIT));
		showFormView(IDD_FORMVIEW3,m_sLangFN);
		::SetCursor(::LoadCursor(NULL,IDC_ARROW));
		//CUMScaSvuppUrvalPlant *pView = (CUMScaSvuppUrvalPlant *)getFormViewByID(IDD_FORMVIEW3);
		//pView->forenklad(0);
	}
	else
	{
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	}
	return 0L;
}

/****************************************************/
/////////////////////////////////////////////////////////////////////////////
// CUMScaSvuppUrvalPlantFrame

IMPLEMENT_DYNCREATE(CUMScaSvuppUrvalPlantFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CUMScaSvuppUrvalPlantFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CUMScaSvuppUrvalPlantFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CUMScaSvuppUrvalPlantFrame::CUMScaSvuppUrvalPlantFrame()
{

	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
}

CUMScaSvuppUrvalPlantFrame::~CUMScaSvuppUrvalPlantFrame()
{	
}

void CUMScaSvuppUrvalPlantFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SCASVUPP_LIST_TRAKT_PLANT_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

int CUMScaSvuppUrvalPlantFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{	
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CUMScaSvuppUrvalPlantFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
	{

		m_bFirstOpen = FALSE;
		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SCASVUPP_LIST_TRAKT_PLANT_KEY);
		LoadPlacement(this, csBuf);
	}
	
}

void CUMScaSvuppUrvalPlantFrame::OnSetFocus(CWnd*)
{

}

BOOL CUMScaSvuppUrvalPlantFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;
	
	return TRUE;
}


void CUMScaSvuppUrvalPlantFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_SCASVUPP_LIST_TRAKT;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_SCASVUPP_LIST_TRAKT;
	
	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CUMScaSvuppUrvalPlantFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CUMScaSvuppUrvalPlantFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CUMScaSvuppUrvalPlantFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}

/****************************************************/
/////////////////////////////////////////////////////////////////////////////
// CUMScaSvuppUrvalMarkFrame

IMPLEMENT_DYNCREATE(CUMScaSvuppUrvalMarkFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CUMScaSvuppUrvalMarkFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CUMScaSvuppUrvalMarkFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CUMScaSvuppUrvalMarkFrame::CUMScaSvuppUrvalMarkFrame()
{

	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
}

CUMScaSvuppUrvalMarkFrame::~CUMScaSvuppUrvalMarkFrame()
{
}

void CUMScaSvuppUrvalMarkFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SCASVUPP_LIST_TRAKT_PLANT_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

int CUMScaSvuppUrvalMarkFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{	
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CUMScaSvuppUrvalMarkFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
	{

		m_bFirstOpen = FALSE;
		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SCASVUPP_LIST_TRAKT_PLANT_KEY);
		LoadPlacement(this, csBuf);
	}
	
}

void CUMScaSvuppUrvalMarkFrame::OnSetFocus(CWnd*)
{

}

BOOL CUMScaSvuppUrvalMarkFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;
	
	return TRUE;
}


void CUMScaSvuppUrvalMarkFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_SCASVUPP_LIST_TRAKT;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_SCASVUPP_LIST_TRAKT;
	
	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CUMScaSvuppUrvalMarkFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CUMScaSvuppUrvalMarkFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CUMScaSvuppUrvalMarkFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}


/****************************************************/
/////////////////////////////////////////////////////////////////////////////
// CUMScaSvuppUrvalRojFrame

IMPLEMENT_DYNCREATE(CUMScaSvuppUrvalRojFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CUMScaSvuppUrvalRojFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CUMScaSvuppUrvalRojFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CUMScaSvuppUrvalRojFrame::CUMScaSvuppUrvalRojFrame()
{

	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
}

CUMScaSvuppUrvalRojFrame::~CUMScaSvuppUrvalRojFrame()
{
}

void CUMScaSvuppUrvalRojFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SCASVUPP_LIST_TRAKT_PLANT_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

int CUMScaSvuppUrvalRojFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{	
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CUMScaSvuppUrvalRojFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
	{

		m_bFirstOpen = FALSE;
		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SCASVUPP_LIST_TRAKT_PLANT_KEY);
		LoadPlacement(this, csBuf);
	}
	
}

void CUMScaSvuppUrvalRojFrame::OnSetFocus(CWnd*)
{

}

BOOL CUMScaSvuppUrvalRojFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;
	
	return TRUE;
}


void CUMScaSvuppUrvalRojFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_SCASVUPP_LIST_TRAKT;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_SCASVUPP_LIST_TRAKT;
	
	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CUMScaSvuppUrvalRojFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CUMScaSvuppUrvalRojFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CUMScaSvuppUrvalRojFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}