#pragma once

#ifndef _RESLANGFILEREADER_H_
#define _RESLANGFILEREADER_H_

class RLFReader
{
  CString m_sModulePath;
	CXTPPropExchangeXMLNode* m_pResources;
public:
	RLFReader(void);

	virtual ~RLFReader();

	BOOL Load(LPCTSTR);

	CString str(DWORD resid);
};

#endif
