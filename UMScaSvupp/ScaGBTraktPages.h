#pragma once

#ifndef _HMS_SCATRAKTPAGES_H_
#define _HMS_SCATRAKTPAGES_H_
#include "UMHmsMisc.h"
#include "MyProgressBarDlg.h"
#include "UMScaSvuppUrvalPlant.h"



class CMyReportPageBase : public CXTPReportView
{
	DECLARE_DYNCREATE(CMyReportPageBase);
public:
	int m_nVarArray[NUM_OF_PLANT_TRAKT_VAR],m_nYtArray[NUM_OF_PLANT_TRAKT_YTVAR];
	CString	m_sLangFN;
public:
	CMyReportPageBase(void);
	void ClearReport(CXTPReportControl *rep);
};

//Klass f�r Traktdata fliken i traktf�nstret
class CMyReportPage1 : public CMyReportPageBase
{
	DECLARE_DYNCREATE(CMyReportPage1);
public:
	CMyReportPage1(void);
private:
	BOOL m_bDoInit;
public:
	virtual void OnInitialUpdate(void);
	void showData(TRAKT_DATA_PLANT data);
	DECLARE_MESSAGE_MAP()
};



//Klass f�r Ytor fliken i traktf�nstret
class CMyReportPage2 : public CMyReportPageBase
{
	DECLARE_DYNCREATE(CMyReportPage2);

public:
	CMyReportPage2(void);
private:
	BOOL m_bDoInit;
public:
	virtual void OnInitialUpdate(void);
	void showData(TRAKT_DATA_PLANT data);
	DECLARE_MESSAGE_MAP()
};


#endif