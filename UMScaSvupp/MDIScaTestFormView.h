#pragma once

#include "UMScaGallBasDB.h"
#include "Resource.h"
#include "UMHmsMisc.h"
// CMDIScaTestFormView dialog

class CMDIScaTestFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CMDIScaTestFormView)
	CString m_sLangAbbrev;
	CString m_sLangFN;

protected:
	CMDIScaTestFormView();   // standard constructor
	virtual ~CMDIScaTestFormView();

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

// Dialog Data
public:
	enum { IDD = IDD_FORMVIEW3 };

public:
	virtual void OnInitialUpdate();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	//afx_msg void OnSetFocus(CWnd*);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
