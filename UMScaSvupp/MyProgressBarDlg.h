#pragma once

#include "Resource.h"
#include "XHTMLStatic.h"
#include "UMHmsMisc.h"

class CMyProgressBarDlg : public CDialog
{
	DECLARE_DYNAMIC(CMyProgressBarDlg)

private:
	CString			m_csCaption;
	CString			m_csText;
	CMyExtStatic2	m_wndLbl_Text;
	CMyExtStatic2	m_wndLbl_Text2;
	CProgressCtrl	m_ProgressBar;

public:


	CMyProgressBarDlg(CWnd* pParent = NULL);   // standard constructor
	CMyProgressBarDlg(CString cap,CString text,CWnd* pParent = NULL); 
	virtual ~CMyProgressBarDlg();
	CProgressCtrl& GetBar();
	void SetDlgTexts(CString Caption,CString Text);
	void UpdateText2(CString Text);

// Dialog Data
	enum { IDD = IDD_PROGRESSBAR_DLG };

protected:
	//{{AFX_VIRTUAL(CComfirmationDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	void OnInitialUpdate();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};