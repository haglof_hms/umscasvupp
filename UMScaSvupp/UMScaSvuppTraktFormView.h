#pragma once

#include "UMScaSvuppDB.h"
#include "Resource.h"
#include "UMHmsMisc.h"
#include "ScaGBTraktPages.h"

#include "DatePickerCombo.h"

// CMDIScaGBTraktFormView dialog


class CMDIScaGBTraktFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CMDIScaGBTraktFormView)

protected:		
	CMDIScaGBTraktFormView();   // standard constructor
	virtual ~CMDIScaGBTraktFormView();

	CMyTabControl2		m_wndTabControl;


	CMyExtStatic2 m_wndLbl_Datum;
	CMyExtStatic2 m_wndLbl_ObjNr;
	CMyExtStatic2 m_wndLbl_Ursprung;	
	CMyExtStatic2 m_wndLbl_Forvaltning;
	CMyExtStatic2 m_wndLbl_Distrik;
	CMyExtStatic2 m_wndLbl_Traktnamn;
	CMyExtStatic2 m_wndLbl_Areal;
	CMyExtStatic2 m_wndLbl_Inventerare;
	CMyExtStatic2 m_wndLbl_Entreprenor;
	CMyExtStatic2 m_wndLbl_Lag;
	CMyExtStatic2 m_wndLbl_MB_typ;
	CMyExtStatic2 m_wndLbl_Mal_PlHa;
	CMyExtStatic2 m_wndLbl_Anteckningar;
	CMyExtStatic2 m_wndLbl_InvTyp;

	CXTResizeGroupBox m_wndGroup;

	CMyExtEdit2 m_wndEdit_Datum;
	CMyExtEdit2 m_wndEdit_AtgardsId;	//m_wndEdit_ObjNr;
	CMyExtEdit2 m_wndEdit_Ursprung;	
	CMyExtEdit2 m_wndEdit_Forvaltning;
	CMyExtEdit2 m_wndEdit_Distrik;
	CMyExtEdit2 m_wndEdit_Traktnamn;
	CMyExtEdit2 m_wndEdit_Areal;
	CMyExtEdit2 m_wndEdit_Inventerare;
	CMyExtEdit2 m_wndEdit_Entreprenor;
	CMyExtEdit2 m_wndEdit_Lag;
	CMyExtEdit2 m_wndEdit_MB_typ;
	CMyExtEdit2 m_wndEdit_Mal_PlHa;
	CMyExtEdit2 m_wndEdit_Anteckningar;
	CMyExtEdit2 m_wndEdit_InvTyp;

	vecTraktIndex_Plant		m_vecTraktIndex;
	TRAKT_DATA_PLANT			m_recNewTrakt;
	TRAKT_DATA_PLANT			m_recActiveTrakt;
	TRAKT_INDEX_PLANT			m_structTraktIdentifer;
		
	enumACTION			m_enumAction;

	int					m_nDBIndex;

	CString				m_sLangFN;

	CMyProgressBarDlg ProgressDlg;

	BOOL					m_bConnected;
	DB_CONNECTION_DATA	m_dbConnectionData;

	void		SetKeyValuesToNegative(TRAKT_DATA_PLANT rec);

	void		setLanguage(void);
	void		getTraktsFromDB(void);

	void		showData();
	void		showTabData(TRAKT_DATA_PLANT data);
	void		populateData(UINT);
	void		setNavigationButtons(BOOL,BOOL);
	void		setNavigationButtonSave(BOOL on);
	void		setNavigationButtons2(void);
	
	void		getTraktIndexFromDB(void);
	void		LoadActiveTrakt(int idx);
	
	void		setKeysReadOnly();
	void		clearAll(void);
	
	/*CString	getType_setByUser(void);
	CString	getTypeName_setByUser(void);
	int		getOrigin_setByUser(void);
	CString	getOriginName_setByUser(void);*/

public:
// Dialog Data
	enum { IDD = IDD_FORMVIEW2 };


public:
	virtual void OnInitialUpdate();
	
	void doPopulate(UINT);
	void resetTrakt(enumRESET reset);
	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon);
	BOOL setupTraktTabs(void);
	void populateDataReport(UINT idx);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnClose();
	//afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()
public:
};


