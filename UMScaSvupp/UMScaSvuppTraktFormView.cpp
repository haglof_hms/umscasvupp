#include "stdafx.h"
#include "UMScaSvuppTraktFormView.h"
#include "UMScaSvuppFrames.h"
#include "ResLangFileReader.h"


IMPLEMENT_DYNCREATE(CMDIScaGBTraktFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CMDIScaGBTraktFormView, CXTResizeFormView)
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_WM_SHOWWINDOW()
//	ON_WM_CREATE()
END_MESSAGE_MAP()

CMDIScaGBTraktFormView::CMDIScaGBTraktFormView()
	: CXTResizeFormView(CMDIScaGBTraktFormView::IDD)
{
	m_bConnected = FALSE;
}

CMDIScaGBTraktFormView::~CMDIScaGBTraktFormView()
{
	ProgressDlg.DestroyWindow();
}

void CMDIScaGBTraktFormView::OnClose()
{
}


BOOL CMDIScaGBTraktFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CMDIScaGBTraktFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	

	DDX_Control(pDX, IDC_STATIC2_1, m_wndLbl_Datum);
	DDX_Control(pDX, IDC_STATIC2_2, m_wndLbl_ObjNr);
	DDX_Control(pDX, IDC_STATIC2_3, m_wndLbl_Ursprung);	
	DDX_Control(pDX, IDC_STATIC2_4, m_wndLbl_Forvaltning);
	DDX_Control(pDX, IDC_STATIC2_5, m_wndLbl_Distrik);
	DDX_Control(pDX, IDC_STATIC2_6, m_wndLbl_Traktnamn);
	DDX_Control(pDX, IDC_STATIC2_7, m_wndLbl_Areal);
	DDX_Control(pDX, IDC_STATIC2_8, m_wndLbl_Inventerare);
	DDX_Control(pDX, IDC_STATIC2_9, m_wndLbl_Entreprenor);
	DDX_Control(pDX, IDC_STATIC2_10, m_wndLbl_Lag);
	DDX_Control(pDX, IDC_STATIC2_11, m_wndLbl_MB_typ);
	DDX_Control(pDX, IDC_STATIC2_12, m_wndLbl_Mal_PlHa);
	DDX_Control(pDX, IDC_STATIC2_13, m_wndLbl_Anteckningar);
	DDX_Control(pDX, IDC_STATIC2_14, m_wndLbl_InvTyp);

	DDX_Control(pDX, IDC_GROUP2_1, m_wndGroup);

	DDX_Control(pDX, IDC_EDIT2_1, m_wndEdit_Datum);
	DDX_Control(pDX, IDC_EDIT2_2, m_wndEdit_AtgardsId);
	DDX_Control(pDX, IDC_EDIT2_3, m_wndEdit_Ursprung);	
	DDX_Control(pDX, IDC_EDIT2_4, m_wndEdit_Forvaltning);
	DDX_Control(pDX, IDC_EDIT2_5, m_wndEdit_Distrik);
	DDX_Control(pDX, IDC_EDIT2_6, m_wndEdit_Traktnamn);
	DDX_Control(pDX, IDC_EDIT2_7, m_wndEdit_Areal);
	DDX_Control(pDX, IDC_EDIT2_8, m_wndEdit_Inventerare);
	DDX_Control(pDX, IDC_EDIT2_9, m_wndEdit_Entreprenor);
	DDX_Control(pDX, IDC_EDIT2_10, m_wndEdit_Lag);
	DDX_Control(pDX, IDC_EDIT2_11, m_wndEdit_MB_typ);
	DDX_Control(pDX, IDC_EDIT2_12, m_wndEdit_Mal_PlHa);
	DDX_Control(pDX, IDC_EDIT2_13, m_wndEdit_Anteckningar);
	DDX_Control(pDX, IDC_EDIT2_14, m_wndEdit_InvTyp);

}


void CMDIScaGBTraktFormView::OnSize(UINT nType,int cx,int cy)
{
	CView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);
	if(m_wndTabControl.GetSafeHwnd())
		setResize(&m_wndTabControl,1,rect.top+330,rect.right - 2,rect.bottom-(rect.top+335));
}


/*int CMDIScaGBTraktFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;
	setupTraktTabs();
	return 0;
}*/

void CMDIScaGBTraktFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	::SetCursor(::LoadCursor(NULL,IDC_WAIT));

	SetScaleToFitSize(CSize(90, 1));

	m_wndEdit_Datum.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_AtgardsId.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Ursprung.SetEnabledColor(BLACK, WHITE );	
	m_wndEdit_Forvaltning.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Distrik.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Traktnamn.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Areal.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Inventerare.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Entreprenor.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Lag.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_MB_typ.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Mal_PlHa.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_Anteckningar.SetEnabledColor(BLACK, WHITE );
	m_wndEdit_InvTyp.SetEnabledColor(BLACK, WHITE );

	m_wndEdit_Datum.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_AtgardsId.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Ursprung.SetDisabledColor(BLACK, INFOBK );	
	m_wndEdit_Forvaltning.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Distrik.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Traktnamn.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Areal.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Inventerare.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Entreprenor.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Lag.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_MB_typ.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Mal_PlHa.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_Anteckningar.SetDisabledColor(BLACK, INFOBK );
	m_wndEdit_InvTyp.SetDisabledColor(BLACK, INFOBK );


	if (!ProgressDlg.Create(CMyProgressBarDlg::IDD,this))
	{
		TRACE0("Failed to create progress window control.\n");
	}

	//m_wndDatePicker.setDateInComboBox();
	//m_wndDatePicker2.setDateInComboBox();

	setKeysReadOnly();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	getTraktIndexFromDB();

	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);	
	setLanguage();
	m_nDBIndex = 0;
	setupTraktTabs();
	populateData(m_nDBIndex);	
	m_enumAction = NOTHING;
	
	RECT rect;
	GetClientRect(&rect);
	if(m_wndTabControl.GetSafeHwnd())
		setResize(&m_wndTabControl,1,rect.top+305,rect.right - 2,rect.bottom-(rect.top+310));

	::SetCursor(::LoadCursor(NULL,IDC_ARROW));
}

BOOL CMDIScaGBTraktFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CMDIScaGBTraktFormView::OnSetFocus(CWnd*)
{
	setNavigationButtons2();
	/*
	if (!m_vecTraktIndex.empty())
	{
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTraktIndex.size()-1));
	}
	else
	{
		setNavigationButtons(FALSE,FALSE);
	}*/
}

void CMDIScaGBTraktFormView::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			// Trakt information
			m_wndLbl_Datum.SetWindowText(xml->str(IDS_STRING_DATUM));
			m_wndLbl_ObjNr.SetWindowText(xml->str(IDS_STRING_ATGARDSID));
			m_wndLbl_Ursprung.SetWindowText(xml->str(IDS_STRING_URSPR));	
			m_wndLbl_Forvaltning.SetWindowText(xml->str(IDS_STRING_FORV));
			m_wndLbl_Distrik.SetWindowText(xml->str(IDS_STRING_DISTR));
			m_wndLbl_Traktnamn.SetWindowText(xml->str(IDS_STRING_NAMN));
			m_wndLbl_Areal.SetWindowText(xml->str(IDS_STRING_AREAL));
			m_wndLbl_Inventerare.SetWindowText(xml->str(IDS_STRING_INVENT));
			m_wndLbl_Entreprenor.SetWindowText(xml->str(IDS_STRING_ENTR));
			m_wndLbl_Lag.SetWindowText(xml->str(IDS_STRING_LAG));
			m_wndLbl_MB_typ.SetWindowText(xml->str(IDS_STRING_MBTYP));
			m_wndLbl_Mal_PlHa.SetWindowText(xml->str(IDS_STRING_MALPL));
			m_wndLbl_InvTyp.SetWindowText(xml->str(IDS_STRING_INVTYP));

		}
		delete xml;
	}
}

void CMDIScaGBTraktFormView::showData()
{
	int idx=-1;
	
	m_wndEdit_Datum.SetWindowText(m_recActiveTrakt.m_sTrakt_Datum);
	m_wndEdit_AtgardsId.SetWindowText(m_recActiveTrakt.m_sTrakt_AtgardsId);
	m_wndEdit_Ursprung.setInt(m_recActiveTrakt.m_nTrakt_Ursprung);
	m_wndEdit_Forvaltning.SetWindowText(m_recActiveTrakt.m_sTrakt_Forvaltning);
	m_wndEdit_Distrik.SetWindowText(m_recActiveTrakt.m_sTrakt_Distrikt);
	m_wndEdit_Traktnamn.SetWindowText(m_recActiveTrakt.m_sTrakt_Traktnamn);
	m_wndEdit_Areal.setFloat(m_recActiveTrakt.m_fTrakt_Areal,1);
	m_wndEdit_Inventerare.SetWindowText(m_recActiveTrakt.m_sTrakt_Inventerare);
	m_wndEdit_Entreprenor.SetWindowText(m_recActiveTrakt.m_sTrakt_Entreprenor);
	m_wndEdit_Lag.SetWindowText(m_recActiveTrakt.m_sTrakt_Lag);
	m_wndEdit_MB_typ.setInt(m_recActiveTrakt.m_nTrakt_MB_typ);
	m_wndEdit_Mal_PlHa.setInt(m_recActiveTrakt.m_nTrakt_Mal_PlHa);
	m_wndEdit_Anteckningar.SetWindowText(m_recActiveTrakt.m_sTrakt_Anteckningar);
	m_wndEdit_InvTyp.setInt(m_recActiveTrakt.m_nTrakt_InvTyp);
	
	showTabData(m_recActiveTrakt);
}


void CMDIScaGBTraktFormView::showTabData(TRAKT_DATA_PLANT data)
{
	int nTabPages = m_wndTabControl.getNumOfTabPages();

	for(int nPage = 0; nPage<nTabPages; nPage++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(nPage);
		if(pItem)
		{
			switch(nPage)
			{
			case 0:
				CMyReportPage1 *wndReport1;
				wndReport1 = DYNAMIC_DOWNCAST(CMyReportPage1,CWnd::FromHandle(pItem->GetHandle()));
				wndReport1->showData(data);
				break;
			case 1:
				CMyReportPage2 *wndReport2;
				wndReport2 = DYNAMIC_DOWNCAST(CMyReportPage2,CWnd::FromHandle(pItem->GetHandle()));
				wndReport2->showData(data);
				break;
			}			
		}
	}
}



void CMDIScaGBTraktFormView::OnShowWindow(BOOL bShow, UINT nStatus)
{
	
	/*
	CString tst=_T(""),tst2=_T(""),tst3=_T("");
	int idx=m_nDBIndex;
	RLFReader *xml = new RLFReader;
	CMDIScaGBTraktFrame *pView = (CMDIScaGBTraktFrame *)getFormViewByID(IDD_FORMVIEW2)->GetParent();
	if (xml->Load(m_sLangFN))
		tst3=_T(xml->str(IDS_STRING752));
	tst+=tst3;
	
	if (!m_vecTraktIndex.empty() && 
		  idx >= 0 && 
			idx < m_vecTraktIndex.size())
	{
		if (pView)
		{
			CDocument *pDoc = pView->GetActiveDocument();
			if (pDoc != NULL)
			{
				tst2.Format(" (%d/%d)",m_nDBIndex+1,m_vecTraktIndex.size());
				tst+=tst2;
				pDoc->SetTitle(tst);			
			}
		}
	}
	else
	{
		if (pView)
		{
			CDocument *pDoc = pView->GetActiveDocument();
			if (pDoc != NULL)
			{
				tst2.Format(" (%d/%d)",0,0);
				tst+=tst2;
				pDoc->SetTitle(tst);						
			}
		}
	}*/
	CXTResizeFormView::OnShowWindow(bShow,nStatus);
}

void CMDIScaGBTraktFormView::populateData(UINT idx)
{
	CString tst=_T(""),tst2=_T(""),tst3=_T("");
	RLFReader *xml = new RLFReader;
	CMDIScaGBTraktFrame *pView = (CMDIScaGBTraktFrame *)getFormViewByID(IDD_FORMVIEW2)->GetParent();

	if (xml->Load(m_sLangFN))
		tst3=xml->str(IDS_STRING8519);
	
	if (!m_vecTraktIndex.empty() && 
		  idx >= 0 && 
			idx < m_vecTraktIndex.size())
	{
	//m_recActiveTrakt = m_vecTraktData[idx];
		LoadActiveTrakt(idx);
		showData();
		if (pView)
		{
			CXTResizeGroupBox * grp= (CXTResizeGroupBox *)GetDlgItem(IDC_GROUP2_1);
   		tst2.Format(_T(" (%d/%d)"),m_nDBIndex+1,m_vecTraktIndex.size());
			tst.Format(_T("%s%s"),tst3,tst2);
			grp->SetWindowText(tst);
			/*CDocument *pDoc = pView->GetActiveDocument();
			if (pDoc != NULL)
			{
				CXTResizeGroupBox * grp= GetDlgItem(IDD_FORMVIEW2,IDC_GROUP2_1);
				tst2.Format(_T(" (%d/%d)"),m_nDBIndex+1,m_vecTraktIndex.size());
				tst.Format(_T("%s%s"),tst3,tst2);
				pDoc->SetTitle(tst);		
			}*/
		}
	}
	else
	{
		// This method set editboxes read or read only, depending on 
		// if there's Compartments and Plots; 061010 p�d
		setKeysReadOnly();
		clearAll();
		if (pView)
		{
			CXTResizeGroupBox * grp= (CXTResizeGroupBox *)GetDlgItem(IDC_GROUP2_1);
			grp->SetWindowText(_T("0/0"));
			/*CDocument *pDoc = pView->GetActiveDocument();
			if (pDoc != NULL)
			{
				tst2.Format(_T(" (%d/%d)"),0,0);
				tst+=tst2;
				pDoc->SetTitle(tst);						
			}*/
		}
	}
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 061010 p�d
LRESULT CMDIScaGBTraktFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{

	switch(wParam)
	{
		// Messages from HMSShell
		case ID_UPDATE_ITEM :
		{
			resetTrakt(RESET_TO_LAST_SET_NB);
			break;
		}	// case ID_NEW_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			if (m_nDBIndex != 0)
			{
				m_nDBIndex = 0;
				setNavigationButtons2();
				//setNavigationButtons(FALSE,TRUE);
				populateData(m_nDBIndex);
				setKeysReadOnly();
			}
			break;
		}
		case ID_DBNAVIG_PREV :
		{
			if (m_nDBIndex != 0)
			{
				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;
				setNavigationButtons2();
				populateData(m_nDBIndex);
				setKeysReadOnly();
			}
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			if (m_nDBIndex != ((int)m_vecTraktIndex.size() - 1))
			{
				m_nDBIndex++;
				if (m_nDBIndex > ((int)m_vecTraktIndex.size() - 1))
					m_nDBIndex = (int)m_vecTraktIndex.size() - 1;
				setNavigationButtons2();
				populateData(m_nDBIndex);
    			setKeysReadOnly();

			}
			break;
		}
		case ID_DBNAVIG_END :
		{
			if (m_nDBIndex != ((int)m_vecTraktIndex.size() - 1))
			{
				m_nDBIndex = (int)m_vecTraktIndex.size()-1;
				setNavigationButtons2();
				//setNavigationButtons(TRUE,FALSE);	
				populateData(m_nDBIndex);
				setKeysReadOnly();
			}
			break;
		}	// case ID_NEW_ITEM :
	};
	return 0L;
}

void CMDIScaGBTraktFormView::getTraktIndexFromDB(void)
{
	if (m_bConnected)
	{
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getTraktIndex(m_vecTraktIndex);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}

void CMDIScaGBTraktFormView::LoadActiveTrakt(int idx)
{
TRAKT_INDEX_PLANT trkt_idx=m_vecTraktIndex[idx];
	if (m_bConnected)
	{
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getTrakt(trkt_idx.m_nTrakt_Id,m_recActiveTrakt);
			pDB->getPlots(m_recActiveTrakt);
			
			m_structTraktIdentifer.m_nTrakt_Id	= m_recActiveTrakt.m_nTrakt_Id;
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}






// PUBLIC


// Method used on createing a new Trakt manually.
// Clears ALL fileds for entering data. OBS! Region/Distrikt is entered
// using a Dialog; 061011 p�d
void CMDIScaGBTraktFormView::clearAll(void)
{
	m_wndEdit_Datum.SetWindowText(_T(""));
	m_wndEdit_AtgardsId.SetWindowText(_T(""));
	m_wndEdit_Ursprung.SetWindowText(_T(""));	
	m_wndEdit_Forvaltning.SetWindowText(_T(""));
	m_wndEdit_Distrik.SetWindowText(_T(""));
	m_wndEdit_Traktnamn.SetWindowText(_T(""));
	m_wndEdit_Areal.SetWindowText(_T(""));
	m_wndEdit_Inventerare.SetWindowText(_T(""));
	m_wndEdit_Entreprenor.SetWindowText(_T(""));
	m_wndEdit_Lag.SetWindowText(_T(""));
	m_wndEdit_MB_typ.SetWindowText(_T(""));
	m_wndEdit_Mal_PlHa.SetWindowText(_T(""));
	m_wndEdit_Anteckningar.SetWindowText(_T(""));
	m_wndEdit_InvTyp.SetWindowText(_T(""));

	TRAKT_DATA_PLANT data = _trakt_data_plant();
	showTabData(data);
	SetKeyValuesToNegative(m_recNewTrakt);
}

void CMDIScaGBTraktFormView::SetKeyValuesToNegative(TRAKT_DATA_PLANT rec)
{
	rec.m_nTrakt_Id=MY_NULL;
}

void CMDIScaGBTraktFormView::setKeysReadOnly()
{
	m_wndEdit_Datum.SetReadOnly(TRUE);
	m_wndEdit_AtgardsId.SetReadOnly(TRUE);
	m_wndEdit_Ursprung.SetReadOnly(TRUE);	
	m_wndEdit_Forvaltning.SetReadOnly(TRUE);
	m_wndEdit_Distrik.SetReadOnly(TRUE);
	m_wndEdit_Traktnamn.SetReadOnly(TRUE);
	m_wndEdit_Areal.SetReadOnly(TRUE);
	m_wndEdit_Inventerare.SetReadOnly(TRUE);
	m_wndEdit_Entreprenor.SetReadOnly(TRUE);
	m_wndEdit_Lag.SetReadOnly(TRUE);
	m_wndEdit_MB_typ.SetReadOnly(TRUE);
	m_wndEdit_Mal_PlHa.SetReadOnly(TRUE);
	m_wndEdit_Anteckningar.SetReadOnly(TRUE);
	m_wndEdit_InvTyp.SetReadOnly(TRUE);
}



void CMDIScaGBTraktFormView::setNavigationButtons2(void)
{
	BOOL start=FALSE,end=FALSE;
	if (!m_vecTraktIndex.empty())
	{
		if(m_nDBIndex > 0)
			start=TRUE;

		if(m_nDBIndex < ((int)m_vecTraktIndex.size()-1))
			end=TRUE;
	}
	setNavigationButtons(start,end);	
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CMDIScaGBTraktFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
}

void CMDIScaGBTraktFormView::setNavigationButtonSave(BOOL on)
{
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,on);
}

void CMDIScaGBTraktFormView::doPopulate(UINT index)
{
	m_nDBIndex = index;
	populateData(m_nDBIndex);
	setNavigationButtons2();
	//setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTraktIndex.size()-1));
}

// Reset and get data from database; 061010 p�d
void CMDIScaGBTraktFormView::resetTrakt(enumRESET reset)
{
	// Get updated data from Database
	getTraktIndexFromDB();
	// Check that there's any data in m_vecMachineData vector; 061010 p�d
	if (m_vecTraktIndex.size() > 0)
	{
		if (reset == RESET_TO_FIRST_SET_NB)
		{
			// Reset to last item in m_vecTraktIndex
			m_nDBIndex = 0;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons2();
			//setNavigationButtons( m_nDBIndex > 0, m_nDBIndex < (m_vecTraktIndex.size()-1));
		}
		if (reset == RESET_TO_FIRST_NO_NB)
		{
			// Reset to last item in m_vecTraktIndex
			m_nDBIndex = 0;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons2();
			//setNavigationButtons(FALSE,FALSE);
		}
		if (reset == RESET_TO_LAST_SET_NB)
		{
			// Reset to last item in m_vecTraktIndex
			m_nDBIndex = (int)m_vecTraktIndex.size()-1;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons2();
			//setNavigationButtons( m_nDBIndex > 0, m_nDBIndex < (m_vecTraktIndex.size()-1));
		}
		if (reset == RESET_TO_LAST_NO_NB)
		{
			// Reset to last item in m_vecTraktIndex
			m_nDBIndex = (int)m_vecTraktIndex.size()-1;
			populateData(m_nDBIndex);
			// Set toolbar on HMSShell
			setNavigationButtons2();
			//setNavigationButtons(FALSE,FALSE);
		}
		if (reset == RESET_TO_JUST_ENTERED_SET_NB || reset == RESET_TO_JUST_ENTERED_NO_NB)
		{

			// Find itemindex for last entry, from file, and set on populateData; 061207 p�d
			for (UINT i = 0;i < m_vecTraktIndex.size();i++)
			{
				//TRAKT_INDEX data = m_vecTraktIndex[i];
				if (m_vecTraktIndex[i].m_nTrakt_Id == m_structTraktIdentifer.m_nTrakt_Id)
				{
					// Reset to last item in m_vecTraktData
					m_nDBIndex = i;
					break;
				}	// if (data.m_nRegionID == m_structTraktIdentifer.nRegionID &&
			}	// for (UINT i = 0;i < m_vecTraktData.size();i++)
			// Populate data on User interface
			populateData(m_nDBIndex);
			if (reset == RESET_TO_JUST_ENTERED_SET_NB)
			{
				setNavigationButtons2();
				//setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecTraktIndex.size()-1));
			}
			else if (reset == RESET_TO_JUST_ENTERED_NO_NB)
			{
				setNavigationButtons2();
				//setNavigationButtons(FALSE,FALSE);
			}
		}	// if (reset == RESET_TO_JUST_ENTERED_SET_NB)
	}	// if (m_vecTraktData.size() > 0)
	else 
	{
		setNavigationButtons2();
		//setNavigationButtons(FALSE,FALSE);
		populateData(-1);	// Clear data
	}
}


BOOL CMDIScaGBTraktFormView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;
	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
				 rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
				 TRACE0( "Warning: couldn't create client tab for view.\n" );
				 // pWnd will be cleaned up by PostNcDestroy
				 return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);
	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);
	return TRUE;
}



BOOL CMDIScaGBTraktFormView::setupTraktTabs(void)
{
	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP,CRect(0,0,0,0),this, IDC_TABCONTROL_1);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->m_bBoldSelected = FALSE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);
	//m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS,NULL,0,CSize(16, 16),xtpImageNormal);
	
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			// Create tabpages
			AddView(RUNTIME_CLASS(CMyReportPage1),xml->str(IDS_STRING_TRAKT),8);	// Trakt
			AddView(RUNTIME_CLASS(CMyReportPage2),xml->str(IDS_STRING_YTOR),8);	// Ytor
		}
		delete xml;
	}
	return TRUE;
}



