#include "stdafx.h"
#include "UMScaSvuppDB.h"
#include "UMScaSvuppUrvalRoj.h"
#include "ResLangFileReader.h"
//#include "UMScaSvuppTraktFormView.h"
#include "UMScaSvuppFrames.h"
#include "HXL.h"


IMPLEMENT_DYNCREATE(CUMScaSvuppUrvalRoj, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CUMScaSvuppUrvalRoj, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_NOTIFY(NM_CLICK, IDC_TRAKT_REPORT_5, OnReportItemDblClick)

	ON_NOTIFY(NM_CLICK, IDC_5_1_REPORT, OnReportUrvalClick)		
	ON_NOTIFY(NM_CLICK, IDC_5_2_REPORT, OnReportUrvalClick)
	ON_NOTIFY(NM_CLICK, IDC_5_3_REPORT, OnReportUrvalClick)
	ON_NOTIFY(NM_CLICK, IDC_5_4_REPORT, OnReportUrvalClick)
	ON_NOTIFY(NM_CLICK, IDC_5_5_REPORT, OnReportUrvalClick)
	ON_NOTIFY(NM_CLICK, IDC_5_6_REPORT, OnReportUrvalClick)
	ON_NOTIFY(NM_CLICK, IDC_5_7_REPORT, OnReportUrvalClick)

	ON_BN_CLICKED(IDC_BUTTON5_1, OnBnCreateFiles)

	ON_NOTIFY(DTN_CLOSEUP, IDC_DATETIMEPICKER5_1, OnChangeDatetimepicker5_1)
	ON_NOTIFY(DTN_CLOSEUP, IDC_DATETIMEPICKER5_2, OnChangeDatetimepicker5_2)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DATETIMEPICKER5_1, OnChangeDatetimepicker5_1)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DATETIMEPICKER5_2, OnChangeDatetimepicker5_2)
	ON_NOTIFY(DTN_DROPDOWN, IDC_DATETIMEPICKER5_1, OnChangeDatetimepicker5_1)
	ON_NOTIFY(DTN_DROPDOWN, IDC_DATETIMEPICKER5_2, OnChangeDatetimepicker5_2)

	ON_NOTIFY(DTN_USERSTRING, IDC_DATETIMEPICKER5_1, OnChangeDatetimepicker5_1)
	ON_NOTIFY(DTN_USERSTRING, IDC_DATETIMEPICKER5_2, OnChangeDatetimepicker5_2)
	ON_MESSAGE(MSG_IN_SUITE, OnArealChange)
	ON_EN_CHANGE(IDC_EDIT5_1,OnArealFromChange)
	ON_EN_CHANGE(IDC_EDIT5_2,OnArealToChange)

	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CUMScaSvuppUrvalRoj::CUMScaSvuppUrvalRoj()
	: CXTResizeFormView(CUMScaSvuppUrvalRoj::IDD)
{
}

CUMScaSvuppUrvalRoj::~CUMScaSvuppUrvalRoj()
{
	CMyReportCtrl2 *rep;
	unsigned int i=0;
	for(i=0;i<vecReportControls.size();i++)
	{
	rep=vecReportControls[i];
	delete rep;
	}
}

void CUMScaSvuppUrvalRoj::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_STATIC5_2, m_wndLbl_1);
	DDX_Control(pDX, IDC_STATIC5_3, m_wndLbl_2);
	DDX_Control(pDX, IDC_STATIC5_4, m_wndLbl_3);
	DDX_Control(pDX, IDC_STATIC5_5, m_wndLbl_Datum);
	DDX_Control(pDX, IDC_STATIC5_5_2, m_wndLbl_Datum_From);
	DDX_Control(pDX, IDC_STATIC5_5_3, m_wndLbl_Datum_To);
	DDX_Control(pDX, IDC_STATIC5_6, m_wndLbl_4);
	DDX_Control(pDX, IDC_STATIC5_7, m_wndLbl_5);
	DDX_Control(pDX, IDC_STATIC5_8, m_wndLbl_6);
	DDX_Control(pDX, IDC_STATIC5_9, m_wndLbl_7);
	DDX_Control(pDX, IDC_STATIC5_10, m_wndLbl_8);
	DDX_Control(pDX, IDC_STATIC5_12, m_wndLbl_9);

	DDX_Control(pDX, IDC_STATIC5_11, m_wndLbl_Areal);
	DDX_Control(pDX, IDC_STATIC5_11_2, m_wndLbl_Areal_From);
	DDX_Control(pDX, IDC_STATIC5_11_3, m_wndLbl_Areal_To);

	DDX_Control(pDX, IDC_DATETIMEPICKER5_1, m_wndDateTimeCtrlFrom);
	DDX_Control(pDX, IDC_DATETIMEPICKER5_2, m_wndDateTimeCtrlTo);
	
	DDX_Control(pDX, IDC_EDIT5_1	, m_wndEdit_Areal_From);
	DDX_Control(pDX, IDC_EDIT5_2	, m_wndEdit_Areal_To);
	
	DDX_Control(pDX, IDC_BUTTON5_1, m_wndMakeFiles);

}

BOOL CUMScaSvuppUrvalRoj::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

BOOL CUMScaSvuppUrvalRoj::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;
	return TRUE;
}

void CUMScaSvuppUrvalRoj::OnSize(UINT nType,int cx,int cy)
{
	int i=0;
	CXTResizeFormView::OnSize(nType, cx, cy);
	//K�r bara detta om initieringen redan �r gjord
	if(m_wndTraktReport.GetSafeHwnd()!=NULL)
	{
		setResize(&m_wndTraktReport
			,X_REPORT
			,Y_REPORT
			,cx - W_REPORT
			,cy-Y_REPORT-20);
		/*
		setsizes(-1);
		for(i=0;i<m_nNumOfUrval;i++)
			setsizes(i);*/
	}
}

void CUMScaSvuppUrvalRoj::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

void CUMScaSvuppUrvalRoj::OnInitialUpdate()
{		
	CXTResizeFormView::OnInitialUpdate();
	m_nInit_Done=0;
	Init();
	//SetScaleToFitSize(CSize(90, 1));
}

void CUMScaSvuppUrvalRoj::Init()
{
	int i=0;
	//Inventeringsspecifika settings
	if(m_nInit_Done)
		return;
	else
		m_nInit_Done=1;

	m_nNumOfUrval=NUM_OF_ROJ_URVAL;
	m_nNumOfRepColumns=NUM_OF_REP_COL_HEADER_ROJ;

	for(i=0;i<m_nNumOfUrval;i++)
		m_nOrder_Urval[i]=0;
	
	CBitmap bBitmap;

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
	m_sLangAbbrev = getLangSet();
	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sLangAbbrev,LANGUAGE_FN_EXT);
	setLanguage();
	//getTraktsFromDB();


	m_nIdc[0]=IDC_5_1_REPORT;
	m_nIdc[1]=IDC_5_2_REPORT;
	m_nIdc[2]=IDC_5_3_REPORT;
	m_nIdc[3]=IDC_5_4_REPORT;
	m_nIdc[4]=IDC_5_5_REPORT;
	m_nIdc[5]=IDC_5_6_REPORT;
	m_nIdc[6]=IDC_5_7_REPORT;

	m_nColHeaderWidth[0]=15;
	m_nColHeaderWidth[1]=15;
	m_nColHeaderWidth[2]=30;
	m_nColHeaderWidth[3]=15;
	m_nColHeaderWidth[4]=30;
	m_nColHeaderWidth[5]=30;
	m_nColHeaderWidth[6]=30;

	Xml_StringId_Rep_Columns[0]=IDS_STRING_FORV;
	Xml_StringId_Rep_Columns[1]=IDS_STRING_DISTR;
	Xml_StringId_Rep_Columns[2]=IDS_STRING_URSPR;
	Xml_StringId_Rep_Columns[3]=IDS_STRING_ATGARDSID;
	Xml_StringId_Rep_Columns[4]=IDS_STRING_NAMN;
	Xml_StringId_Rep_Columns[5]=IDS_STRING_AREAL;
	Xml_StringId_Rep_Columns[6]=IDS_STRING_ENTR;
	Xml_StringId_Rep_Columns[7]=IDS_STRING_LAG;
	Xml_StringId_Rep_Columns[8]=IDS_STRING_DATUM;
	Xml_StringId_Rep_Columns[9]=IDS_STRING_INVENT;
	Xml_StringId_Rep_Columns[10]=IDS_STRING_SI;

	m_nWinPos[0][0]=X_REPORT_1;
	m_nWinPos[0][1]=Y_REPORT_1;
	m_nWinPos[0][2]=W_REPORT_1;
	m_nWinPos[0][3]=H_REPORT_1;

	m_nWinPos[1][0]=X_REPORT_2;
	m_nWinPos[1][1]=Y_REPORT_2;
	m_nWinPos[1][2]=W_REPORT_2;
	m_nWinPos[1][3]=H_REPORT_2;

	m_nWinPos[2][0]=X_REPORT_3;
	m_nWinPos[2][1]=Y_REPORT_3;
	m_nWinPos[2][2]=W_REPORT_3;
	m_nWinPos[2][3]=H_REPORT_3;

	m_nWinPos[3][0]=X_REPORT_4;
	m_nWinPos[3][1]=Y_REPORT_4;
	m_nWinPos[3][2]=W_REPORT_4;
	m_nWinPos[3][3]=H_REPORT_4;

	m_nWinPos[4][0]=X_REPORT_5;
	m_nWinPos[4][1]=Y_REPORT_5;
	m_nWinPos[4][2]=W_REPORT_5;
	m_nWinPos[4][3]=H_REPORT_5;

	m_nWinPos[5][0]=X_REPORT_6;
	m_nWinPos[5][1]=Y_REPORT_6;
	m_nWinPos[5][2]=W_REPORT_6;
	m_nWinPos[5][3]=H_REPORT_6;

	m_nWinPos[6][0]=X_REPORT_7;
	m_nWinPos[6][1]=Y_REPORT_7;
	m_nWinPos[6][2]=W_REPORT_7;
	m_nWinPos[6][3]=H_REPORT_7;

	m_nWinPos[7][0]=X_REPORT_8;
	m_nWinPos[7][1]=Y_REPORT_8;
	m_nWinPos[7][2]=W_REPORT_8;
	m_nWinPos[7][3]=H_REPORT_8;

	m_nWinPos[8][0]=X_REPORT_9;
	m_nWinPos[8][1]=Y_REPORT_9;
	m_nWinPos[8][2]=W_REPORT_9;
	m_nWinPos[8][3]=H_REPORT_9;
	
	
	pImageList.Add(::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_ICON2)));	
	pImageList.Add(::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_ICON3)));

	m_wndDateTimeCtrlFrom.SetFormat(_T(" "));
	SYSTEMTIME sysTime;
	memset(&sysTime, 0, sizeof(sysTime));
	m_wndDateTimeCtrlFrom.SetTime(sysTime);
	m_wndDateTimeCtrlTo.SetFormat(_T(" "));
	m_wndDateTimeCtrlTo.SetTime(sysTime);
	
	m_wndDateTimeCtrlFrom.SetWindowText(_T(""));
	m_wndDateTimeCtrlTo.SetWindowText(_T(""));

	sDateFromSql=_T("");
	sDateToSql=_T("");
	nCheckedFrom=1;
	nCheckedTo=1;

	m_wndEdit_Areal_From.SetId(1,this->GetSafeHwnd());
	m_wndEdit_Areal_From.SetAsNumeric();
	m_wndEdit_Areal_To.SetId(2,this->GetSafeHwnd());
	m_wndEdit_Areal_To.SetAsNumeric();
	
	for(i=0;i<m_nNumOfUrval;i++)
		setupReport_Urval(i);
	
	setupReport();

	setsizes(-1);

	for(i=0;i<m_nNumOfUrval;i++)
		setsizes(i);
	
	Populate_TraktReport();
}

void CUMScaSvuppUrvalRoj::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_wnd_caption=xml->str(IDS_STRING_URVAL_ROJ);
			// Trakt information
			Xml_StringId_Urval[0]=IDS_STRING_FORV;
			Xml_StringId_Urval[1]=IDS_STRING_DISTR;
			Xml_StringId_Urval[2]=IDS_STRING_URSPR;
			Xml_StringId_Urval[3]=IDS_STRING_ATGARDSID;
			Xml_StringId_Urval[4]=IDS_STRING_ENTR;
			Xml_StringId_Urval[5]=IDS_STRING_LAG;
			Xml_StringId_Urval[6]=IDS_STRING_INVENT;

			m_wndLbl_1.SetWindowText(xml->str(Xml_StringId_Urval[0]));
			m_wndLbl_2.SetWindowText(xml->str(Xml_StringId_Urval[1]));
			m_wndLbl_3.SetWindowText(xml->str(Xml_StringId_Urval[2]));			
			m_wndLbl_4.SetWindowText(xml->str(Xml_StringId_Urval[3]));
			m_wndLbl_5.SetWindowText(xml->str(Xml_StringId_Urval[4]));
			m_wndLbl_6.SetWindowText(xml->str(Xml_StringId_Urval[5]));
			m_wndLbl_7.SetWindowText(xml->str(Xml_StringId_Urval[6]));
			m_wndLbl_8.ShowWindow(SW_HIDE);
			m_wndLbl_9.ShowWindow(SW_HIDE);

			m_wndLbl_Datum.SetWindowText(xml->str(IDS_STRING_DATUM));
			m_wndLbl_Datum_From.SetWindowText(xml->str(IDS_STRING8580));
			m_wndLbl_Datum_To.SetWindowText(xml->str(IDS_STRING8581));

			m_wndLbl_Areal.SetWindowText(xml->str(IDS_STRING_AREAL));
			m_wndLbl_Areal_From.SetWindowText(xml->str(IDS_STRING8580));
			m_wndLbl_Areal_To.SetWindowText(xml->str(IDS_STRING8581));

			m_sDBVarNames.Add(_T("Forvaltning"));
			m_sDBVarNames.Add(_T("Distrikt"));
			m_sDBVarNames.Add(_T("Ursprung"));
			m_sDBVarNames.Add(_T("AtgardsId"));
			m_sDBVarNames.Add(_T("Entreprenor"));
			m_sDBVarNames.Add(_T("Lag"));
			m_sDBVarNames.Add(_T("Inventerare"));
	

		}
		delete xml;
	}
	SetWindowText(m_wnd_caption);
}


//S�tter placering av urvalsvariabelf�nster samt huvudf�nster
void CUMScaSvuppUrvalRoj::setsizes(int var)
{
	RECT rect;
	GetClientRect(&rect);
	CMyReportCtrl2	*rep;
	int nLblWidth=18,nLblYDist=20;

	switch(var)
	{
	case -1:
		rep=&m_wndTraktReport;
		break;
	default:
		if(vecReportControls.size()>var)
			rep=vecReportControls[var];
		break;
	}

	//G�r detta bara en g�ng
	if(var==0)
	{
		m_wndMakeFiles.MoveWindow(rect.top+X_REPORT_PLANT_FILES,Y_REPORT_PLANT_FILES,W_REPORT_PLANT_FILES,H_REPORT_PLANT_FILES,true);

		m_wndLbl_Areal.MoveWindow(rect.top+X_REPORT_PLANT_AREAL,Y_REPORT_PLANT_AREAL,W_REPORT_PLANT_AREAL,H_REPORT_PLANT_AREAL,true);
		m_wndLbl_Areal_From.MoveWindow(rect.top+X_REPORT_PLANT_AREAL_FROM,Y_REPORT_PLANT_AREAL_FROM,W_REPORT_PLANT_AREAL_FROM,H_REPORT_PLANT_AREAL_FROM,true);
		m_wndLbl_Areal_To.MoveWindow(rect.top+X_REPORT_PLANT_AREAL_TO,Y_REPORT_PLANT_AREAL_TO,W_REPORT_PLANT_AREAL_TO,H_REPORT_PLANT_AREAL_TO,true);

		m_wndEdit_Areal_From.MoveWindow(rect.top+X_REPORT_PLANT_AREAL_EDIT_FROM,Y_REPORT_PLANT_AREAL_EDIT_FROM,W_REPORT_PLANT_AREAL_EDIT_FROM,H_REPORT_PLANT_AREAL_EDIT_FROM,true);
		m_wndEdit_Areal_To.MoveWindow(rect.top+X_REPORT_PLANT_AREAL_EDIT_TO,Y_REPORT_PLANT_AREAL_EDIT_TO,W_REPORT_PLANT_AREAL_EDIT_TO,H_REPORT_PLANT_AREAL_EDIT_TO,true);

		m_wndLbl_Datum.MoveWindow(rect.top+X_REPORT_PLANT_DATUM,Y_REPORT_PLANT_DATUM,W_REPORT_PLANT_DATUM,nLblWidth,true);
		m_wndLbl_Datum_From.MoveWindow(rect.top+X_REPORT_PLANT_DATUM_FROM,Y_REPORT_PLANT_DATUM_FROM,W_REPORT_PLANT_DATUM_FROM,H_REPORT_PLANT_DATUM_FROM,true);
		m_wndLbl_Datum_To.MoveWindow(rect.top+X_REPORT_PLANT_DATUM_TO,Y_REPORT_PLANT_DATUM_TO,W_REPORT_PLANT_DATUM_TO,H_REPORT_PLANT_DATUM_TO,true);

		m_wndDateTimeCtrlFrom.MoveWindow(rect.top+X_REPORT_PLANT_DATUM_EDIT_FROM,Y_REPORT_PLANT_DATUM_EDIT_FROM,W_REPORT_PLANT_DATUM_EDIT_FROM,H_REPORT_PLANT_DATUM_EDIT_FROM,true);
		m_wndDateTimeCtrlTo.MoveWindow(rect.top+X_REPORT_PLANT_DATUM_EDIT_TO,Y_REPORT_PLANT_DATUM_EDIT_TO,W_REPORT_PLANT_DATUM_EDIT_TO,H_REPORT_PLANT_DATUM_EDIT_TO,true);
	
		m_wndLbl_1.MoveWindow(rect.top+m_nWinPos[0][0],m_nWinPos[0][1]-18,m_nWinPos[0][2],15);
		m_wndLbl_2.MoveWindow(rect.top+m_nWinPos[1][0],m_nWinPos[1][1]-18,m_nWinPos[1][2],15);
		m_wndLbl_3.MoveWindow(rect.top+m_nWinPos[2][0],m_nWinPos[2][1]-18,m_nWinPos[2][2],15);
		m_wndLbl_4.MoveWindow(rect.top+m_nWinPos[3][0],m_nWinPos[3][1]-18,m_nWinPos[3][2],15);
		m_wndLbl_5.MoveWindow(rect.top+m_nWinPos[4][0],m_nWinPos[4][1]-18,m_nWinPos[4][2],15);
		m_wndLbl_6.MoveWindow(rect.top+m_nWinPos[5][0],m_nWinPos[5][1]-18,m_nWinPos[5][2],15);
		m_wndLbl_7.MoveWindow(rect.top+m_nWinPos[6][0],m_nWinPos[6][1]-18,m_nWinPos[6][2],15);

		m_wndLbl_Areal.SetLblFont(14,FW_BOLD);
		m_wndLbl_Datum.SetLblFont(14,FW_BOLD);
		m_wndLbl_1.SetLblFont(14,FW_BOLD);
		m_wndLbl_2.SetLblFont(14,FW_BOLD);
		m_wndLbl_3.SetLblFont(14,FW_BOLD);
		m_wndLbl_4.SetLblFont(14,FW_BOLD);
		m_wndLbl_5.SetLblFont(14,FW_BOLD);
		m_wndLbl_6.SetLblFont(14,FW_BOLD);
		m_wndLbl_7.SetLblFont(14,FW_BOLD);
	}
	if (rep->GetSafeHwnd() != NULL)
	{
		switch(var)
		{
		case -1:
			setResize(rep
				,X_REPORT
				,rect.top+Y_REPORT
				,rect.right - W_REPORT
				,rect.bottom-Y_REPORT-20);
			//rep->EnableScrollBar(SB_HORZ,FALSE);
			//rep->EnableScrollBar(SB_VERT,TRUE);
			rep->GetReportHeader()->SetAutoColumnSizing(FALSE);
			break;		
			/*
		case 0:
			m_wndLbl_1.MoveWindow(rect.top+m_nWinPos[var][0],m_nWinPos[var][1]-nLblYDist,m_nWinPos[var][2],nLblWidth,true);
			break;
		case 1:
			m_wndLbl_2.MoveWindow(rect.top+m_nWinPos[var][0],m_nWinPos[var][1]-nLblYDist,m_nWinPos[var][2],nLblWidth,true);
			break;
		case 2:
			m_wndLbl_3.MoveWindow(rect.top+m_nWinPos[var][0],m_nWinPos[var][1]-nLblYDist,m_nWinPos[var][2],nLblWidth,true);
			break;
		case 3:
			m_wndLbl_4.MoveWindow(rect.top+m_nWinPos[var][0],m_nWinPos[var][1]-nLblYDist,m_nWinPos[var][2],nLblWidth,true);
			break;
		case 4:
			m_wndLbl_5.MoveWindow(rect.top+m_nWinPos[var][0],m_nWinPos[var][1]-nLblYDist,m_nWinPos[var][2],nLblWidth,true);
			break;
		case 5:
			m_wndLbl_6.MoveWindow(rect.top+m_nWinPos[var][0],m_nWinPos[var][1]-nLblYDist,m_nWinPos[var][2],nLblWidth,true);
			break;
		case 6:
			m_wndLbl_7.MoveWindow(rect.top+m_nWinPos[var][0],m_nWinPos[var][1]-nLblYDist,m_nWinPos[var][2],nLblWidth,true);
			break;
		case 7:
			m_wndLbl_8.MoveWindow(rect.top+m_nWinPos[var][0],m_nWinPos[var][1]-nLblYDist,m_nWinPos[var][2],nLblWidth,true);
			break;
		case 8:
			m_wndLbl_9.MoveWindow(rect.top+m_nWinPos[var][0],m_nWinPos[var][1]-nLblYDist,m_nWinPos[var][2],nLblWidth,true);			
			break;*/
		default:
			if(var>=0)
			{
				setResize(rep,rect.top+m_nWinPos[var][0],m_nWinPos[var][1],m_nWinPos[var][2],m_nWinPos[var][3]);
			}
			break;
		}
	}
}



//Initerarar urvalsrapportf�nster med kolumnnamn
BOOL CUMScaSvuppUrvalRoj::setupReport_Urval(int var)
{
	int nNumOfTabs = 0,col=0,nAddRep=0;
	CXTPReportColumn *pCol = NULL;
	CMyReportCtrl2 *rep=new CMyReportCtrl2;
	
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			if (rep->GetSafeHwnd() == 0)
			{
				if (!rep->Create(this, m_nIdc[var]))
				{
					TRACE0( "Failed to create sheet " +sVarName +" \n");
					return FALSE;
				}
				if (rep->GetSafeHwnd() != NULL)
				{
					rep->SetImageList(&pImageList);
					vecReportControls.push_back(rep);
					rep->ShowWindow( SW_NORMAL );
					pCol = rep->AddColumn(new CXTPReportColumn(0, _T("++"),10));
					pCol->AllowRemove(FALSE);
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					pCol->SetEditable( TRUE );
					pCol->SetSortable(false);
					pCol->SetTooltip(_T("L�gg till eller ta bort ")+ xml->str(Xml_StringId_Urval[var]));
					pCol = rep->AddColumn(new CXTPReportColumn(1, xml->str(Xml_StringId_Urval[var]),m_nColHeaderWidth[var],true,1,true,true));
					pCol->AllowRemove(FALSE);
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					pCol->SetEditable( TRUE );
					setsizes(var);
				}
			}
			else
			{
				rep->ClearReport();
				setsizes(var);
			}
		}
		delete xml;
	}
	return TRUE;
}

									 
void CUMScaSvuppUrvalRoj::populateReport_Urval(int var)
{
	
	CMyReportCtrl2	*rep;
   CTraktReportDataRec_Urval *pRec;
	int nRow=0;
	BOOL bChecked=FALSE;
	CString test=_T("");
	std::vector<CString> vecCheck;
	vecCheck.clear();

	//S�tter rapportpekaren till r�tt urvalsvariabelrapport
	rep=vecReportControls[var];
	// Spara raderna i rapporten i en vektor
	for (nRow = 0;nRow < rep->GetRecords()->GetCount();nRow++)
	{
		CXTPReportRecords *pRecords = rep->GetRecords();
		// Stega igneom raderna i rapporten
		for (nRow = 0;nRow < pRecords->GetCount();nRow++)
		{
			// H�mta specifika raden
			pRec = (CTraktReportDataRec_Urval *)pRecords->GetAt(nRow);

			if(pRec->getColumnCheck(0))
			{
				vecCheck.push_back(pRec->getColumnText(1));
			}
		}
	}

	//Fyller urvalsvariabelrapport med data
	rep->ClearReport();

	for (UINT i = 0;i < m_vecTraktData_Roj.size();i++)
	{
		TRAKT_DATA_ROJ data = m_vecTraktData_Roj[i];
		bChecked=FALSE;
		for(UINT v=0;v<vecCheck.size();v++)
		{
			switch (var)
			{
			case 0:
				if(vecCheck[v]==data.m_sTrakt_Forvaltning)
					bChecked=TRUE;
				break;
			case 1:
				if(vecCheck[v]==data.m_sTrakt_Distrikt)
					bChecked=TRUE;
				break;
			case 2:
				test.Format(_T("%d"),data.m_nTrakt_Ursprung);
				if(vecCheck[v]==test)
					bChecked=TRUE;
				break;
			case 3:
				//test.Format(_T("%d"),data.m_nTrakt_ObjNr);
				if(vecCheck[v]==data.m_sTrakt_AtgardsId)
					bChecked=TRUE;
				break;
			case 4:
				if(vecCheck[v]==data.m_sTrakt_Entreprenor)
					bChecked=TRUE;
				break;
			case 5:
				if(vecCheck[v]==data.m_sTrakt_Lag)
					bChecked=TRUE;
				break;
			case 6:
				if(vecCheck[v]==data.m_sTrakt_Inventerare)
					bChecked=TRUE;
				break;
			}

		}
		rep->AddRecord(new CTraktReportDataRec_Urval(i,data,var,bChecked));
	}
	rep->Populate();
	rep->UpdateWindow();
}



//S�tter urvalsordningen, dvs vilken som valts f�rst etc...
void CUMScaSvuppUrvalRoj::SetOrder(int var)
{
	int nOrder=0,i=0;
	
	for(i=0;i<m_nNumOfUrval;i++)
	{
		if(i!=var)
		{
			if(nOrder<=m_nOrder_Urval[i])
				nOrder=m_nOrder_Urval[i]+1;
		}
	}
	m_nOrder_Urval[var]=nOrder;
}

//S�tter sql fr�ga baserat p� urval
int CUMScaSvuppUrvalRoj::SetSqlQues(int var)
{
	int nRow=0,nFirst=1,i=0,nSecond=0,found=0,nFound=0;
	CTraktReportDataRec_Urval *pRec;
	CString sVarName=_T("");
	CMyReportCtrl2	*rep;

	sVarName=m_sDBVarNames[var];
	m_sSqlQues.Format(_T("select distinct %s from %s"),sVarName,TBL_ROJ_TRAKT);

	if(m_nOrder_Urval[var]!=1)
	{
		
		for(i=0;i<m_nNumOfUrval;i++)
		{
			if(i!=var)
			{
				rep=vecReportControls[i];
				sVarName=m_sDBVarNames[i];				
				if(nFirst==0)
				{
					nSecond=1;
					nFirst=1;
				}
				nFound=0;
				if (rep->GetSafeHwnd() != NULL)
				{
					CXTPReportRecords *pRecords = rep->GetRecords();
					// Stega igneom raderna i rapporten
					for (nRow = 0;nRow < pRecords->GetCount();nRow++)
					{
						// H�mta specifika raden
						pRec = (CTraktReportDataRec_Urval *)pRecords->GetAt(nRow);

						if(pRec->getColumnCheck(0))
						{
							nFound=1;
							found=1;
							if(nFirst)
							{
								if(nSecond)
									m_sSqlQues+=_T(" and (");
								else
									m_sSqlQues+=_T(" where (");
								if(pRec->getColumnText(1).GetLength()<1)
									m_sSqlQues+=_T(" ")+ sVarName +  _T(" is null ");
								else
									m_sSqlQues+=_T(" ")+ sVarName +  _T(" = ") +  _T("'") + pRec->getColumnText(1)+ _T("'");
								nFirst=0;
							}
							else
								if(pRec->getColumnText(1).GetLength()<1)
									m_sSqlQues+=_T("or ")+sVarName + _T(" is null ");
								else
								m_sSqlQues+=_T("or ")+sVarName + _T(" =") + _T("'")+ pRec->getColumnText(1)+ _T("'");
						}
					}
				}
				if(nFound)
					m_sSqlQues+=_T(")");
			}
		}
	}

	if(!found && (m_nOrder_Urval[var]!=1))
		return 0;
	else
	{
		//L�gg till datum urval
		if(!found)
		{
			if(sDateFromSql.GetLength()>1 && nCheckedFrom)
			{
				found=1;
				m_sSqlQues+=_T(" where") + sDateFromSql;
				if(sDateToSql.GetLength()>1 && nCheckedTo)
					m_sSqlQues+=_T(" and ") + sDateToSql;
			}
			else
				if(sDateToSql.GetLength()>1 && nCheckedTo)
				{
					found=1;
					m_sSqlQues+=_T(" where ") + sDateToSql;
				}
		}
		else
		{
			if(sDateFromSql.GetLength()>1 && nCheckedFrom)
			{
				found=1;
				m_sSqlQues+=_T(" and ") + sDateFromSql;
				if(sDateToSql.GetLength()>1 && nCheckedTo)
					m_sSqlQues+=_T(" and ") + sDateToSql;		
			}
			else
			{
				if(sDateToSql.GetLength()>1 && nCheckedTo)
				{
					m_sSqlQues+=_T(" and ") + sDateToSql;		
					found=1;
				}
			}
		}

		//L�gg till arealsurval
		if(!found)
		{
			if(m_wndEdit_Areal_From.getText().GetLength()>0)
			{
				found=1;
				sArealFromSql.Format(_T(" where (Areal>='%s')"),m_wndEdit_Areal_From.getText());
				m_sSqlQues+=sArealFromSql;
				if(m_wndEdit_Areal_To.getText().GetLength()>0)
				{
					sArealToSql.Format(_T(" and (Areal<='%s')"),m_wndEdit_Areal_To.getText());
					m_sSqlQues+=sArealToSql;
				}
			}
			else
			{			
				if(m_wndEdit_Areal_To.getText().GetLength()>0)
				{
					found=1;
					sArealToSql.Format(_T(" where (Areal<='%s')"),m_wndEdit_Areal_To.getText());
					m_sSqlQues+=sArealToSql;
				}
			}
		}
		else
		{
			if(m_wndEdit_Areal_From.getText().GetLength()>0)
			{
				found=1;
				sArealFromSql.Format(_T(" and (Areal>='%s')"),m_wndEdit_Areal_From.getText());
				m_sSqlQues+=sArealFromSql;
				if(m_wndEdit_Areal_To.getText().GetLength()>0)
				{
					sArealToSql.Format(_T(" and (Areal<='%s')"),m_wndEdit_Areal_To.getText());
					m_sSqlQues+=sArealToSql;
				}
			}
			else
			{			
				if(m_wndEdit_Areal_To.getText().GetLength()>0)
				{
					found=1;
					sArealToSql.Format(_T(" and (Areal<='%s')"),m_wndEdit_Areal_To.getText());
					m_sSqlQues+=sArealToSql;
				}
			}
		}
		//L�gg till inv typ 
		if(!found)
			m_sSqlQues+=_T(" where (InvTyp=0)");
		else
			m_sSqlQues+=_T(" and (InvTyp=0)");
		return 1;
	}
}


//S�tter sql fr�ga baserat p� urval
int CUMScaSvuppUrvalRoj::SetSqlQues()
{
	int nRow=0,nFirst=1,i=0,nSecond=0,nFound=0,nFound2=0;
	CTraktReportDataRec_Urval *pRec;
	CString sVarName=_T(""),sTest=_T("");
	CMyReportCtrl2	*rep;	

	m_sSqlQues.Format(_T("select * from %s"),TBL_ROJ_TRAKT);
	
	for(i=0;i<m_nNumOfUrval;i++)
	{
		rep=vecReportControls[i];
		sVarName=m_sDBVarNames[i];
		nFound=0;
		if(nFirst==0)
		{
			nSecond=1;
			nFirst=1;
		}
		if (rep->GetSafeHwnd() != NULL)
		{
			CXTPReportRecords *pRecords = rep->GetRecords();
			// Stega igneom raderna i rapporten
			for (nRow = 0;nRow < pRecords->GetCount();nRow++)
			{
				// H�mta specifika raden
				pRec = (CTraktReportDataRec_Urval *)pRecords->GetAt(nRow);

				if(pRec->getColumnCheck(0))
				{
					nFound=1;
					nFound2=1;
					if(nFirst)
					{
						if(nSecond)
							m_sSqlQues+=_T(" and (");
						else
							m_sSqlQues+=_T(" where (");
						if(pRec->getColumnText(1).GetLength()<1)
							m_sSqlQues+=_T(" ")+ sVarName +  _T(" is null ");							
						else
							m_sSqlQues+=_T(" ")+ sVarName +  _T(" = ") + _T("'") + pRec->getColumnText(1) + _T("'");
						nFirst=0;
					}
					else
						if(pRec->getColumnText(1).GetLength()<1)
							m_sSqlQues+=_T(" or ")+sVarName + _T(" is null ");
						else
							m_sSqlQues+=_T(" or ")+sVarName + _T(" =") + _T("'") + pRec->getColumnText(1) + _T("'");
				}
			}
		}
		if(nFound)
			m_sSqlQues+=_T(")");
	}

	//Datum urval
	if(nFound2)
	{
		if(sDateFromSql.GetLength()>1 && nCheckedFrom)
			m_sSqlQues+=_T(" and ") + sDateFromSql;
		if(sDateToSql.GetLength()>1 && nCheckedTo)
			m_sSqlQues+=_T(" and ") + sDateToSql;
	}
	else
	{
		if(sDateFromSql.GetLength()>1 && nCheckedFrom)
		{
			nFound2=1;
			nFound=1;
			m_sSqlQues+=_T(" where ") + sDateFromSql;
			if(sDateToSql.GetLength()>1 && nCheckedTo)
				m_sSqlQues+=_T(" and ") + sDateToSql;		
		}
		else
		{
			if(sDateToSql.GetLength()>1 && nCheckedTo)
			{
				nFound2=1;
				nFound=1;
				m_sSqlQues+=_T(" where ") + sDateToSql;		
			}
		}
	}

	//L�gg till arealsurval
	if(!nFound2)
	{
		if(m_wndEdit_Areal_From.getText().GetLength()>0)
		{
			nFound=1;
			sArealFromSql.Format(_T(" where (Areal>='%s')"),m_wndEdit_Areal_From.getText());
			m_sSqlQues+=sArealFromSql;
			if(m_wndEdit_Areal_To.getText().GetLength()>0)
			{
				sArealToSql.Format(_T(" and (Areal<='%s')"),m_wndEdit_Areal_To.getText());
				m_sSqlQues+=sArealToSql;
			}
		}
		else
		{			
			if(m_wndEdit_Areal_To.getText().GetLength()>0)
			{
				nFound=1;
				sArealToSql.Format(_T(" where (Areal<='%s')"),m_wndEdit_Areal_To.getText());
				m_sSqlQues+=sArealToSql;
			}
		}
	}
	else
	{
		if(m_wndEdit_Areal_From.getText().GetLength()>0)
		{
			sArealFromSql.Format(_T(" and (Areal>='%s')"),m_wndEdit_Areal_From.getText());
			m_sSqlQues+=sArealFromSql;
			if(m_wndEdit_Areal_To.getText().GetLength()>0)
			{
				sArealToSql.Format(_T(" and (Areal<='%s')"),m_wndEdit_Areal_To.getText());
				m_sSqlQues+=sArealToSql;
			}
		}
		else
		{			
			if(m_wndEdit_Areal_To.getText().GetLength()>0)
			{
				sArealToSql.Format(_T(" and (Areal<='%s')"),m_wndEdit_Areal_To.getText());
				m_sSqlQues+=sArealToSql;
			}
		}
	}
	//L�gg till inv typ 
	if(!nFound2)
		m_sSqlQues+=_T(" where (InvTyp=0)");
	else
		m_sSqlQues+=_T(" and (InvTyp=0)");
	return 1;

}


void CUMScaSvuppUrvalRoj::OnClickedPlantButtons(int var)
{
	SetOrder(var);
	if(SetSqlQues(var))
	{
		if (m_bConnected)
		{
			CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
			if (pDB != NULL)
			{
				m_vecTraktData_Roj.clear();
				if(pDB->getTrakts(m_vecTraktData_Roj,m_sSqlQues,var))
				{
					populateReport_Urval(var);
				}
				delete pDB;
			}
		}
	}
	else //Om inga vald gjorda i ovanst�ende urval s� rensa de nedanf�r
	{
		ClearAndUpdateUrval(var);
	}
}

//Rensa ett urval och rensa de med h�gre nummer
void CUMScaSvuppUrvalRoj::ClearAndUpdateUrval(int var)
{
	CMyReportCtrl2	*rep;
	int i=0;
	for(i=0;i<m_nNumOfUrval;i++)
	{
		if(i!=var)
		{
			if(m_nOrder_Urval[i]>m_nOrder_Urval[var])
			{
				rep=vecReportControls[i];
				rep->ClearReport();
				rep->Populate();
				rep->UpdateWindow();
				m_nOrder_Urval[i]=0;
				//Borde nog s�tta -- tecknet till ett ++ h�r ocks� 20120507 J�
				rep->GetColumns()->GetAt(0)->SetCaption(_T("++"));	
			}
		}
	}
	rep=vecReportControls[var];
	rep->ClearReport();
	rep->Populate();
	rep->UpdateWindow();
	m_nOrder_Urval[var]=0;
	//Borde nog s�tta -- tecknet till ett ++ h�r ocks� 20120507 J�
	rep->GetColumns()->GetAt(0)->SetCaption(_T("++"));	
	Populate_TraktReport();
}



//Uppdatera val med h�gre ordningsnummer om n�got val �ndrats
void CUMScaSvuppUrvalRoj::UpdateUrvalFrom(int var)
{
	int i=0;
	int nCurrentOrder;
	if( var==-1)
		nCurrentOrder=-1;
	else
		nCurrentOrder=m_nOrder_Urval[var];

	for(i=0;i<m_nNumOfUrval;i++)
	{
		if(i!=var)
		{
			if(m_nOrder_Urval[i]>nCurrentOrder &&m_nOrder_Urval[i]>0)
			{
				if(SetSqlQues(i))
				{
					if (m_bConnected)
					{
						CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
						if (pDB != NULL)
						{
							m_vecTraktData_Roj.clear();
							if(pDB->getTrakts(m_vecTraktData_Roj,m_sSqlQues,i))
							{
								populateReport_Urval(i);
							}
							delete pDB;
						}
					}
					m_sSqlQues=_T("");
				}
				else
				{
					m_sSqlQues=_T("");
					ClearAndUpdateUrval(i);
				}
			}
		}
	}
	Populate_TraktReport();
}

int CUMScaSvuppUrvalRoj::IdcToVar(int nIdc)
{
	int i=0,ret=-1;
	for(i=0;i<m_nNumOfUrval;i++)
	{
		if(nIdc==m_nIdc[i])
		{
			ret=i;
		}
	}
	return ret;
}

//Klickat p� n�gon planterings urvalsrapport
void CUMScaSvuppUrvalRoj::OnReportUrvalClick(NMHDR * pNotifyStruct, LRESULT * )
{
	int nVar=0;
	CMyReportCtrl2	*rep;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		if (pItemNotify->pRow)
		{
			//Om klick p� checkboxen s� uppdatera de urval med h�gre ordningsnummer
			if(pItemNotify->pColumn->GetIndex()==0)
			{
				nVar=IdcToVar(pItemNotify->hdr.idFrom);
				if(nVar!=-1)
					UpdateUrvalFrom(nVar);
			}
		}
		else//Klickat p� header
		{
			//Om klick p� Vald header s� l�gg till  eller rensa
			if(pItemNotify->pColumn->GetIndex()==0)
			{
				nVar=IdcToVar(pItemNotify->hdr.idFrom);
				if(nVar!=-1)
				{
					//Kolla om det redan finns data annars rensa
					rep=vecReportControls[nVar];
					//Inget Data Fyll p�
					if(rep->GetRecords()->GetCount()<=0)
					{
						pItemNotify->pColumn->SetCaption(_T("--"));
						OnClickedPlantButtons(nVar);
					}
					else //Data finns, rensa
					{
						ClearAndUpdateUrval(nVar);
						pItemNotify->pColumn->SetCaption(_T("++"));
					}
				}
			}
		}
	}          
}


//---------------------------------------------------------------------
//-------------------- HUVUDTRAKTLISTAN -------------------------------
//---------------------------------------------------------------------

// Skapa huvudtraktlistan
BOOL CUMScaSvuppUrvalRoj::setupReport(void)
{
	int nNumOfTabs = 0,col=0,i=0,nTest=0;
	CString sTest=_T("");
	CXTPReportColumn *pCol = NULL;

	if (m_wndTraktReport.GetSafeHwnd() == 0)
	{
		if (!m_wndTraktReport.Create(this, IDC_TRAKT_REPORT_5 ))
		{
			TRACE0( "Failed to create sheet1.\n" );
			return FALSE;
		}
		else
		{
		m_wndTraktReport.ShowGroupBy(TRUE);
		}
		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				if (m_wndTraktReport.GetSafeHwnd() != NULL)
				{
					CUMScaSvuppUrvalRojFrame *pView = (CUMScaSvuppUrvalRojFrame *)getFormViewByID(IDD_FORMVIEW5)->GetParent();
					if (pView)
					{
						CDocument *pDoc = pView->GetActiveDocument();
						if (pDoc != NULL)
							pDoc->SetTitle(xml->str(IDS_STRING8518));		
					}
					m_wndTraktReport.ShowWindow( SW_NORMAL );
					pCol = m_wndTraktReport.AddColumn(new CXTPReportColumn(0, _T("Vald"),30));
					pCol->AllowRemove(FALSE);
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					pCol->SetEditable( TRUE );
					pCol->SetGroupable(FALSE);

					for(col=1;col<m_nNumOfRepColumns+1;col++)
					{
						pCol = m_wndTraktReport.AddColumn(new CXTPReportColumn(col, xml->str(Xml_StringId_Rep_Columns[col-1]), 90));
						pCol->SetEditable( FALSE );
						pCol->SetHeaderAlignment( DT_CENTER );
						pCol->SetAlignment( DT_CENTER );
						pCol->SetGroupable(TRUE);
					}
					RECT rect;
					GetClientRect(&rect);
					setsizes(-1);
					populateReport();
				}	
			}
			delete xml;
		}
	}
	else
	{
		m_wndTraktReport.ClearReport();
		setsizes(-1);
		populateReport();
	}
	return TRUE;
}

// G�r xml filer av valda trakter
void CUMScaSvuppUrvalRoj::OnBnCreateFiles()
{	
	CMyReportCtrl2	*rep;
   CTraktReportDataRec *pRec;
	int nRow=0,nChecked=0,nTraktId,num=0;
	TRAKT_DATA_PLANT trkt;
	TRAKT_DATA_MARK trkt2;
	TRAKT_DATA_ROJ trkt3;
	CString sFileName,sTemp,sCreated, csTmp, csDir;
	n_Replace_All_Files=DO_DIALOG;
	//S�tter rapportpekaren till r�tt rapport
	rep=&m_wndTraktReport;

	//Kolla om det finns n�gra trakter valda
	for (nRow = 0;nRow < rep->GetRecords()->GetCount();nRow++)
	{
		CXTPReportRecords *pRecords = rep->GetRecords();
		// Stega igneom raderna i rapporten
		for (nRow = 0;nRow < pRecords->GetCount();nRow++)
		{
			// H�mta specifika raden
			pRec = (CTraktReportDataRec *)pRecords->GetAt(nRow);
			//Kolla om den �r icheckad i s� fall skall xml fil genereras
			if(pRec->getColumnCheck(0))
			{
				nChecked=1;
				CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);

				if (pDB != NULL)
				{
					nTraktId=pRec->getTraktId();
					//H�mta traktdata
					if(pDB->getTrakt(nTraktId,trkt3))
					{
						//H�mta plotdata f�r trakten
						if(pDB->getPlots(trkt3))
						{
							// directory
							csDir = regGetStr(REG_ROOT, PROGRAM_NAME, REG_SCASVUPP_XML_ROJ_FILEPATH_KEY_NAME, _T(""));
							csTmp.Format(_T("%s %8.8s00\\"), trkt3.m_sTrakt_Traktnamn, trkt3.m_sTrakt_Datum);
							csDir += csTmp;

							// fil
							sFileName = csDir;
							CreateDirectory(sFileName, NULL);							
							if(trkt3.m_sTrakt_OwnerId==_T(""))
								sTemp.Format(_T("r %8.8s00-0000.ecx"),trkt3.m_sTrakt_Datum);
							else
								sTemp.Format(_T("r %8.8s00-%s.ecx"),trkt3.m_sTrakt_Datum,trkt3.m_sTrakt_OwnerId);
							sFileName+=sTemp;

							int nRet;
							if( (nRet = Check_File(sFileName, &n_Replace_All_Files)) > 0 )
							{
								if( nRet == 2 )	// rename
								{
									// kolla efter n�sta, lediga, l�pnummer
									int nTmp = GetNextFileNumber(csDir);

									// �ndra filnamnet
									csDir = regGetStr(REG_ROOT, PROGRAM_NAME, REG_SCASVUPP_XML_ROJ_FILEPATH_KEY_NAME, _T(""));
									csTmp.Format(_T("%s %8.8s%02d\\"), trkt3.m_sTrakt_Traktnamn, trkt3.m_sTrakt_Datum, nTmp);
									csDir += csTmp;
									sFileName = csDir;

									if(trkt3.m_sTrakt_OwnerId==_T(""))
										sTemp.Format(_T("r %8.8s%02d-0000.ecx"), trkt3.m_sTrakt_Datum, nTmp);
									else
										sTemp.Format(_T("r %8.8s%02d-%s.ecx"), trkt3.m_sTrakt_Datum, nTmp, trkt3.m_sTrakt_OwnerId);
									sFileName += sTemp;
								}

								if(HXL_Make_File_Roj(trkt3,sFileName))
								{
									num++;
									sTemp.Format(_T("%d. %s %s %s %s\r\n"),num,trkt3.m_sTrakt_Datum,trkt3.m_sTrakt_Forvaltning,trkt3.m_sTrakt_Distrikt,trkt3.m_sTrakt_Traktnamn);
									sCreated+=sTemp;
								}
							}
						}
					}
					delete pDB;
				}
			}
		}
	}
	if(num)
	{
		sTemp.Format(_T("\r\n\r\n Skapade %d st filer"),num);
		sCreated+=sTemp;
		AfxMessageBox(sCreated);
	}
}

// Populera hela listan
void CUMScaSvuppUrvalRoj::Populate_TraktReport()
{
	if(SetSqlQues())
	{
		if (m_bConnected)
		{
			CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
			if (pDB != NULL)
			{
				m_vecTraktData_Roj.clear();
				if(pDB->getTrakts(m_vecTraktData_Roj,m_sSqlQues))
				{
					populateReport();
				}
				delete pDB;
			}
		}
	}
	else
	{
		m_wndTraktReport.ClearReport();
		m_wndTraktReport.Populate();
		m_wndTraktReport.UpdateWindow();	
	}
}

void CUMScaSvuppUrvalRoj::populateReport(void)
{
	CMyReportCtrl2	*rep;
   CTraktReportDataRec *pRec;
	int nRow=0;
	BOOL bChecked=FALSE;
	CString test=_T("");
	std::vector<int> vecCheck;
	vecCheck.clear();

	//S�tter rapportpekaren till r�tt rapport
	rep=&m_wndTraktReport;


	// Spara de trakter som �r valda i rapporten i en vektor
	for (nRow = 0;nRow < rep->GetRecords()->GetCount();nRow++)
	{
		CXTPReportRecords *pRecords = rep->GetRecords();
		// Stega igneom raderna i rapporten
		for (nRow = 0;nRow < pRecords->GetCount();nRow++)
		{
			// H�mta specifika raden
			pRec = (CTraktReportDataRec *)pRecords->GetAt(nRow);

			if(pRec->getColumnCheck(0))
			{
				vecCheck.push_back(pRec->getTraktId());
			}
		}
	}

	//Fyller rapport med data
	rep->ClearReport();
	for (UINT i = 0;i < m_vecTraktData_Roj.size();i++)
	{
		TRAKT_DATA_ROJ data = m_vecTraktData_Roj[i];
		bChecked=FALSE;
		for(UINT v=0;v<vecCheck.size();v++)
		{
			if(vecCheck[v]==data.m_nTrakt_Id)
			{
				bChecked=TRUE;
			}
		}
		rep->AddRecord(new CTraktReportDataRec(i,data,bChecked));
	}
	rep->Populate();
	rep->UpdateWindow();
}

void CUMScaSvuppUrvalRoj::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * )
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify->pRow)
	{


		/*CTraktReportDataRec *pRec = (CTraktReportDataRec*)pItemNotify->pItem->GetRecord();

		CMDIScaGBTraktFormView *pView = (CMDIScaGBTraktFormView *)getFormViewByID(IDD_FORMVIEW2);
		if (pView)
			pView->doPopulate(pRec->getIndex());*/
	}
}



/*void CUMScaSvuppUrvalRoj::getTraktsFromDB(void)
{
	if (m_bConnected)
	{
		// Get Region information from Database server; 070122 p�d
		CScaGallBasDB *pDB = new CScaGallBasDB(m_dbConnectionData);
		if (pDB != NULL)
		{
			pDB->getTrakts(m_vecTraktData_Roj);
			delete pDB;
		}	// if (pDB != NULL)
	} // 	if (m_bConnected)
}*/


bool CUMScaSvuppUrvalRoj::OnChangeDtpicker1()
{
		m_wndDateTimeCtrlFrom.SetFormat(DTS_SHORTDATEFORMAT);
		return true;
}


LRESULT CUMScaSvuppUrvalRoj::OnArealChange( WPARAM wParam, LPARAM lParam )
{
	switch(wParam)
	{
	case 1:
		UpdateUrvalFrom(-1);
		break;
	case 2:
		UpdateUrvalFrom(-1);
		break;
	}
	return 0L;
}


void CUMScaSvuppUrvalRoj::OnArealFromChange()
{
	CString sTst=_T("");
	m_wndEdit_Areal_From.GetWindowText(sTst);
	m_wndEdit_Areal_From.Change(sTst);
	//UpdateUrvalFrom(-1);
}

void CUMScaSvuppUrvalRoj::OnArealToChange()
{
	CString sTst=_T("");
	m_wndEdit_Areal_To.GetWindowText(sTst);
	m_wndEdit_Areal_To.Change(sTst);
	//UpdateUrvalFrom(-1);
}

//Fr�n datum boxen h�ndelse
void CUMScaSvuppUrvalRoj::OnChangeDatetimepicker5_1( NMHDR* pNMHDR, LRESULT* pResult )
{
	LPNMDATETIMECHANGE pDTPicker;
	CTime t;
	switch( pNMHDR->code )
	{
	case DTN_USERSTRING:
	case DTN_DROPDOWN:
		m_wndDateTimeCtrlFrom.SetFormat(DTS_SHORTDATEFORMAT);
		*pResult= 0;
		break;
	case DTN_DATETIMECHANGE:
		pDTPicker= (LPNMDATETIMECHANGE)pNMHDR;
		if (pDTPicker->dwFlags == GDT_VALID && nCheckedFrom==0)
		{
			nCheckedFrom=1;
			m_wndDateTimeCtrlFrom.SetFormat(DTS_SHORTDATEFORMAT);
			// Date entered...
			m_wndDateTimeCtrlFrom.GetTime(t);
			sDateFromSql.Format(_T(" (Datum>='%04d%02d%02d')"),t.GetYear(),t.GetMonth(),t.GetDay());				
			//S�tter -1 som argument vilket betyder att alla val uppdateras
			//Dvs datum urvalet p�verkar alla andra val.
			UpdateUrvalFrom(-1);
		}
		else if (pDTPicker->dwFlags == GDT_NONE && nCheckedFrom==1)
		{
			sDateFromSql=_T("");
			//S�tter -1 som argument vilket betyder att alla val uppdateras
			//Dvs datum urvalet p�verkar alla andra val.
			UpdateUrvalFrom(-1);
			nCheckedFrom=0;
		}
		*pResult= 0;
		break;
	case DTN_CLOSEUP:
		pDTPicker= (LPNMDATETIMECHANGE)pNMHDR;
		if ( pDTPicker->dwFlags == 1 )
		{
			sDateFromSql=_T("");
			//S�tter -1 som argument vilket betyder att alla val uppdateras
			//Dvs datum urvalet p�verkar alla andra val.
			UpdateUrvalFrom(-1);
		}
		else
		{
			// Date entered...
			m_wndDateTimeCtrlFrom.GetTime(t);
			sDateFromSql.Format(_T(" (Datum>='%04d%02d%02d')"),t.GetYear(),t.GetMonth(),t.GetDay());				
			//S�tter -1 som argument vilket betyder att alla val uppdateras
			//Dvs datum urvalet p�verkar alla andra val.
			UpdateUrvalFrom(-1);
		}
		break;
	default:
		break;
	}
	*pResult= 0;
}

//Till datum boxen st�ngt
void CUMScaSvuppUrvalRoj::OnChangeDatetimepicker5_2( NMHDR* pNMHDR, LRESULT* pResult )
{
	LPNMDATETIMECHANGE pDTPicker;
	CTime t;

	switch( pNMHDR->code )
	{
	case DTN_USERSTRING:
	case DTN_DROPDOWN:
		m_wndDateTimeCtrlTo.SetFormat(DTS_SHORTDATEFORMAT);
		*pResult= 0;
		break;
	case DTN_DATETIMECHANGE:
		pDTPicker= (LPNMDATETIMECHANGE)pNMHDR;
		if (pDTPicker->dwFlags == GDT_VALID && nCheckedTo==0)
		{
			nCheckedTo=1;
			m_wndDateTimeCtrlTo.SetFormat(DTS_SHORTDATEFORMAT);
		// Date entered...
			m_wndDateTimeCtrlTo.GetTime(t);
			sDateToSql.Format(_T(" (Datum<='%04d%02d%02d')"),t.GetYear(),t.GetMonth(),t.GetDay());				
			//S�tter -1 som argument vilket betyder att alla val uppdateras
			//Dvs datum urvalet p�verkar alla andra val.
			UpdateUrvalFrom(-1);
		}
		else if (pDTPicker->dwFlags == GDT_NONE && nCheckedTo==1)
		{
			nCheckedTo=0;
			sDateToSql=_T("");
			//S�tter -1 som argument vilket betyder att alla val uppdateras
			//Dvs datum urvalet p�verkar alla andra val.
			UpdateUrvalFrom(-1);
		}
		*pResult= 0;
		break;
	case DTN_CLOSEUP:
		pDTPicker= (LPNMDATETIMECHANGE)pNMHDR;
		if ( pDTPicker->dwFlags == 1 )
		{
			sDateToSql=_T("");
			//S�tter -1 som argument vilket betyder att alla val uppdateras
			//Dvs datum urvalet p�verkar alla andra val.
			UpdateUrvalFrom(-1);
		}
		else
		{
			// Date entered...
			m_wndDateTimeCtrlTo.GetTime(t);
			sDateToSql.Format(_T(" (Datum<='%04d%02d%02d')"),t.GetYear(),t.GetMonth(),t.GetDay());				
			//S�tter -1 som argument vilket betyder att alla val uppdateras
			//Dvs datum urvalet p�verkar alla andra val.
			UpdateUrvalFrom(-1);
		}
		break;
	default:
		break;
	}

	*pResult= 0;
}
